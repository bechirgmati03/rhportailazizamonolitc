package com.aziza.rhportail.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DiplomaDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DiplomaDTO.class);
        DiplomaDTO diplomaDTO1 = new DiplomaDTO();
        diplomaDTO1.setId(1L);
        DiplomaDTO diplomaDTO2 = new DiplomaDTO();
        assertThat(diplomaDTO1).isNotEqualTo(diplomaDTO2);
        diplomaDTO2.setId(diplomaDTO1.getId());
        assertThat(diplomaDTO1).isEqualTo(diplomaDTO2);
        diplomaDTO2.setId(2L);
        assertThat(diplomaDTO1).isNotEqualTo(diplomaDTO2);
        diplomaDTO1.setId(null);
        assertThat(diplomaDTO1).isNotEqualTo(diplomaDTO2);
    }
}
