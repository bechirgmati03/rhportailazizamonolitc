package com.aziza.rhportail.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChildrentCoordinateDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChildrentCoordinateDTO.class);
        ChildrentCoordinateDTO childrentCoordinateDTO1 = new ChildrentCoordinateDTO();
        childrentCoordinateDTO1.setId(1L);
        ChildrentCoordinateDTO childrentCoordinateDTO2 = new ChildrentCoordinateDTO();
        assertThat(childrentCoordinateDTO1).isNotEqualTo(childrentCoordinateDTO2);
        childrentCoordinateDTO2.setId(childrentCoordinateDTO1.getId());
        assertThat(childrentCoordinateDTO1).isEqualTo(childrentCoordinateDTO2);
        childrentCoordinateDTO2.setId(2L);
        assertThat(childrentCoordinateDTO1).isNotEqualTo(childrentCoordinateDTO2);
        childrentCoordinateDTO1.setId(null);
        assertThat(childrentCoordinateDTO1).isNotEqualTo(childrentCoordinateDTO2);
    }
}
