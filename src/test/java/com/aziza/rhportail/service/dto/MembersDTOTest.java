package com.aziza.rhportail.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MembersDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MembersDTO.class);
        MembersDTO membersDTO1 = new MembersDTO();
        membersDTO1.setId(1L);
        MembersDTO membersDTO2 = new MembersDTO();
        assertThat(membersDTO1).isNotEqualTo(membersDTO2);
        membersDTO2.setId(membersDTO1.getId());
        assertThat(membersDTO1).isEqualTo(membersDTO2);
        membersDTO2.setId(2L);
        assertThat(membersDTO1).isNotEqualTo(membersDTO2);
        membersDTO1.setId(null);
        assertThat(membersDTO1).isNotEqualTo(membersDTO2);
    }
}
