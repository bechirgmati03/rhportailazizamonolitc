package com.aziza.rhportail.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ProfessionalExpeienceDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfessionalExpeienceDTO.class);
        ProfessionalExpeienceDTO professionalExpeienceDTO1 = new ProfessionalExpeienceDTO();
        professionalExpeienceDTO1.setId(1L);
        ProfessionalExpeienceDTO professionalExpeienceDTO2 = new ProfessionalExpeienceDTO();
        assertThat(professionalExpeienceDTO1).isNotEqualTo(professionalExpeienceDTO2);
        professionalExpeienceDTO2.setId(professionalExpeienceDTO1.getId());
        assertThat(professionalExpeienceDTO1).isEqualTo(professionalExpeienceDTO2);
        professionalExpeienceDTO2.setId(2L);
        assertThat(professionalExpeienceDTO1).isNotEqualTo(professionalExpeienceDTO2);
        professionalExpeienceDTO1.setId(null);
        assertThat(professionalExpeienceDTO1).isNotEqualTo(professionalExpeienceDTO2);
    }
}
