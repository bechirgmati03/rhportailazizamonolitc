package com.aziza.rhportail.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.aziza.rhportail.IntegrationTest;
import com.aziza.rhportail.domain.Diploma;
import com.aziza.rhportail.repository.DiplomaRepository;
import com.aziza.rhportail.service.dto.DiplomaDTO;
import com.aziza.rhportail.service.mapper.DiplomaMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DiplomaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DiplomaResourceIT {

    private static final String DEFAULT_INSTITUTION = "AAAAAAAAAA";
    private static final String UPDATED_INSTITUTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_YEAR_OF_GRADUATION = 1;
    private static final Integer UPDATED_YEAR_OF_GRADUATION = 2;

    private static final String DEFAULT_DIPLOMA_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_DIPLOMA_TITLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/diplomas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DiplomaRepository diplomaRepository;

    @Autowired
    private DiplomaMapper diplomaMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDiplomaMockMvc;

    private Diploma diploma;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diploma createEntity(EntityManager em) {
        Diploma diploma = new Diploma()
            .institution(DEFAULT_INSTITUTION)
            .yearOfGraduation(DEFAULT_YEAR_OF_GRADUATION)
            .diplomaTitle(DEFAULT_DIPLOMA_TITLE);
        return diploma;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diploma createUpdatedEntity(EntityManager em) {
        Diploma diploma = new Diploma()
            .institution(UPDATED_INSTITUTION)
            .yearOfGraduation(UPDATED_YEAR_OF_GRADUATION)
            .diplomaTitle(UPDATED_DIPLOMA_TITLE);
        return diploma;
    }

    @BeforeEach
    public void initTest() {
        diploma = createEntity(em);
    }

    @Test
    @Transactional
    void createDiploma() throws Exception {
        int databaseSizeBeforeCreate = diplomaRepository.findAll().size();
        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);
        restDiplomaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diplomaDTO)))
            .andExpect(status().isCreated());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeCreate + 1);
        Diploma testDiploma = diplomaList.get(diplomaList.size() - 1);
        assertThat(testDiploma.getInstitution()).isEqualTo(DEFAULT_INSTITUTION);
        assertThat(testDiploma.getYearOfGraduation()).isEqualTo(DEFAULT_YEAR_OF_GRADUATION);
        assertThat(testDiploma.getDiplomaTitle()).isEqualTo(DEFAULT_DIPLOMA_TITLE);
    }

    @Test
    @Transactional
    void createDiplomaWithExistingId() throws Exception {
        // Create the Diploma with an existing ID
        diploma.setId(1L);
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        int databaseSizeBeforeCreate = diplomaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDiplomaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diplomaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDiplomas() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        // Get all the diplomaList
        restDiplomaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(diploma.getId().intValue())))
            .andExpect(jsonPath("$.[*].institution").value(hasItem(DEFAULT_INSTITUTION)))
            .andExpect(jsonPath("$.[*].yearOfGraduation").value(hasItem(DEFAULT_YEAR_OF_GRADUATION)))
            .andExpect(jsonPath("$.[*].diplomaTitle").value(hasItem(DEFAULT_DIPLOMA_TITLE)));
    }

    @Test
    @Transactional
    void getDiploma() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        // Get the diploma
        restDiplomaMockMvc
            .perform(get(ENTITY_API_URL_ID, diploma.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(diploma.getId().intValue()))
            .andExpect(jsonPath("$.institution").value(DEFAULT_INSTITUTION))
            .andExpect(jsonPath("$.yearOfGraduation").value(DEFAULT_YEAR_OF_GRADUATION))
            .andExpect(jsonPath("$.diplomaTitle").value(DEFAULT_DIPLOMA_TITLE));
    }

    @Test
    @Transactional
    void getNonExistingDiploma() throws Exception {
        // Get the diploma
        restDiplomaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDiploma() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();

        // Update the diploma
        Diploma updatedDiploma = diplomaRepository.findById(diploma.getId()).get();
        // Disconnect from session so that the updates on updatedDiploma are not directly saved in db
        em.detach(updatedDiploma);
        updatedDiploma.institution(UPDATED_INSTITUTION).yearOfGraduation(UPDATED_YEAR_OF_GRADUATION).diplomaTitle(UPDATED_DIPLOMA_TITLE);
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(updatedDiploma);

        restDiplomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, diplomaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isOk());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
        Diploma testDiploma = diplomaList.get(diplomaList.size() - 1);
        assertThat(testDiploma.getInstitution()).isEqualTo(UPDATED_INSTITUTION);
        assertThat(testDiploma.getYearOfGraduation()).isEqualTo(UPDATED_YEAR_OF_GRADUATION);
        assertThat(testDiploma.getDiplomaTitle()).isEqualTo(UPDATED_DIPLOMA_TITLE);
    }

    @Test
    @Transactional
    void putNonExistingDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, diplomaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(diplomaDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDiplomaWithPatch() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();

        // Update the diploma using partial update
        Diploma partialUpdatedDiploma = new Diploma();
        partialUpdatedDiploma.setId(diploma.getId());

        restDiplomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDiploma.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDiploma))
            )
            .andExpect(status().isOk());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
        Diploma testDiploma = diplomaList.get(diplomaList.size() - 1);
        assertThat(testDiploma.getInstitution()).isEqualTo(DEFAULT_INSTITUTION);
        assertThat(testDiploma.getYearOfGraduation()).isEqualTo(DEFAULT_YEAR_OF_GRADUATION);
        assertThat(testDiploma.getDiplomaTitle()).isEqualTo(DEFAULT_DIPLOMA_TITLE);
    }

    @Test
    @Transactional
    void fullUpdateDiplomaWithPatch() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();

        // Update the diploma using partial update
        Diploma partialUpdatedDiploma = new Diploma();
        partialUpdatedDiploma.setId(diploma.getId());

        partialUpdatedDiploma
            .institution(UPDATED_INSTITUTION)
            .yearOfGraduation(UPDATED_YEAR_OF_GRADUATION)
            .diplomaTitle(UPDATED_DIPLOMA_TITLE);

        restDiplomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDiploma.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDiploma))
            )
            .andExpect(status().isOk());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
        Diploma testDiploma = diplomaList.get(diplomaList.size() - 1);
        assertThat(testDiploma.getInstitution()).isEqualTo(UPDATED_INSTITUTION);
        assertThat(testDiploma.getYearOfGraduation()).isEqualTo(UPDATED_YEAR_OF_GRADUATION);
        assertThat(testDiploma.getDiplomaTitle()).isEqualTo(UPDATED_DIPLOMA_TITLE);
    }

    @Test
    @Transactional
    void patchNonExistingDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, diplomaDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDiploma() throws Exception {
        int databaseSizeBeforeUpdate = diplomaRepository.findAll().size();
        diploma.setId(count.incrementAndGet());

        // Create the Diploma
        DiplomaDTO diplomaDTO = diplomaMapper.toDto(diploma);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDiplomaMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(diplomaDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Diploma in the database
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDiploma() throws Exception {
        // Initialize the database
        diplomaRepository.saveAndFlush(diploma);

        int databaseSizeBeforeDelete = diplomaRepository.findAll().size();

        // Delete the diploma
        restDiplomaMockMvc
            .perform(delete(ENTITY_API_URL_ID, diploma.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Diploma> diplomaList = diplomaRepository.findAll();
        assertThat(diplomaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
