package com.aziza.rhportail.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.aziza.rhportail.IntegrationTest;
import com.aziza.rhportail.domain.ProfessionalExpeience;
import com.aziza.rhportail.repository.ProfessionalExpeienceRepository;
import com.aziza.rhportail.service.dto.ProfessionalExpeienceDTO;
import com.aziza.rhportail.service.mapper.ProfessionalExpeienceMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProfessionalExpeienceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ProfessionalExpeienceResourceIT {

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_POSTE = "AAAAAAAAAA";
    private static final String UPDATED_POSTE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTEMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTEMENT = "BBBBBBBBBB";

    private static final String DEFAULT_STORE = "AAAAAAAAAA";
    private static final String UPDATED_STORE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/professional-expeiences";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ProfessionalExpeienceRepository professionalExpeienceRepository;

    @Autowired
    private ProfessionalExpeienceMapper professionalExpeienceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProfessionalExpeienceMockMvc;

    private ProfessionalExpeience professionalExpeience;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfessionalExpeience createEntity(EntityManager em) {
        ProfessionalExpeience professionalExpeience = new ProfessionalExpeience()
            .company(DEFAULT_COMPANY)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .poste(DEFAULT_POSTE)
            .departement(DEFAULT_DEPARTEMENT)
            .store(DEFAULT_STORE);
        return professionalExpeience;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfessionalExpeience createUpdatedEntity(EntityManager em) {
        ProfessionalExpeience professionalExpeience = new ProfessionalExpeience()
            .company(UPDATED_COMPANY)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .poste(UPDATED_POSTE)
            .departement(UPDATED_DEPARTEMENT)
            .store(UPDATED_STORE);
        return professionalExpeience;
    }

    @BeforeEach
    public void initTest() {
        professionalExpeience = createEntity(em);
    }

    @Test
    @Transactional
    void createProfessionalExpeience() throws Exception {
        int databaseSizeBeforeCreate = professionalExpeienceRepository.findAll().size();
        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);
        restProfessionalExpeienceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeCreate + 1);
        ProfessionalExpeience testProfessionalExpeience = professionalExpeienceList.get(professionalExpeienceList.size() - 1);
        assertThat(testProfessionalExpeience.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testProfessionalExpeience.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testProfessionalExpeience.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testProfessionalExpeience.getPoste()).isEqualTo(DEFAULT_POSTE);
        assertThat(testProfessionalExpeience.getDepartement()).isEqualTo(DEFAULT_DEPARTEMENT);
        assertThat(testProfessionalExpeience.getStore()).isEqualTo(DEFAULT_STORE);
    }

    @Test
    @Transactional
    void createProfessionalExpeienceWithExistingId() throws Exception {
        // Create the ProfessionalExpeience with an existing ID
        professionalExpeience.setId(1L);
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        int databaseSizeBeforeCreate = professionalExpeienceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfessionalExpeienceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllProfessionalExpeiences() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        // Get all the professionalExpeienceList
        restProfessionalExpeienceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(professionalExpeience.getId().intValue())))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].poste").value(hasItem(DEFAULT_POSTE)))
            .andExpect(jsonPath("$.[*].departement").value(hasItem(DEFAULT_DEPARTEMENT)))
            .andExpect(jsonPath("$.[*].store").value(hasItem(DEFAULT_STORE)));
    }

    @Test
    @Transactional
    void getProfessionalExpeience() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        // Get the professionalExpeience
        restProfessionalExpeienceMockMvc
            .perform(get(ENTITY_API_URL_ID, professionalExpeience.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(professionalExpeience.getId().intValue()))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.poste").value(DEFAULT_POSTE))
            .andExpect(jsonPath("$.departement").value(DEFAULT_DEPARTEMENT))
            .andExpect(jsonPath("$.store").value(DEFAULT_STORE));
    }

    @Test
    @Transactional
    void getNonExistingProfessionalExpeience() throws Exception {
        // Get the professionalExpeience
        restProfessionalExpeienceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewProfessionalExpeience() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();

        // Update the professionalExpeience
        ProfessionalExpeience updatedProfessionalExpeience = professionalExpeienceRepository.findById(professionalExpeience.getId()).get();
        // Disconnect from session so that the updates on updatedProfessionalExpeience are not directly saved in db
        em.detach(updatedProfessionalExpeience);
        updatedProfessionalExpeience
            .company(UPDATED_COMPANY)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .poste(UPDATED_POSTE)
            .departement(UPDATED_DEPARTEMENT)
            .store(UPDATED_STORE);
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(updatedProfessionalExpeience);

        restProfessionalExpeienceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, professionalExpeienceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isOk());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
        ProfessionalExpeience testProfessionalExpeience = professionalExpeienceList.get(professionalExpeienceList.size() - 1);
        assertThat(testProfessionalExpeience.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testProfessionalExpeience.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testProfessionalExpeience.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testProfessionalExpeience.getPoste()).isEqualTo(UPDATED_POSTE);
        assertThat(testProfessionalExpeience.getDepartement()).isEqualTo(UPDATED_DEPARTEMENT);
        assertThat(testProfessionalExpeience.getStore()).isEqualTo(UPDATED_STORE);
    }

    @Test
    @Transactional
    void putNonExistingProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, professionalExpeienceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProfessionalExpeienceWithPatch() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();

        // Update the professionalExpeience using partial update
        ProfessionalExpeience partialUpdatedProfessionalExpeience = new ProfessionalExpeience();
        partialUpdatedProfessionalExpeience.setId(professionalExpeience.getId());

        partialUpdatedProfessionalExpeience.company(UPDATED_COMPANY).startDate(UPDATED_START_DATE).poste(UPDATED_POSTE);

        restProfessionalExpeienceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProfessionalExpeience.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProfessionalExpeience))
            )
            .andExpect(status().isOk());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
        ProfessionalExpeience testProfessionalExpeience = professionalExpeienceList.get(professionalExpeienceList.size() - 1);
        assertThat(testProfessionalExpeience.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testProfessionalExpeience.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testProfessionalExpeience.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testProfessionalExpeience.getPoste()).isEqualTo(UPDATED_POSTE);
        assertThat(testProfessionalExpeience.getDepartement()).isEqualTo(DEFAULT_DEPARTEMENT);
        assertThat(testProfessionalExpeience.getStore()).isEqualTo(DEFAULT_STORE);
    }

    @Test
    @Transactional
    void fullUpdateProfessionalExpeienceWithPatch() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();

        // Update the professionalExpeience using partial update
        ProfessionalExpeience partialUpdatedProfessionalExpeience = new ProfessionalExpeience();
        partialUpdatedProfessionalExpeience.setId(professionalExpeience.getId());

        partialUpdatedProfessionalExpeience
            .company(UPDATED_COMPANY)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .poste(UPDATED_POSTE)
            .departement(UPDATED_DEPARTEMENT)
            .store(UPDATED_STORE);

        restProfessionalExpeienceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProfessionalExpeience.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProfessionalExpeience))
            )
            .andExpect(status().isOk());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
        ProfessionalExpeience testProfessionalExpeience = professionalExpeienceList.get(professionalExpeienceList.size() - 1);
        assertThat(testProfessionalExpeience.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testProfessionalExpeience.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testProfessionalExpeience.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testProfessionalExpeience.getPoste()).isEqualTo(UPDATED_POSTE);
        assertThat(testProfessionalExpeience.getDepartement()).isEqualTo(UPDATED_DEPARTEMENT);
        assertThat(testProfessionalExpeience.getStore()).isEqualTo(UPDATED_STORE);
    }

    @Test
    @Transactional
    void patchNonExistingProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, professionalExpeienceDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProfessionalExpeience() throws Exception {
        int databaseSizeBeforeUpdate = professionalExpeienceRepository.findAll().size();
        professionalExpeience.setId(count.incrementAndGet());

        // Create the ProfessionalExpeience
        ProfessionalExpeienceDTO professionalExpeienceDTO = professionalExpeienceMapper.toDto(professionalExpeience);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProfessionalExpeienceMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(professionalExpeienceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProfessionalExpeience in the database
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProfessionalExpeience() throws Exception {
        // Initialize the database
        professionalExpeienceRepository.saveAndFlush(professionalExpeience);

        int databaseSizeBeforeDelete = professionalExpeienceRepository.findAll().size();

        // Delete the professionalExpeience
        restProfessionalExpeienceMockMvc
            .perform(delete(ENTITY_API_URL_ID, professionalExpeience.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProfessionalExpeience> professionalExpeienceList = professionalExpeienceRepository.findAll();
        assertThat(professionalExpeienceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
