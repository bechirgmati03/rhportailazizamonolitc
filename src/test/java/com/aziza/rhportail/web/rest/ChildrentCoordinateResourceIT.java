package com.aziza.rhportail.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.aziza.rhportail.IntegrationTest;
import com.aziza.rhportail.domain.ChildrentCoordinate;
import com.aziza.rhportail.repository.ChildrentCoordinateRepository;
import com.aziza.rhportail.service.dto.ChildrentCoordinateDTO;
import com.aziza.rhportail.service.mapper.ChildrentCoordinateMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChildrentCoordinateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ChildrentCoordinateResourceIT {

    private static final String DEFAULT_FIRST_NAME_CHILD = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_CHILD = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_CHILD = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_CHILD = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_OF_BIRTH_CHILD = "AAAAAAAAAA";
    private static final String UPDATED_DATE_OF_BIRTH_CHILD = "BBBBBBBBBB";

    private static final String DEFAULT_SCHOOL_LEVEL = "AAAAAAAAAA";
    private static final String UPDATED_SCHOOL_LEVEL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HANDICAPPED = false;
    private static final Boolean UPDATED_HANDICAPPED = true;

    private static final String ENTITY_API_URL = "/api/childrent-coordinates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ChildrentCoordinateRepository childrentCoordinateRepository;

    @Autowired
    private ChildrentCoordinateMapper childrentCoordinateMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChildrentCoordinateMockMvc;

    private ChildrentCoordinate childrentCoordinate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChildrentCoordinate createEntity(EntityManager em) {
        ChildrentCoordinate childrentCoordinate = new ChildrentCoordinate()
            .firstNameChild(DEFAULT_FIRST_NAME_CHILD)
            .lastNameChild(DEFAULT_LAST_NAME_CHILD)
            .dateOfBirthChild(DEFAULT_DATE_OF_BIRTH_CHILD)
            .schoolLevel(DEFAULT_SCHOOL_LEVEL)
            .handicapped(DEFAULT_HANDICAPPED);
        return childrentCoordinate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChildrentCoordinate createUpdatedEntity(EntityManager em) {
        ChildrentCoordinate childrentCoordinate = new ChildrentCoordinate()
            .firstNameChild(UPDATED_FIRST_NAME_CHILD)
            .lastNameChild(UPDATED_LAST_NAME_CHILD)
            .dateOfBirthChild(UPDATED_DATE_OF_BIRTH_CHILD)
            .schoolLevel(UPDATED_SCHOOL_LEVEL)
            .handicapped(UPDATED_HANDICAPPED);
        return childrentCoordinate;
    }

    @BeforeEach
    public void initTest() {
        childrentCoordinate = createEntity(em);
    }

    @Test
    @Transactional
    void createChildrentCoordinate() throws Exception {
        int databaseSizeBeforeCreate = childrentCoordinateRepository.findAll().size();
        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);
        restChildrentCoordinateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeCreate + 1);
        ChildrentCoordinate testChildrentCoordinate = childrentCoordinateList.get(childrentCoordinateList.size() - 1);
        assertThat(testChildrentCoordinate.getFirstNameChild()).isEqualTo(DEFAULT_FIRST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getLastNameChild()).isEqualTo(DEFAULT_LAST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getDateOfBirthChild()).isEqualTo(DEFAULT_DATE_OF_BIRTH_CHILD);
        assertThat(testChildrentCoordinate.getSchoolLevel()).isEqualTo(DEFAULT_SCHOOL_LEVEL);
        assertThat(testChildrentCoordinate.getHandicapped()).isEqualTo(DEFAULT_HANDICAPPED);
    }

    @Test
    @Transactional
    void createChildrentCoordinateWithExistingId() throws Exception {
        // Create the ChildrentCoordinate with an existing ID
        childrentCoordinate.setId(1L);
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        int databaseSizeBeforeCreate = childrentCoordinateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restChildrentCoordinateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllChildrentCoordinates() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        // Get all the childrentCoordinateList
        restChildrentCoordinateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(childrentCoordinate.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstNameChild").value(hasItem(DEFAULT_FIRST_NAME_CHILD)))
            .andExpect(jsonPath("$.[*].lastNameChild").value(hasItem(DEFAULT_LAST_NAME_CHILD)))
            .andExpect(jsonPath("$.[*].dateOfBirthChild").value(hasItem(DEFAULT_DATE_OF_BIRTH_CHILD)))
            .andExpect(jsonPath("$.[*].schoolLevel").value(hasItem(DEFAULT_SCHOOL_LEVEL)))
            .andExpect(jsonPath("$.[*].handicapped").value(hasItem(DEFAULT_HANDICAPPED.booleanValue())));
    }

    @Test
    @Transactional
    void getChildrentCoordinate() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        // Get the childrentCoordinate
        restChildrentCoordinateMockMvc
            .perform(get(ENTITY_API_URL_ID, childrentCoordinate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(childrentCoordinate.getId().intValue()))
            .andExpect(jsonPath("$.firstNameChild").value(DEFAULT_FIRST_NAME_CHILD))
            .andExpect(jsonPath("$.lastNameChild").value(DEFAULT_LAST_NAME_CHILD))
            .andExpect(jsonPath("$.dateOfBirthChild").value(DEFAULT_DATE_OF_BIRTH_CHILD))
            .andExpect(jsonPath("$.schoolLevel").value(DEFAULT_SCHOOL_LEVEL))
            .andExpect(jsonPath("$.handicapped").value(DEFAULT_HANDICAPPED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingChildrentCoordinate() throws Exception {
        // Get the childrentCoordinate
        restChildrentCoordinateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewChildrentCoordinate() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();

        // Update the childrentCoordinate
        ChildrentCoordinate updatedChildrentCoordinate = childrentCoordinateRepository.findById(childrentCoordinate.getId()).get();
        // Disconnect from session so that the updates on updatedChildrentCoordinate are not directly saved in db
        em.detach(updatedChildrentCoordinate);
        updatedChildrentCoordinate
            .firstNameChild(UPDATED_FIRST_NAME_CHILD)
            .lastNameChild(UPDATED_LAST_NAME_CHILD)
            .dateOfBirthChild(UPDATED_DATE_OF_BIRTH_CHILD)
            .schoolLevel(UPDATED_SCHOOL_LEVEL)
            .handicapped(UPDATED_HANDICAPPED);
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(updatedChildrentCoordinate);

        restChildrentCoordinateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, childrentCoordinateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isOk());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
        ChildrentCoordinate testChildrentCoordinate = childrentCoordinateList.get(childrentCoordinateList.size() - 1);
        assertThat(testChildrentCoordinate.getFirstNameChild()).isEqualTo(UPDATED_FIRST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getLastNameChild()).isEqualTo(UPDATED_LAST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getDateOfBirthChild()).isEqualTo(UPDATED_DATE_OF_BIRTH_CHILD);
        assertThat(testChildrentCoordinate.getSchoolLevel()).isEqualTo(UPDATED_SCHOOL_LEVEL);
        assertThat(testChildrentCoordinate.getHandicapped()).isEqualTo(UPDATED_HANDICAPPED);
    }

    @Test
    @Transactional
    void putNonExistingChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, childrentCoordinateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateChildrentCoordinateWithPatch() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();

        // Update the childrentCoordinate using partial update
        ChildrentCoordinate partialUpdatedChildrentCoordinate = new ChildrentCoordinate();
        partialUpdatedChildrentCoordinate.setId(childrentCoordinate.getId());

        partialUpdatedChildrentCoordinate
            .firstNameChild(UPDATED_FIRST_NAME_CHILD)
            .lastNameChild(UPDATED_LAST_NAME_CHILD)
            .handicapped(UPDATED_HANDICAPPED);

        restChildrentCoordinateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChildrentCoordinate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChildrentCoordinate))
            )
            .andExpect(status().isOk());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
        ChildrentCoordinate testChildrentCoordinate = childrentCoordinateList.get(childrentCoordinateList.size() - 1);
        assertThat(testChildrentCoordinate.getFirstNameChild()).isEqualTo(UPDATED_FIRST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getLastNameChild()).isEqualTo(UPDATED_LAST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getDateOfBirthChild()).isEqualTo(DEFAULT_DATE_OF_BIRTH_CHILD);
        assertThat(testChildrentCoordinate.getSchoolLevel()).isEqualTo(DEFAULT_SCHOOL_LEVEL);
        assertThat(testChildrentCoordinate.getHandicapped()).isEqualTo(UPDATED_HANDICAPPED);
    }

    @Test
    @Transactional
    void fullUpdateChildrentCoordinateWithPatch() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();

        // Update the childrentCoordinate using partial update
        ChildrentCoordinate partialUpdatedChildrentCoordinate = new ChildrentCoordinate();
        partialUpdatedChildrentCoordinate.setId(childrentCoordinate.getId());

        partialUpdatedChildrentCoordinate
            .firstNameChild(UPDATED_FIRST_NAME_CHILD)
            .lastNameChild(UPDATED_LAST_NAME_CHILD)
            .dateOfBirthChild(UPDATED_DATE_OF_BIRTH_CHILD)
            .schoolLevel(UPDATED_SCHOOL_LEVEL)
            .handicapped(UPDATED_HANDICAPPED);

        restChildrentCoordinateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedChildrentCoordinate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedChildrentCoordinate))
            )
            .andExpect(status().isOk());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
        ChildrentCoordinate testChildrentCoordinate = childrentCoordinateList.get(childrentCoordinateList.size() - 1);
        assertThat(testChildrentCoordinate.getFirstNameChild()).isEqualTo(UPDATED_FIRST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getLastNameChild()).isEqualTo(UPDATED_LAST_NAME_CHILD);
        assertThat(testChildrentCoordinate.getDateOfBirthChild()).isEqualTo(UPDATED_DATE_OF_BIRTH_CHILD);
        assertThat(testChildrentCoordinate.getSchoolLevel()).isEqualTo(UPDATED_SCHOOL_LEVEL);
        assertThat(testChildrentCoordinate.getHandicapped()).isEqualTo(UPDATED_HANDICAPPED);
    }

    @Test
    @Transactional
    void patchNonExistingChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, childrentCoordinateDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamChildrentCoordinate() throws Exception {
        int databaseSizeBeforeUpdate = childrentCoordinateRepository.findAll().size();
        childrentCoordinate.setId(count.incrementAndGet());

        // Create the ChildrentCoordinate
        ChildrentCoordinateDTO childrentCoordinateDTO = childrentCoordinateMapper.toDto(childrentCoordinate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restChildrentCoordinateMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(childrentCoordinateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ChildrentCoordinate in the database
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteChildrentCoordinate() throws Exception {
        // Initialize the database
        childrentCoordinateRepository.saveAndFlush(childrentCoordinate);

        int databaseSizeBeforeDelete = childrentCoordinateRepository.findAll().size();

        // Delete the childrentCoordinate
        restChildrentCoordinateMockMvc
            .perform(delete(ENTITY_API_URL_ID, childrentCoordinate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ChildrentCoordinate> childrentCoordinateList = childrentCoordinateRepository.findAll();
        assertThat(childrentCoordinateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
