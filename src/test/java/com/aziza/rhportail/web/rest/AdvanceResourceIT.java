package com.aziza.rhportail.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.aziza.rhportail.IntegrationTest;
import com.aziza.rhportail.domain.Advance;
import com.aziza.rhportail.repository.AdvanceRepository;
import com.aziza.rhportail.service.dto.AdvanceDTO;
import com.aziza.rhportail.service.mapper.AdvanceMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AdvanceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AdvanceResourceIT {

    private static final String DEFAULT_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_REFERENCE = "BBBBBBBBBB";

    private static final String DEFAULT_SALARY_ADVANCE_REASON = "AAAAAAAAAA";
    private static final String UPDATED_SALARY_ADVANCE_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_SALARY_ADVANCE_REASON_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_SALARY_ADVANCE_REASON_DETAILS = "BBBBBBBBBB";

    private static final String DEFAULT_SALARY_ADVANCE_TYPES = "AAAAAAAAAA";
    private static final String UPDATED_SALARY_ADVANCE_TYPES = "BBBBBBBBBB";

    private static final Double DEFAULT_CEILING_SALARY_ADVANCE_TYPES = 1D;
    private static final Double UPDATED_CEILING_SALARY_ADVANCE_TYPES = 2D;

    private static final String DEFAULT_DEGREE_OF_URGENCY = "AAAAAAAAAA";
    private static final String UPDATED_DEGREE_OF_URGENCY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_MAXIMUM_SALARY_ADVANCE = 1D;
    private static final Double UPDATED_MAXIMUM_SALARY_ADVANCE = 2D;

    private static final Double DEFAULT_SALARY_ADVANCE_AMOUNT = 1D;
    private static final Double UPDATED_SALARY_ADVANCE_AMOUNT = 2D;

    private static final Double DEFAULT_REPAYMENT_PERIOD = 1D;
    private static final Double UPDATED_REPAYMENT_PERIOD = 2D;

    private static final String DEFAULT_SALARY_DEDUCTION_METHOD = "AAAAAAAAAA";
    private static final String UPDATED_SALARY_DEDUCTION_METHOD = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE = 1D;
    private static final Double UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE = 2D;

    private static final Double DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE = 1D;
    private static final Double UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE = 2D;

    private static final Double DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE = 1D;
    private static final Double UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE = 2D;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REASON_REJECTION = "AAAAAAAAAA";
    private static final String UPDATED_REASON_REJECTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/advances";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AdvanceRepository advanceRepository;

    @Autowired
    private AdvanceMapper advanceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdvanceMockMvc;

    private Advance advance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advance createEntity(EntityManager em) {
        Advance advance = new Advance()
            .reference(DEFAULT_REFERENCE)
            .salaryAdvanceReason(DEFAULT_SALARY_ADVANCE_REASON)
            .salaryAdvanceReasonDetails(DEFAULT_SALARY_ADVANCE_REASON_DETAILS)
            .salaryAdvanceTypes(DEFAULT_SALARY_ADVANCE_TYPES)
            .ceilingSalaryAdvanceTypes(DEFAULT_CEILING_SALARY_ADVANCE_TYPES)
            .degreeOfUrgency(DEFAULT_DEGREE_OF_URGENCY)
            .date(DEFAULT_DATE)
            .maximumSalaryAdvance(DEFAULT_MAXIMUM_SALARY_ADVANCE)
            .salaryAdvanceAmount(DEFAULT_SALARY_ADVANCE_AMOUNT)
            .repaymentPeriod(DEFAULT_REPAYMENT_PERIOD)
            .salaryDeductionMethod(DEFAULT_SALARY_DEDUCTION_METHOD)
            .totalAmountMonthlyPaymentDeducte(DEFAULT_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE)
            .salaryMonthlyPaymentDeducte(DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE)
            .primesMonthlyPaymentDeducte(DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE)
            .status(DEFAULT_STATUS)
            .reasonRejection(DEFAULT_REASON_REJECTION);
        return advance;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advance createUpdatedEntity(EntityManager em) {
        Advance advance = new Advance()
            .reference(UPDATED_REFERENCE)
            .salaryAdvanceReason(UPDATED_SALARY_ADVANCE_REASON)
            .salaryAdvanceReasonDetails(UPDATED_SALARY_ADVANCE_REASON_DETAILS)
            .salaryAdvanceTypes(UPDATED_SALARY_ADVANCE_TYPES)
            .ceilingSalaryAdvanceTypes(UPDATED_CEILING_SALARY_ADVANCE_TYPES)
            .degreeOfUrgency(UPDATED_DEGREE_OF_URGENCY)
            .date(UPDATED_DATE)
            .maximumSalaryAdvance(UPDATED_MAXIMUM_SALARY_ADVANCE)
            .salaryAdvanceAmount(UPDATED_SALARY_ADVANCE_AMOUNT)
            .repaymentPeriod(UPDATED_REPAYMENT_PERIOD)
            .salaryDeductionMethod(UPDATED_SALARY_DEDUCTION_METHOD)
            .totalAmountMonthlyPaymentDeducte(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE)
            .salaryMonthlyPaymentDeducte(UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE)
            .primesMonthlyPaymentDeducte(UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE)
            .status(UPDATED_STATUS)
            .reasonRejection(UPDATED_REASON_REJECTION);
        return advance;
    }

    @BeforeEach
    public void initTest() {
        advance = createEntity(em);
    }

    @Test
    @Transactional
    void createAdvance() throws Exception {
        int databaseSizeBeforeCreate = advanceRepository.findAll().size();
        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);
        restAdvanceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(advanceDTO)))
            .andExpect(status().isCreated());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeCreate + 1);
        Advance testAdvance = advanceList.get(advanceList.size() - 1);
        assertThat(testAdvance.getReference()).isEqualTo(DEFAULT_REFERENCE);
        assertThat(testAdvance.getSalaryAdvanceReason()).isEqualTo(DEFAULT_SALARY_ADVANCE_REASON);
        assertThat(testAdvance.getSalaryAdvanceReasonDetails()).isEqualTo(DEFAULT_SALARY_ADVANCE_REASON_DETAILS);
        assertThat(testAdvance.getSalaryAdvanceTypes()).isEqualTo(DEFAULT_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getCeilingSalaryAdvanceTypes()).isEqualTo(DEFAULT_CEILING_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getDegreeOfUrgency()).isEqualTo(DEFAULT_DEGREE_OF_URGENCY);
        assertThat(testAdvance.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testAdvance.getMaximumSalaryAdvance()).isEqualTo(DEFAULT_MAXIMUM_SALARY_ADVANCE);
        assertThat(testAdvance.getSalaryAdvanceAmount()).isEqualTo(DEFAULT_SALARY_ADVANCE_AMOUNT);
        assertThat(testAdvance.getRepaymentPeriod()).isEqualTo(DEFAULT_REPAYMENT_PERIOD);
        assertThat(testAdvance.getSalaryDeductionMethod()).isEqualTo(DEFAULT_SALARY_DEDUCTION_METHOD);
        assertThat(testAdvance.getTotalAmountMonthlyPaymentDeducte()).isEqualTo(DEFAULT_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getSalaryMonthlyPaymentDeducte()).isEqualTo(DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getPrimesMonthlyPaymentDeducte()).isEqualTo(DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAdvance.getReasonRejection()).isEqualTo(DEFAULT_REASON_REJECTION);
    }

    @Test
    @Transactional
    void createAdvanceWithExistingId() throws Exception {
        // Create the Advance with an existing ID
        advance.setId(1L);
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        int databaseSizeBeforeCreate = advanceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvanceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(advanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAdvances() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        // Get all the advanceList
        restAdvanceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advance.getId().intValue())))
            .andExpect(jsonPath("$.[*].reference").value(hasItem(DEFAULT_REFERENCE)))
            .andExpect(jsonPath("$.[*].salaryAdvanceReason").value(hasItem(DEFAULT_SALARY_ADVANCE_REASON)))
            .andExpect(jsonPath("$.[*].salaryAdvanceReasonDetails").value(hasItem(DEFAULT_SALARY_ADVANCE_REASON_DETAILS)))
            .andExpect(jsonPath("$.[*].salaryAdvanceTypes").value(hasItem(DEFAULT_SALARY_ADVANCE_TYPES)))
            .andExpect(jsonPath("$.[*].ceilingSalaryAdvanceTypes").value(hasItem(DEFAULT_CEILING_SALARY_ADVANCE_TYPES.doubleValue())))
            .andExpect(jsonPath("$.[*].degreeOfUrgency").value(hasItem(DEFAULT_DEGREE_OF_URGENCY)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].maximumSalaryAdvance").value(hasItem(DEFAULT_MAXIMUM_SALARY_ADVANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].salaryAdvanceAmount").value(hasItem(DEFAULT_SALARY_ADVANCE_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].repaymentPeriod").value(hasItem(DEFAULT_REPAYMENT_PERIOD.doubleValue())))
            .andExpect(jsonPath("$.[*].salaryDeductionMethod").value(hasItem(DEFAULT_SALARY_DEDUCTION_METHOD)))
            .andExpect(
                jsonPath("$.[*].totalAmountMonthlyPaymentDeducte")
                    .value(hasItem(DEFAULT_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE.doubleValue()))
            )
            .andExpect(jsonPath("$.[*].salaryMonthlyPaymentDeducte").value(hasItem(DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE.doubleValue())))
            .andExpect(jsonPath("$.[*].primesMonthlyPaymentDeducte").value(hasItem(DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].reasonRejection").value(hasItem(DEFAULT_REASON_REJECTION)));
    }

    @Test
    @Transactional
    void getAdvance() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        // Get the advance
        restAdvanceMockMvc
            .perform(get(ENTITY_API_URL_ID, advance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(advance.getId().intValue()))
            .andExpect(jsonPath("$.reference").value(DEFAULT_REFERENCE))
            .andExpect(jsonPath("$.salaryAdvanceReason").value(DEFAULT_SALARY_ADVANCE_REASON))
            .andExpect(jsonPath("$.salaryAdvanceReasonDetails").value(DEFAULT_SALARY_ADVANCE_REASON_DETAILS))
            .andExpect(jsonPath("$.salaryAdvanceTypes").value(DEFAULT_SALARY_ADVANCE_TYPES))
            .andExpect(jsonPath("$.ceilingSalaryAdvanceTypes").value(DEFAULT_CEILING_SALARY_ADVANCE_TYPES.doubleValue()))
            .andExpect(jsonPath("$.degreeOfUrgency").value(DEFAULT_DEGREE_OF_URGENCY))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.maximumSalaryAdvance").value(DEFAULT_MAXIMUM_SALARY_ADVANCE.doubleValue()))
            .andExpect(jsonPath("$.salaryAdvanceAmount").value(DEFAULT_SALARY_ADVANCE_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.repaymentPeriod").value(DEFAULT_REPAYMENT_PERIOD.doubleValue()))
            .andExpect(jsonPath("$.salaryDeductionMethod").value(DEFAULT_SALARY_DEDUCTION_METHOD))
            .andExpect(jsonPath("$.totalAmountMonthlyPaymentDeducte").value(DEFAULT_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE.doubleValue()))
            .andExpect(jsonPath("$.salaryMonthlyPaymentDeducte").value(DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE.doubleValue()))
            .andExpect(jsonPath("$.primesMonthlyPaymentDeducte").value(DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.reasonRejection").value(DEFAULT_REASON_REJECTION));
    }

    @Test
    @Transactional
    void getNonExistingAdvance() throws Exception {
        // Get the advance
        restAdvanceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAdvance() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();

        // Update the advance
        Advance updatedAdvance = advanceRepository.findById(advance.getId()).get();
        // Disconnect from session so that the updates on updatedAdvance are not directly saved in db
        em.detach(updatedAdvance);
        updatedAdvance
            .reference(UPDATED_REFERENCE)
            .salaryAdvanceReason(UPDATED_SALARY_ADVANCE_REASON)
            .salaryAdvanceReasonDetails(UPDATED_SALARY_ADVANCE_REASON_DETAILS)
            .salaryAdvanceTypes(UPDATED_SALARY_ADVANCE_TYPES)
            .ceilingSalaryAdvanceTypes(UPDATED_CEILING_SALARY_ADVANCE_TYPES)
            .degreeOfUrgency(UPDATED_DEGREE_OF_URGENCY)
            .date(UPDATED_DATE)
            .maximumSalaryAdvance(UPDATED_MAXIMUM_SALARY_ADVANCE)
            .salaryAdvanceAmount(UPDATED_SALARY_ADVANCE_AMOUNT)
            .repaymentPeriod(UPDATED_REPAYMENT_PERIOD)
            .salaryDeductionMethod(UPDATED_SALARY_DEDUCTION_METHOD)
            .totalAmountMonthlyPaymentDeducte(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE)
            .salaryMonthlyPaymentDeducte(UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE)
            .primesMonthlyPaymentDeducte(UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE)
            .status(UPDATED_STATUS)
            .reasonRejection(UPDATED_REASON_REJECTION);
        AdvanceDTO advanceDTO = advanceMapper.toDto(updatedAdvance);

        restAdvanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, advanceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isOk());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
        Advance testAdvance = advanceList.get(advanceList.size() - 1);
        assertThat(testAdvance.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testAdvance.getSalaryAdvanceReason()).isEqualTo(UPDATED_SALARY_ADVANCE_REASON);
        assertThat(testAdvance.getSalaryAdvanceReasonDetails()).isEqualTo(UPDATED_SALARY_ADVANCE_REASON_DETAILS);
        assertThat(testAdvance.getSalaryAdvanceTypes()).isEqualTo(UPDATED_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getCeilingSalaryAdvanceTypes()).isEqualTo(UPDATED_CEILING_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getDegreeOfUrgency()).isEqualTo(UPDATED_DEGREE_OF_URGENCY);
        assertThat(testAdvance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAdvance.getMaximumSalaryAdvance()).isEqualTo(UPDATED_MAXIMUM_SALARY_ADVANCE);
        assertThat(testAdvance.getSalaryAdvanceAmount()).isEqualTo(UPDATED_SALARY_ADVANCE_AMOUNT);
        assertThat(testAdvance.getRepaymentPeriod()).isEqualTo(UPDATED_REPAYMENT_PERIOD);
        assertThat(testAdvance.getSalaryDeductionMethod()).isEqualTo(UPDATED_SALARY_DEDUCTION_METHOD);
        assertThat(testAdvance.getTotalAmountMonthlyPaymentDeducte()).isEqualTo(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getSalaryMonthlyPaymentDeducte()).isEqualTo(UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getPrimesMonthlyPaymentDeducte()).isEqualTo(UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAdvance.getReasonRejection()).isEqualTo(UPDATED_REASON_REJECTION);
    }

    @Test
    @Transactional
    void putNonExistingAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, advanceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(advanceDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAdvanceWithPatch() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();

        // Update the advance using partial update
        Advance partialUpdatedAdvance = new Advance();
        partialUpdatedAdvance.setId(advance.getId());

        partialUpdatedAdvance
            .reference(UPDATED_REFERENCE)
            .salaryAdvanceReasonDetails(UPDATED_SALARY_ADVANCE_REASON_DETAILS)
            .ceilingSalaryAdvanceTypes(UPDATED_CEILING_SALARY_ADVANCE_TYPES)
            .date(UPDATED_DATE)
            .maximumSalaryAdvance(UPDATED_MAXIMUM_SALARY_ADVANCE)
            .repaymentPeriod(UPDATED_REPAYMENT_PERIOD)
            .totalAmountMonthlyPaymentDeducte(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE)
            .reasonRejection(UPDATED_REASON_REJECTION);

        restAdvanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAdvance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAdvance))
            )
            .andExpect(status().isOk());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
        Advance testAdvance = advanceList.get(advanceList.size() - 1);
        assertThat(testAdvance.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testAdvance.getSalaryAdvanceReason()).isEqualTo(DEFAULT_SALARY_ADVANCE_REASON);
        assertThat(testAdvance.getSalaryAdvanceReasonDetails()).isEqualTo(UPDATED_SALARY_ADVANCE_REASON_DETAILS);
        assertThat(testAdvance.getSalaryAdvanceTypes()).isEqualTo(DEFAULT_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getCeilingSalaryAdvanceTypes()).isEqualTo(UPDATED_CEILING_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getDegreeOfUrgency()).isEqualTo(DEFAULT_DEGREE_OF_URGENCY);
        assertThat(testAdvance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAdvance.getMaximumSalaryAdvance()).isEqualTo(UPDATED_MAXIMUM_SALARY_ADVANCE);
        assertThat(testAdvance.getSalaryAdvanceAmount()).isEqualTo(DEFAULT_SALARY_ADVANCE_AMOUNT);
        assertThat(testAdvance.getRepaymentPeriod()).isEqualTo(UPDATED_REPAYMENT_PERIOD);
        assertThat(testAdvance.getSalaryDeductionMethod()).isEqualTo(DEFAULT_SALARY_DEDUCTION_METHOD);
        assertThat(testAdvance.getTotalAmountMonthlyPaymentDeducte()).isEqualTo(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getSalaryMonthlyPaymentDeducte()).isEqualTo(DEFAULT_SALARY_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getPrimesMonthlyPaymentDeducte()).isEqualTo(DEFAULT_PRIMES_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAdvance.getReasonRejection()).isEqualTo(UPDATED_REASON_REJECTION);
    }

    @Test
    @Transactional
    void fullUpdateAdvanceWithPatch() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();

        // Update the advance using partial update
        Advance partialUpdatedAdvance = new Advance();
        partialUpdatedAdvance.setId(advance.getId());

        partialUpdatedAdvance
            .reference(UPDATED_REFERENCE)
            .salaryAdvanceReason(UPDATED_SALARY_ADVANCE_REASON)
            .salaryAdvanceReasonDetails(UPDATED_SALARY_ADVANCE_REASON_DETAILS)
            .salaryAdvanceTypes(UPDATED_SALARY_ADVANCE_TYPES)
            .ceilingSalaryAdvanceTypes(UPDATED_CEILING_SALARY_ADVANCE_TYPES)
            .degreeOfUrgency(UPDATED_DEGREE_OF_URGENCY)
            .date(UPDATED_DATE)
            .maximumSalaryAdvance(UPDATED_MAXIMUM_SALARY_ADVANCE)
            .salaryAdvanceAmount(UPDATED_SALARY_ADVANCE_AMOUNT)
            .repaymentPeriod(UPDATED_REPAYMENT_PERIOD)
            .salaryDeductionMethod(UPDATED_SALARY_DEDUCTION_METHOD)
            .totalAmountMonthlyPaymentDeducte(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE)
            .salaryMonthlyPaymentDeducte(UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE)
            .primesMonthlyPaymentDeducte(UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE)
            .status(UPDATED_STATUS)
            .reasonRejection(UPDATED_REASON_REJECTION);

        restAdvanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAdvance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAdvance))
            )
            .andExpect(status().isOk());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
        Advance testAdvance = advanceList.get(advanceList.size() - 1);
        assertThat(testAdvance.getReference()).isEqualTo(UPDATED_REFERENCE);
        assertThat(testAdvance.getSalaryAdvanceReason()).isEqualTo(UPDATED_SALARY_ADVANCE_REASON);
        assertThat(testAdvance.getSalaryAdvanceReasonDetails()).isEqualTo(UPDATED_SALARY_ADVANCE_REASON_DETAILS);
        assertThat(testAdvance.getSalaryAdvanceTypes()).isEqualTo(UPDATED_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getCeilingSalaryAdvanceTypes()).isEqualTo(UPDATED_CEILING_SALARY_ADVANCE_TYPES);
        assertThat(testAdvance.getDegreeOfUrgency()).isEqualTo(UPDATED_DEGREE_OF_URGENCY);
        assertThat(testAdvance.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAdvance.getMaximumSalaryAdvance()).isEqualTo(UPDATED_MAXIMUM_SALARY_ADVANCE);
        assertThat(testAdvance.getSalaryAdvanceAmount()).isEqualTo(UPDATED_SALARY_ADVANCE_AMOUNT);
        assertThat(testAdvance.getRepaymentPeriod()).isEqualTo(UPDATED_REPAYMENT_PERIOD);
        assertThat(testAdvance.getSalaryDeductionMethod()).isEqualTo(UPDATED_SALARY_DEDUCTION_METHOD);
        assertThat(testAdvance.getTotalAmountMonthlyPaymentDeducte()).isEqualTo(UPDATED_TOTAL_AMOUNT_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getSalaryMonthlyPaymentDeducte()).isEqualTo(UPDATED_SALARY_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getPrimesMonthlyPaymentDeducte()).isEqualTo(UPDATED_PRIMES_MONTHLY_PAYMENT_DEDUCTE);
        assertThat(testAdvance.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAdvance.getReasonRejection()).isEqualTo(UPDATED_REASON_REJECTION);
    }

    @Test
    @Transactional
    void patchNonExistingAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, advanceDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAdvance() throws Exception {
        int databaseSizeBeforeUpdate = advanceRepository.findAll().size();
        advance.setId(count.incrementAndGet());

        // Create the Advance
        AdvanceDTO advanceDTO = advanceMapper.toDto(advance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAdvanceMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(advanceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Advance in the database
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAdvance() throws Exception {
        // Initialize the database
        advanceRepository.saveAndFlush(advance);

        int databaseSizeBeforeDelete = advanceRepository.findAll().size();

        // Delete the advance
        restAdvanceMockMvc
            .perform(delete(ENTITY_API_URL_ID, advance.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Advance> advanceList = advanceRepository.findAll();
        assertThat(advanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
