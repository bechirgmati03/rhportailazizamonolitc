package com.aziza.rhportail.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.aziza.rhportail.IntegrationTest;
import com.aziza.rhportail.domain.Members;
import com.aziza.rhportail.repository.MembersRepository;
import com.aziza.rhportail.service.dto.MembersDTO;
import com.aziza.rhportail.service.mapper.MembersMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MembersResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MembersResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BRITH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BRITH = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CIVILITY = "AAAAAAAAAA";
    private static final String UPDATED_CIVILITY = "BBBBBBBBBB";

    private static final String DEFAULT_PLACE_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_PLACE_OF_BIRTH = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME_FATHER = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_FATHER = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME_MATHER = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_MATHER = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_FATHER = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_FATHER = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_MATHER = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_MATHER = "BBBBBBBBBB";

    private static final String DEFAULT_SITE = "AAAAAAAAAA";
    private static final String UPDATED_SITE = "BBBBBBBBBB";

    private static final String DEFAULT_POSITION_HELD = "AAAAAAAAAA";
    private static final String UPDATED_POSITION_HELD = "BBBBBBBBBB";

    private static final String DEFAULT_MATRICULE_OF_HIERARCHICAL_CHIEF = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_HIERARCHICAL_CHIEF = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_HIERARCHICAL_CHIEF = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTITY_DOCUMENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTITY_DOCUMENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NUMBER_IDENTITY_DOCUEMENT = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER_IDENTITY_DOCUEMENT = "BBBBBBBBBB";

    private static final String DEFAULT_NUMBER_SOCIAL_SECURITY = "AAAAAAAAAA";
    private static final String UPDATED_NUMBER_SOCIAL_SECURITY = "BBBBBBBBBB";

    private static final String DEFAULT_PERSONEL_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_PERSONEL_ADRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESS_OF_NEXT_OF_KIN = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS_OF_NEXT_OF_KIN = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_ADRESS = "BBBBBBBBBB";

    private static final Long DEFAULT_HOME_PHONE = 1L;
    private static final Long UPDATED_HOME_PHONE = 2L;

    private static final Long DEFAULT_PERSONAL_PHONE = 1L;
    private static final Long UPDATED_PERSONAL_PHONE = 2L;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HOUSE_HOLDER = false;
    private static final Boolean UPDATED_HOUSE_HOLDER = true;

    private static final Integer DEFAULT_DEPENDENTS = 1;
    private static final Integer UPDATED_DEPENDENTS = 2;

    private static final Boolean DEFAULT_DRIVING_PERMIT = false;
    private static final Boolean UPDATED_DRIVING_PERMIT = true;

    private static final String DEFAULT_FIRST_NAME_SPOUSSE = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_SPOUSSE = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_SPOUSSE = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_SPOUSSE = "BBBBBBBBBB";

    private static final Long DEFAULT_PHONE_SPOUSSE = 1L;
    private static final Long UPDATED_PHONE_SPOUSSE = 2L;

    private static final String DEFAULT_NAME_EMERGENCY_CONTACT = "AAAAAAAAAA";
    private static final String UPDATED_NAME_EMERGENCY_CONTACT = "BBBBBBBBBB";

    private static final Long DEFAULT_PHONE_EMERGENCY_CONTACT = 1L;
    private static final Long UPDATED_PHONE_EMERGENCY_CONTACT = 2L;

    private static final Boolean DEFAULT_CHRONIC_DISEASE = false;
    private static final Boolean UPDATED_CHRONIC_DISEASE = true;

    private static final String DEFAULT_LEVEL_OF_STUDY = "AAAAAAAAAA";
    private static final String UPDATED_LEVEL_OF_STUDY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/members";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MembersRepository membersRepository;

    @Autowired
    private MembersMapper membersMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMembersMockMvc;

    private Members members;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Members createEntity(EntityManager em) {
        Members members = new Members()
            .matricule(DEFAULT_MATRICULE)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .dateOfBrith(DEFAULT_DATE_OF_BRITH)
            .civility(DEFAULT_CIVILITY)
            .placeOfBirth(DEFAULT_PLACE_OF_BIRTH)
            .nationality(DEFAULT_NATIONALITY)
            .firstNameFather(DEFAULT_FIRST_NAME_FATHER)
            .firstNameMather(DEFAULT_FIRST_NAME_MATHER)
            .lastNameFather(DEFAULT_LAST_NAME_FATHER)
            .lastNameMather(DEFAULT_LAST_NAME_MATHER)
            .site(DEFAULT_SITE)
            .positionHeld(DEFAULT_POSITION_HELD)
            .matriculeOfHierarchicalChief(DEFAULT_MATRICULE_OF_HIERARCHICAL_CHIEF)
            .firstNameHierarchicalChief(DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF)
            .lastNameHierarchicalChief(DEFAULT_LAST_NAME_HIERARCHICAL_CHIEF)
            .identityDocumentType(DEFAULT_IDENTITY_DOCUMENT_TYPE)
            .numberIdentityDocuement(DEFAULT_NUMBER_IDENTITY_DOCUEMENT)
            .numberSocialSecurity(DEFAULT_NUMBER_SOCIAL_SECURITY)
            .personelAdress(DEFAULT_PERSONEL_ADRESS)
            .adressOfNextOfKin(DEFAULT_ADRESS_OF_NEXT_OF_KIN)
            .mailAdress(DEFAULT_MAIL_ADRESS)
            .homePhone(DEFAULT_HOME_PHONE)
            .personalPhone(DEFAULT_PERSONAL_PHONE)
            .status(DEFAULT_STATUS)
            .houseHolder(DEFAULT_HOUSE_HOLDER)
            .dependents(DEFAULT_DEPENDENTS)
            .drivingPermit(DEFAULT_DRIVING_PERMIT)
            .firstNameSpousse(DEFAULT_FIRST_NAME_SPOUSSE)
            .lastNameSpousse(DEFAULT_LAST_NAME_SPOUSSE)
            .phoneSpousse(DEFAULT_PHONE_SPOUSSE)
            .nameEmergencyContact(DEFAULT_NAME_EMERGENCY_CONTACT)
            .phoneEmergencyContact(DEFAULT_PHONE_EMERGENCY_CONTACT)
            .chronicDisease(DEFAULT_CHRONIC_DISEASE)
            .levelOfStudy(DEFAULT_LEVEL_OF_STUDY);
        return members;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Members createUpdatedEntity(EntityManager em) {
        Members members = new Members()
            .matricule(UPDATED_MATRICULE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .dateOfBrith(UPDATED_DATE_OF_BRITH)
            .civility(UPDATED_CIVILITY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .nationality(UPDATED_NATIONALITY)
            .firstNameFather(UPDATED_FIRST_NAME_FATHER)
            .firstNameMather(UPDATED_FIRST_NAME_MATHER)
            .lastNameFather(UPDATED_LAST_NAME_FATHER)
            .lastNameMather(UPDATED_LAST_NAME_MATHER)
            .site(UPDATED_SITE)
            .positionHeld(UPDATED_POSITION_HELD)
            .matriculeOfHierarchicalChief(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF)
            .firstNameHierarchicalChief(UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF)
            .lastNameHierarchicalChief(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF)
            .identityDocumentType(UPDATED_IDENTITY_DOCUMENT_TYPE)
            .numberIdentityDocuement(UPDATED_NUMBER_IDENTITY_DOCUEMENT)
            .numberSocialSecurity(UPDATED_NUMBER_SOCIAL_SECURITY)
            .personelAdress(UPDATED_PERSONEL_ADRESS)
            .adressOfNextOfKin(UPDATED_ADRESS_OF_NEXT_OF_KIN)
            .mailAdress(UPDATED_MAIL_ADRESS)
            .homePhone(UPDATED_HOME_PHONE)
            .personalPhone(UPDATED_PERSONAL_PHONE)
            .status(UPDATED_STATUS)
            .houseHolder(UPDATED_HOUSE_HOLDER)
            .dependents(UPDATED_DEPENDENTS)
            .drivingPermit(UPDATED_DRIVING_PERMIT)
            .firstNameSpousse(UPDATED_FIRST_NAME_SPOUSSE)
            .lastNameSpousse(UPDATED_LAST_NAME_SPOUSSE)
            .phoneSpousse(UPDATED_PHONE_SPOUSSE)
            .nameEmergencyContact(UPDATED_NAME_EMERGENCY_CONTACT)
            .phoneEmergencyContact(UPDATED_PHONE_EMERGENCY_CONTACT)
            .chronicDisease(UPDATED_CHRONIC_DISEASE)
            .levelOfStudy(UPDATED_LEVEL_OF_STUDY);
        return members;
    }

    @BeforeEach
    public void initTest() {
        members = createEntity(em);
    }

    @Test
    @Transactional
    void createMembers() throws Exception {
        int databaseSizeBeforeCreate = membersRepository.findAll().size();
        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);
        restMembersMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membersDTO)))
            .andExpect(status().isCreated());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeCreate + 1);
        Members testMembers = membersList.get(membersList.size() - 1);
        assertThat(testMembers.getMatricule()).isEqualTo(DEFAULT_MATRICULE);
        assertThat(testMembers.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testMembers.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testMembers.getDateOfBrith()).isEqualTo(DEFAULT_DATE_OF_BRITH);
        assertThat(testMembers.getCivility()).isEqualTo(DEFAULT_CIVILITY);
        assertThat(testMembers.getPlaceOfBirth()).isEqualTo(DEFAULT_PLACE_OF_BIRTH);
        assertThat(testMembers.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testMembers.getFirstNameFather()).isEqualTo(DEFAULT_FIRST_NAME_FATHER);
        assertThat(testMembers.getFirstNameMather()).isEqualTo(DEFAULT_FIRST_NAME_MATHER);
        assertThat(testMembers.getLastNameFather()).isEqualTo(DEFAULT_LAST_NAME_FATHER);
        assertThat(testMembers.getLastNameMather()).isEqualTo(DEFAULT_LAST_NAME_MATHER);
        assertThat(testMembers.getSite()).isEqualTo(DEFAULT_SITE);
        assertThat(testMembers.getPositionHeld()).isEqualTo(DEFAULT_POSITION_HELD);
        assertThat(testMembers.getMatriculeOfHierarchicalChief()).isEqualTo(DEFAULT_MATRICULE_OF_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getFirstNameHierarchicalChief()).isEqualTo(DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getLastNameHierarchicalChief()).isEqualTo(DEFAULT_LAST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getIdentityDocumentType()).isEqualTo(DEFAULT_IDENTITY_DOCUMENT_TYPE);
        assertThat(testMembers.getNumberIdentityDocuement()).isEqualTo(DEFAULT_NUMBER_IDENTITY_DOCUEMENT);
        assertThat(testMembers.getNumberSocialSecurity()).isEqualTo(DEFAULT_NUMBER_SOCIAL_SECURITY);
        assertThat(testMembers.getPersonelAdress()).isEqualTo(DEFAULT_PERSONEL_ADRESS);
        assertThat(testMembers.getAdressOfNextOfKin()).isEqualTo(DEFAULT_ADRESS_OF_NEXT_OF_KIN);
        assertThat(testMembers.getMailAdress()).isEqualTo(DEFAULT_MAIL_ADRESS);
        assertThat(testMembers.getHomePhone()).isEqualTo(DEFAULT_HOME_PHONE);
        assertThat(testMembers.getPersonalPhone()).isEqualTo(DEFAULT_PERSONAL_PHONE);
        assertThat(testMembers.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMembers.getHouseHolder()).isEqualTo(DEFAULT_HOUSE_HOLDER);
        assertThat(testMembers.getDependents()).isEqualTo(DEFAULT_DEPENDENTS);
        assertThat(testMembers.getDrivingPermit()).isEqualTo(DEFAULT_DRIVING_PERMIT);
        assertThat(testMembers.getFirstNameSpousse()).isEqualTo(DEFAULT_FIRST_NAME_SPOUSSE);
        assertThat(testMembers.getLastNameSpousse()).isEqualTo(DEFAULT_LAST_NAME_SPOUSSE);
        assertThat(testMembers.getPhoneSpousse()).isEqualTo(DEFAULT_PHONE_SPOUSSE);
        assertThat(testMembers.getNameEmergencyContact()).isEqualTo(DEFAULT_NAME_EMERGENCY_CONTACT);
        assertThat(testMembers.getPhoneEmergencyContact()).isEqualTo(DEFAULT_PHONE_EMERGENCY_CONTACT);
        assertThat(testMembers.getChronicDisease()).isEqualTo(DEFAULT_CHRONIC_DISEASE);
        assertThat(testMembers.getLevelOfStudy()).isEqualTo(DEFAULT_LEVEL_OF_STUDY);
    }

    @Test
    @Transactional
    void createMembersWithExistingId() throws Exception {
        // Create the Members with an existing ID
        members.setId(1L);
        MembersDTO membersDTO = membersMapper.toDto(members);

        int databaseSizeBeforeCreate = membersRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembersMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMembers() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        // Get all the membersList
        restMembersMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(members.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].dateOfBrith").value(hasItem(DEFAULT_DATE_OF_BRITH.toString())))
            .andExpect(jsonPath("$.[*].civility").value(hasItem(DEFAULT_CIVILITY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY)))
            .andExpect(jsonPath("$.[*].firstNameFather").value(hasItem(DEFAULT_FIRST_NAME_FATHER)))
            .andExpect(jsonPath("$.[*].firstNameMather").value(hasItem(DEFAULT_FIRST_NAME_MATHER)))
            .andExpect(jsonPath("$.[*].lastNameFather").value(hasItem(DEFAULT_LAST_NAME_FATHER)))
            .andExpect(jsonPath("$.[*].lastNameMather").value(hasItem(DEFAULT_LAST_NAME_MATHER)))
            .andExpect(jsonPath("$.[*].site").value(hasItem(DEFAULT_SITE)))
            .andExpect(jsonPath("$.[*].positionHeld").value(hasItem(DEFAULT_POSITION_HELD)))
            .andExpect(jsonPath("$.[*].matriculeOfHierarchicalChief").value(hasItem(DEFAULT_MATRICULE_OF_HIERARCHICAL_CHIEF)))
            .andExpect(jsonPath("$.[*].firstNameHierarchicalChief").value(hasItem(DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF)))
            .andExpect(jsonPath("$.[*].lastNameHierarchicalChief").value(hasItem(DEFAULT_LAST_NAME_HIERARCHICAL_CHIEF)))
            .andExpect(jsonPath("$.[*].identityDocumentType").value(hasItem(DEFAULT_IDENTITY_DOCUMENT_TYPE)))
            .andExpect(jsonPath("$.[*].numberIdentityDocuement").value(hasItem(DEFAULT_NUMBER_IDENTITY_DOCUEMENT)))
            .andExpect(jsonPath("$.[*].numberSocialSecurity").value(hasItem(DEFAULT_NUMBER_SOCIAL_SECURITY)))
            .andExpect(jsonPath("$.[*].personelAdress").value(hasItem(DEFAULT_PERSONEL_ADRESS)))
            .andExpect(jsonPath("$.[*].adressOfNextOfKin").value(hasItem(DEFAULT_ADRESS_OF_NEXT_OF_KIN)))
            .andExpect(jsonPath("$.[*].mailAdress").value(hasItem(DEFAULT_MAIL_ADRESS)))
            .andExpect(jsonPath("$.[*].homePhone").value(hasItem(DEFAULT_HOME_PHONE.intValue())))
            .andExpect(jsonPath("$.[*].personalPhone").value(hasItem(DEFAULT_PERSONAL_PHONE.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].houseHolder").value(hasItem(DEFAULT_HOUSE_HOLDER.booleanValue())))
            .andExpect(jsonPath("$.[*].dependents").value(hasItem(DEFAULT_DEPENDENTS)))
            .andExpect(jsonPath("$.[*].drivingPermit").value(hasItem(DEFAULT_DRIVING_PERMIT.booleanValue())))
            .andExpect(jsonPath("$.[*].firstNameSpousse").value(hasItem(DEFAULT_FIRST_NAME_SPOUSSE)))
            .andExpect(jsonPath("$.[*].lastNameSpousse").value(hasItem(DEFAULT_LAST_NAME_SPOUSSE)))
            .andExpect(jsonPath("$.[*].phoneSpousse").value(hasItem(DEFAULT_PHONE_SPOUSSE.intValue())))
            .andExpect(jsonPath("$.[*].nameEmergencyContact").value(hasItem(DEFAULT_NAME_EMERGENCY_CONTACT)))
            .andExpect(jsonPath("$.[*].phoneEmergencyContact").value(hasItem(DEFAULT_PHONE_EMERGENCY_CONTACT.intValue())))
            .andExpect(jsonPath("$.[*].chronicDisease").value(hasItem(DEFAULT_CHRONIC_DISEASE.booleanValue())))
            .andExpect(jsonPath("$.[*].levelOfStudy").value(hasItem(DEFAULT_LEVEL_OF_STUDY)));
    }

    @Test
    @Transactional
    void getMembers() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        // Get the members
        restMembersMockMvc
            .perform(get(ENTITY_API_URL_ID, members.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(members.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.dateOfBrith").value(DEFAULT_DATE_OF_BRITH.toString()))
            .andExpect(jsonPath("$.civility").value(DEFAULT_CIVILITY))
            .andExpect(jsonPath("$.placeOfBirth").value(DEFAULT_PLACE_OF_BIRTH))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY))
            .andExpect(jsonPath("$.firstNameFather").value(DEFAULT_FIRST_NAME_FATHER))
            .andExpect(jsonPath("$.firstNameMather").value(DEFAULT_FIRST_NAME_MATHER))
            .andExpect(jsonPath("$.lastNameFather").value(DEFAULT_LAST_NAME_FATHER))
            .andExpect(jsonPath("$.lastNameMather").value(DEFAULT_LAST_NAME_MATHER))
            .andExpect(jsonPath("$.site").value(DEFAULT_SITE))
            .andExpect(jsonPath("$.positionHeld").value(DEFAULT_POSITION_HELD))
            .andExpect(jsonPath("$.matriculeOfHierarchicalChief").value(DEFAULT_MATRICULE_OF_HIERARCHICAL_CHIEF))
            .andExpect(jsonPath("$.firstNameHierarchicalChief").value(DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF))
            .andExpect(jsonPath("$.lastNameHierarchicalChief").value(DEFAULT_LAST_NAME_HIERARCHICAL_CHIEF))
            .andExpect(jsonPath("$.identityDocumentType").value(DEFAULT_IDENTITY_DOCUMENT_TYPE))
            .andExpect(jsonPath("$.numberIdentityDocuement").value(DEFAULT_NUMBER_IDENTITY_DOCUEMENT))
            .andExpect(jsonPath("$.numberSocialSecurity").value(DEFAULT_NUMBER_SOCIAL_SECURITY))
            .andExpect(jsonPath("$.personelAdress").value(DEFAULT_PERSONEL_ADRESS))
            .andExpect(jsonPath("$.adressOfNextOfKin").value(DEFAULT_ADRESS_OF_NEXT_OF_KIN))
            .andExpect(jsonPath("$.mailAdress").value(DEFAULT_MAIL_ADRESS))
            .andExpect(jsonPath("$.homePhone").value(DEFAULT_HOME_PHONE.intValue()))
            .andExpect(jsonPath("$.personalPhone").value(DEFAULT_PERSONAL_PHONE.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.houseHolder").value(DEFAULT_HOUSE_HOLDER.booleanValue()))
            .andExpect(jsonPath("$.dependents").value(DEFAULT_DEPENDENTS))
            .andExpect(jsonPath("$.drivingPermit").value(DEFAULT_DRIVING_PERMIT.booleanValue()))
            .andExpect(jsonPath("$.firstNameSpousse").value(DEFAULT_FIRST_NAME_SPOUSSE))
            .andExpect(jsonPath("$.lastNameSpousse").value(DEFAULT_LAST_NAME_SPOUSSE))
            .andExpect(jsonPath("$.phoneSpousse").value(DEFAULT_PHONE_SPOUSSE.intValue()))
            .andExpect(jsonPath("$.nameEmergencyContact").value(DEFAULT_NAME_EMERGENCY_CONTACT))
            .andExpect(jsonPath("$.phoneEmergencyContact").value(DEFAULT_PHONE_EMERGENCY_CONTACT.intValue()))
            .andExpect(jsonPath("$.chronicDisease").value(DEFAULT_CHRONIC_DISEASE.booleanValue()))
            .andExpect(jsonPath("$.levelOfStudy").value(DEFAULT_LEVEL_OF_STUDY));
    }

    @Test
    @Transactional
    void getNonExistingMembers() throws Exception {
        // Get the members
        restMembersMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMembers() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        int databaseSizeBeforeUpdate = membersRepository.findAll().size();

        // Update the members
        Members updatedMembers = membersRepository.findById(members.getId()).get();
        // Disconnect from session so that the updates on updatedMembers are not directly saved in db
        em.detach(updatedMembers);
        updatedMembers
            .matricule(UPDATED_MATRICULE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .dateOfBrith(UPDATED_DATE_OF_BRITH)
            .civility(UPDATED_CIVILITY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .nationality(UPDATED_NATIONALITY)
            .firstNameFather(UPDATED_FIRST_NAME_FATHER)
            .firstNameMather(UPDATED_FIRST_NAME_MATHER)
            .lastNameFather(UPDATED_LAST_NAME_FATHER)
            .lastNameMather(UPDATED_LAST_NAME_MATHER)
            .site(UPDATED_SITE)
            .positionHeld(UPDATED_POSITION_HELD)
            .matriculeOfHierarchicalChief(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF)
            .firstNameHierarchicalChief(UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF)
            .lastNameHierarchicalChief(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF)
            .identityDocumentType(UPDATED_IDENTITY_DOCUMENT_TYPE)
            .numberIdentityDocuement(UPDATED_NUMBER_IDENTITY_DOCUEMENT)
            .numberSocialSecurity(UPDATED_NUMBER_SOCIAL_SECURITY)
            .personelAdress(UPDATED_PERSONEL_ADRESS)
            .adressOfNextOfKin(UPDATED_ADRESS_OF_NEXT_OF_KIN)
            .mailAdress(UPDATED_MAIL_ADRESS)
            .homePhone(UPDATED_HOME_PHONE)
            .personalPhone(UPDATED_PERSONAL_PHONE)
            .status(UPDATED_STATUS)
            .houseHolder(UPDATED_HOUSE_HOLDER)
            .dependents(UPDATED_DEPENDENTS)
            .drivingPermit(UPDATED_DRIVING_PERMIT)
            .firstNameSpousse(UPDATED_FIRST_NAME_SPOUSSE)
            .lastNameSpousse(UPDATED_LAST_NAME_SPOUSSE)
            .phoneSpousse(UPDATED_PHONE_SPOUSSE)
            .nameEmergencyContact(UPDATED_NAME_EMERGENCY_CONTACT)
            .phoneEmergencyContact(UPDATED_PHONE_EMERGENCY_CONTACT)
            .chronicDisease(UPDATED_CHRONIC_DISEASE)
            .levelOfStudy(UPDATED_LEVEL_OF_STUDY);
        MembersDTO membersDTO = membersMapper.toDto(updatedMembers);

        restMembersMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membersDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isOk());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
        Members testMembers = membersList.get(membersList.size() - 1);
        assertThat(testMembers.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMembers.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testMembers.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testMembers.getDateOfBrith()).isEqualTo(UPDATED_DATE_OF_BRITH);
        assertThat(testMembers.getCivility()).isEqualTo(UPDATED_CIVILITY);
        assertThat(testMembers.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testMembers.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testMembers.getFirstNameFather()).isEqualTo(UPDATED_FIRST_NAME_FATHER);
        assertThat(testMembers.getFirstNameMather()).isEqualTo(UPDATED_FIRST_NAME_MATHER);
        assertThat(testMembers.getLastNameFather()).isEqualTo(UPDATED_LAST_NAME_FATHER);
        assertThat(testMembers.getLastNameMather()).isEqualTo(UPDATED_LAST_NAME_MATHER);
        assertThat(testMembers.getSite()).isEqualTo(UPDATED_SITE);
        assertThat(testMembers.getPositionHeld()).isEqualTo(UPDATED_POSITION_HELD);
        assertThat(testMembers.getMatriculeOfHierarchicalChief()).isEqualTo(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getFirstNameHierarchicalChief()).isEqualTo(UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getLastNameHierarchicalChief()).isEqualTo(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getIdentityDocumentType()).isEqualTo(UPDATED_IDENTITY_DOCUMENT_TYPE);
        assertThat(testMembers.getNumberIdentityDocuement()).isEqualTo(UPDATED_NUMBER_IDENTITY_DOCUEMENT);
        assertThat(testMembers.getNumberSocialSecurity()).isEqualTo(UPDATED_NUMBER_SOCIAL_SECURITY);
        assertThat(testMembers.getPersonelAdress()).isEqualTo(UPDATED_PERSONEL_ADRESS);
        assertThat(testMembers.getAdressOfNextOfKin()).isEqualTo(UPDATED_ADRESS_OF_NEXT_OF_KIN);
        assertThat(testMembers.getMailAdress()).isEqualTo(UPDATED_MAIL_ADRESS);
        assertThat(testMembers.getHomePhone()).isEqualTo(UPDATED_HOME_PHONE);
        assertThat(testMembers.getPersonalPhone()).isEqualTo(UPDATED_PERSONAL_PHONE);
        assertThat(testMembers.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMembers.getHouseHolder()).isEqualTo(UPDATED_HOUSE_HOLDER);
        assertThat(testMembers.getDependents()).isEqualTo(UPDATED_DEPENDENTS);
        assertThat(testMembers.getDrivingPermit()).isEqualTo(UPDATED_DRIVING_PERMIT);
        assertThat(testMembers.getFirstNameSpousse()).isEqualTo(UPDATED_FIRST_NAME_SPOUSSE);
        assertThat(testMembers.getLastNameSpousse()).isEqualTo(UPDATED_LAST_NAME_SPOUSSE);
        assertThat(testMembers.getPhoneSpousse()).isEqualTo(UPDATED_PHONE_SPOUSSE);
        assertThat(testMembers.getNameEmergencyContact()).isEqualTo(UPDATED_NAME_EMERGENCY_CONTACT);
        assertThat(testMembers.getPhoneEmergencyContact()).isEqualTo(UPDATED_PHONE_EMERGENCY_CONTACT);
        assertThat(testMembers.getChronicDisease()).isEqualTo(UPDATED_CHRONIC_DISEASE);
        assertThat(testMembers.getLevelOfStudy()).isEqualTo(UPDATED_LEVEL_OF_STUDY);
    }

    @Test
    @Transactional
    void putNonExistingMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membersDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membersDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMembersWithPatch() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        int databaseSizeBeforeUpdate = membersRepository.findAll().size();

        // Update the members using partial update
        Members partialUpdatedMembers = new Members();
        partialUpdatedMembers.setId(members.getId());

        partialUpdatedMembers
            .matricule(UPDATED_MATRICULE)
            .lastName(UPDATED_LAST_NAME)
            .civility(UPDATED_CIVILITY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .firstNameFather(UPDATED_FIRST_NAME_FATHER)
            .firstNameMather(UPDATED_FIRST_NAME_MATHER)
            .lastNameFather(UPDATED_LAST_NAME_FATHER)
            .lastNameMather(UPDATED_LAST_NAME_MATHER)
            .site(UPDATED_SITE)
            .matriculeOfHierarchicalChief(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF)
            .lastNameHierarchicalChief(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF)
            .numberIdentityDocuement(UPDATED_NUMBER_IDENTITY_DOCUEMENT)
            .numberSocialSecurity(UPDATED_NUMBER_SOCIAL_SECURITY)
            .personelAdress(UPDATED_PERSONEL_ADRESS)
            .adressOfNextOfKin(UPDATED_ADRESS_OF_NEXT_OF_KIN)
            .mailAdress(UPDATED_MAIL_ADRESS)
            .status(UPDATED_STATUS)
            .dependents(UPDATED_DEPENDENTS)
            .drivingPermit(UPDATED_DRIVING_PERMIT)
            .nameEmergencyContact(UPDATED_NAME_EMERGENCY_CONTACT)
            .chronicDisease(UPDATED_CHRONIC_DISEASE)
            .levelOfStudy(UPDATED_LEVEL_OF_STUDY);

        restMembersMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembers.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembers))
            )
            .andExpect(status().isOk());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
        Members testMembers = membersList.get(membersList.size() - 1);
        assertThat(testMembers.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMembers.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testMembers.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testMembers.getDateOfBrith()).isEqualTo(DEFAULT_DATE_OF_BRITH);
        assertThat(testMembers.getCivility()).isEqualTo(UPDATED_CIVILITY);
        assertThat(testMembers.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testMembers.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testMembers.getFirstNameFather()).isEqualTo(UPDATED_FIRST_NAME_FATHER);
        assertThat(testMembers.getFirstNameMather()).isEqualTo(UPDATED_FIRST_NAME_MATHER);
        assertThat(testMembers.getLastNameFather()).isEqualTo(UPDATED_LAST_NAME_FATHER);
        assertThat(testMembers.getLastNameMather()).isEqualTo(UPDATED_LAST_NAME_MATHER);
        assertThat(testMembers.getSite()).isEqualTo(UPDATED_SITE);
        assertThat(testMembers.getPositionHeld()).isEqualTo(DEFAULT_POSITION_HELD);
        assertThat(testMembers.getMatriculeOfHierarchicalChief()).isEqualTo(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getFirstNameHierarchicalChief()).isEqualTo(DEFAULT_FIRST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getLastNameHierarchicalChief()).isEqualTo(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getIdentityDocumentType()).isEqualTo(DEFAULT_IDENTITY_DOCUMENT_TYPE);
        assertThat(testMembers.getNumberIdentityDocuement()).isEqualTo(UPDATED_NUMBER_IDENTITY_DOCUEMENT);
        assertThat(testMembers.getNumberSocialSecurity()).isEqualTo(UPDATED_NUMBER_SOCIAL_SECURITY);
        assertThat(testMembers.getPersonelAdress()).isEqualTo(UPDATED_PERSONEL_ADRESS);
        assertThat(testMembers.getAdressOfNextOfKin()).isEqualTo(UPDATED_ADRESS_OF_NEXT_OF_KIN);
        assertThat(testMembers.getMailAdress()).isEqualTo(UPDATED_MAIL_ADRESS);
        assertThat(testMembers.getHomePhone()).isEqualTo(DEFAULT_HOME_PHONE);
        assertThat(testMembers.getPersonalPhone()).isEqualTo(DEFAULT_PERSONAL_PHONE);
        assertThat(testMembers.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMembers.getHouseHolder()).isEqualTo(DEFAULT_HOUSE_HOLDER);
        assertThat(testMembers.getDependents()).isEqualTo(UPDATED_DEPENDENTS);
        assertThat(testMembers.getDrivingPermit()).isEqualTo(UPDATED_DRIVING_PERMIT);
        assertThat(testMembers.getFirstNameSpousse()).isEqualTo(DEFAULT_FIRST_NAME_SPOUSSE);
        assertThat(testMembers.getLastNameSpousse()).isEqualTo(DEFAULT_LAST_NAME_SPOUSSE);
        assertThat(testMembers.getPhoneSpousse()).isEqualTo(DEFAULT_PHONE_SPOUSSE);
        assertThat(testMembers.getNameEmergencyContact()).isEqualTo(UPDATED_NAME_EMERGENCY_CONTACT);
        assertThat(testMembers.getPhoneEmergencyContact()).isEqualTo(DEFAULT_PHONE_EMERGENCY_CONTACT);
        assertThat(testMembers.getChronicDisease()).isEqualTo(UPDATED_CHRONIC_DISEASE);
        assertThat(testMembers.getLevelOfStudy()).isEqualTo(UPDATED_LEVEL_OF_STUDY);
    }

    @Test
    @Transactional
    void fullUpdateMembersWithPatch() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        int databaseSizeBeforeUpdate = membersRepository.findAll().size();

        // Update the members using partial update
        Members partialUpdatedMembers = new Members();
        partialUpdatedMembers.setId(members.getId());

        partialUpdatedMembers
            .matricule(UPDATED_MATRICULE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .dateOfBrith(UPDATED_DATE_OF_BRITH)
            .civility(UPDATED_CIVILITY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .nationality(UPDATED_NATIONALITY)
            .firstNameFather(UPDATED_FIRST_NAME_FATHER)
            .firstNameMather(UPDATED_FIRST_NAME_MATHER)
            .lastNameFather(UPDATED_LAST_NAME_FATHER)
            .lastNameMather(UPDATED_LAST_NAME_MATHER)
            .site(UPDATED_SITE)
            .positionHeld(UPDATED_POSITION_HELD)
            .matriculeOfHierarchicalChief(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF)
            .firstNameHierarchicalChief(UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF)
            .lastNameHierarchicalChief(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF)
            .identityDocumentType(UPDATED_IDENTITY_DOCUMENT_TYPE)
            .numberIdentityDocuement(UPDATED_NUMBER_IDENTITY_DOCUEMENT)
            .numberSocialSecurity(UPDATED_NUMBER_SOCIAL_SECURITY)
            .personelAdress(UPDATED_PERSONEL_ADRESS)
            .adressOfNextOfKin(UPDATED_ADRESS_OF_NEXT_OF_KIN)
            .mailAdress(UPDATED_MAIL_ADRESS)
            .homePhone(UPDATED_HOME_PHONE)
            .personalPhone(UPDATED_PERSONAL_PHONE)
            .status(UPDATED_STATUS)
            .houseHolder(UPDATED_HOUSE_HOLDER)
            .dependents(UPDATED_DEPENDENTS)
            .drivingPermit(UPDATED_DRIVING_PERMIT)
            .firstNameSpousse(UPDATED_FIRST_NAME_SPOUSSE)
            .lastNameSpousse(UPDATED_LAST_NAME_SPOUSSE)
            .phoneSpousse(UPDATED_PHONE_SPOUSSE)
            .nameEmergencyContact(UPDATED_NAME_EMERGENCY_CONTACT)
            .phoneEmergencyContact(UPDATED_PHONE_EMERGENCY_CONTACT)
            .chronicDisease(UPDATED_CHRONIC_DISEASE)
            .levelOfStudy(UPDATED_LEVEL_OF_STUDY);

        restMembersMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembers.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembers))
            )
            .andExpect(status().isOk());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
        Members testMembers = membersList.get(membersList.size() - 1);
        assertThat(testMembers.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMembers.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testMembers.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testMembers.getDateOfBrith()).isEqualTo(UPDATED_DATE_OF_BRITH);
        assertThat(testMembers.getCivility()).isEqualTo(UPDATED_CIVILITY);
        assertThat(testMembers.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testMembers.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testMembers.getFirstNameFather()).isEqualTo(UPDATED_FIRST_NAME_FATHER);
        assertThat(testMembers.getFirstNameMather()).isEqualTo(UPDATED_FIRST_NAME_MATHER);
        assertThat(testMembers.getLastNameFather()).isEqualTo(UPDATED_LAST_NAME_FATHER);
        assertThat(testMembers.getLastNameMather()).isEqualTo(UPDATED_LAST_NAME_MATHER);
        assertThat(testMembers.getSite()).isEqualTo(UPDATED_SITE);
        assertThat(testMembers.getPositionHeld()).isEqualTo(UPDATED_POSITION_HELD);
        assertThat(testMembers.getMatriculeOfHierarchicalChief()).isEqualTo(UPDATED_MATRICULE_OF_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getFirstNameHierarchicalChief()).isEqualTo(UPDATED_FIRST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getLastNameHierarchicalChief()).isEqualTo(UPDATED_LAST_NAME_HIERARCHICAL_CHIEF);
        assertThat(testMembers.getIdentityDocumentType()).isEqualTo(UPDATED_IDENTITY_DOCUMENT_TYPE);
        assertThat(testMembers.getNumberIdentityDocuement()).isEqualTo(UPDATED_NUMBER_IDENTITY_DOCUEMENT);
        assertThat(testMembers.getNumberSocialSecurity()).isEqualTo(UPDATED_NUMBER_SOCIAL_SECURITY);
        assertThat(testMembers.getPersonelAdress()).isEqualTo(UPDATED_PERSONEL_ADRESS);
        assertThat(testMembers.getAdressOfNextOfKin()).isEqualTo(UPDATED_ADRESS_OF_NEXT_OF_KIN);
        assertThat(testMembers.getMailAdress()).isEqualTo(UPDATED_MAIL_ADRESS);
        assertThat(testMembers.getHomePhone()).isEqualTo(UPDATED_HOME_PHONE);
        assertThat(testMembers.getPersonalPhone()).isEqualTo(UPDATED_PERSONAL_PHONE);
        assertThat(testMembers.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMembers.getHouseHolder()).isEqualTo(UPDATED_HOUSE_HOLDER);
        assertThat(testMembers.getDependents()).isEqualTo(UPDATED_DEPENDENTS);
        assertThat(testMembers.getDrivingPermit()).isEqualTo(UPDATED_DRIVING_PERMIT);
        assertThat(testMembers.getFirstNameSpousse()).isEqualTo(UPDATED_FIRST_NAME_SPOUSSE);
        assertThat(testMembers.getLastNameSpousse()).isEqualTo(UPDATED_LAST_NAME_SPOUSSE);
        assertThat(testMembers.getPhoneSpousse()).isEqualTo(UPDATED_PHONE_SPOUSSE);
        assertThat(testMembers.getNameEmergencyContact()).isEqualTo(UPDATED_NAME_EMERGENCY_CONTACT);
        assertThat(testMembers.getPhoneEmergencyContact()).isEqualTo(UPDATED_PHONE_EMERGENCY_CONTACT);
        assertThat(testMembers.getChronicDisease()).isEqualTo(UPDATED_CHRONIC_DISEASE);
        assertThat(testMembers.getLevelOfStudy()).isEqualTo(UPDATED_LEVEL_OF_STUDY);
    }

    @Test
    @Transactional
    void patchNonExistingMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, membersDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMembers() throws Exception {
        int databaseSizeBeforeUpdate = membersRepository.findAll().size();
        members.setId(count.incrementAndGet());

        // Create the Members
        MembersDTO membersDTO = membersMapper.toDto(members);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembersMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(membersDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Members in the database
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMembers() throws Exception {
        // Initialize the database
        membersRepository.saveAndFlush(members);

        int databaseSizeBeforeDelete = membersRepository.findAll().size();

        // Delete the members
        restMembersMockMvc
            .perform(delete(ENTITY_API_URL_ID, members.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Members> membersList = membersRepository.findAll();
        assertThat(membersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
