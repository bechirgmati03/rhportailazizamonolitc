package com.aziza.rhportail.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AdvanceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Advance.class);
        Advance advance1 = new Advance();
        advance1.setId(1L);
        Advance advance2 = new Advance();
        advance2.setId(advance1.getId());
        assertThat(advance1).isEqualTo(advance2);
        advance2.setId(2L);
        assertThat(advance1).isNotEqualTo(advance2);
        advance1.setId(null);
        assertThat(advance1).isNotEqualTo(advance2);
    }
}
