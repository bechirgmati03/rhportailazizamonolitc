package com.aziza.rhportail.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ProfessionalExpeienceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfessionalExpeience.class);
        ProfessionalExpeience professionalExpeience1 = new ProfessionalExpeience();
        professionalExpeience1.setId(1L);
        ProfessionalExpeience professionalExpeience2 = new ProfessionalExpeience();
        professionalExpeience2.setId(professionalExpeience1.getId());
        assertThat(professionalExpeience1).isEqualTo(professionalExpeience2);
        professionalExpeience2.setId(2L);
        assertThat(professionalExpeience1).isNotEqualTo(professionalExpeience2);
        professionalExpeience1.setId(null);
        assertThat(professionalExpeience1).isNotEqualTo(professionalExpeience2);
    }
}
