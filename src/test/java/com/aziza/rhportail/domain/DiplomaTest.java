package com.aziza.rhportail.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DiplomaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Diploma.class);
        Diploma diploma1 = new Diploma();
        diploma1.setId(1L);
        Diploma diploma2 = new Diploma();
        diploma2.setId(diploma1.getId());
        assertThat(diploma1).isEqualTo(diploma2);
        diploma2.setId(2L);
        assertThat(diploma1).isNotEqualTo(diploma2);
        diploma1.setId(null);
        assertThat(diploma1).isNotEqualTo(diploma2);
    }
}
