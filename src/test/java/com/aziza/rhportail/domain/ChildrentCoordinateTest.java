package com.aziza.rhportail.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.aziza.rhportail.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChildrentCoordinateTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChildrentCoordinate.class);
        ChildrentCoordinate childrentCoordinate1 = new ChildrentCoordinate();
        childrentCoordinate1.setId(1L);
        ChildrentCoordinate childrentCoordinate2 = new ChildrentCoordinate();
        childrentCoordinate2.setId(childrentCoordinate1.getId());
        assertThat(childrentCoordinate1).isEqualTo(childrentCoordinate2);
        childrentCoordinate2.setId(2L);
        assertThat(childrentCoordinate1).isNotEqualTo(childrentCoordinate2);
        childrentCoordinate1.setId(null);
        assertThat(childrentCoordinate1).isNotEqualTo(childrentCoordinate2);
    }
}
