package com.aziza.rhportail.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Members.
 */
@Entity
@Table(name = "members")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Members implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "matricule")
    private String matricule;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_brith")
    private LocalDate dateOfBrith;

    @Column(name = "civility")
    private String civility;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "first_name_father")
    private String firstNameFather;

    @Column(name = "first_name_mather")
    private String firstNameMather;

    @Column(name = "last_name_father")
    private String lastNameFather;

    @Column(name = "last_name_mather")
    private String lastNameMather;

    @Column(name = "site")
    private String site;

    @Column(name = "position_held")
    private String positionHeld;

    @Column(name = "matricule_of_hierarchical_chief")
    private String matriculeOfHierarchicalChief;

    @Column(name = "first_name_hierarchical_chief")
    private String firstNameHierarchicalChief;

    @Column(name = "last_name_hierarchical_chief")
    private String lastNameHierarchicalChief;

    @Column(name = "identity_document_type")
    private String identityDocumentType;

    @Column(name = "number_identity_docuement")
    private String numberIdentityDocuement;

    @Column(name = "number_social_security")
    private String numberSocialSecurity;

    @Column(name = "personel_adress")
    private String personelAdress;

    @Column(name = "adress_of_next_of_kin")
    private String adressOfNextOfKin;

    @Column(name = "mail_adress")
    private String mailAdress;

    @Column(name = "home_phone")
    private Long homePhone;

    @Column(name = "personal_phone")
    private Long personalPhone;

    @Column(name = "status")
    private String status;

    @Column(name = "house_holder")
    private Boolean houseHolder;

    @Column(name = "dependents")
    private Integer dependents;

    @Column(name = "driving_permit")
    private Boolean drivingPermit;

    @Column(name = "first_name_spousse")
    private String firstNameSpousse;

    @Column(name = "last_name_spousse")
    private String lastNameSpousse;

    @Column(name = "phone_spousse")
    private Long phoneSpousse;

    @Column(name = "name_emergency_contact")
    private String nameEmergencyContact;

    @Column(name = "phone_emergency_contact")
    private Long phoneEmergencyContact;

    @Column(name = "chronic_disease")
    private Boolean chronicDisease;

    @Column(name = "level_of_study")
    private String levelOfStudy;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "members")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "members" }, allowSetters = true)
    private Set<Diploma> diplomas = new HashSet<>();

    @OneToMany(mappedBy = "members")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "members" }, allowSetters = true)
    private Set<ProfessionalExpeience> professionalExpeiences = new HashSet<>();

    @OneToMany(mappedBy = "members")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "members" }, allowSetters = true)
    private Set<ChildrentCoordinate> childrentCoordinates = new HashSet<>();

    @OneToMany(mappedBy = "members")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "members" }, allowSetters = true)
    private Set<Advance> advances = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Members id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return this.matricule;
    }

    public Members matricule(String matricule) {
        this.setMatricule(matricule);
        return this;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Members firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Members lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBrith() {
        return this.dateOfBrith;
    }

    public Members dateOfBrith(LocalDate dateOfBrith) {
        this.setDateOfBrith(dateOfBrith);
        return this;
    }

    public void setDateOfBrith(LocalDate dateOfBrith) {
        this.dateOfBrith = dateOfBrith;
    }

    public String getCivility() {
        return this.civility;
    }

    public Members civility(String civility) {
        this.setCivility(civility);
        return this;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    public String getPlaceOfBirth() {
        return this.placeOfBirth;
    }

    public Members placeOfBirth(String placeOfBirth) {
        this.setPlaceOfBirth(placeOfBirth);
        return this;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getNationality() {
        return this.nationality;
    }

    public Members nationality(String nationality) {
        this.setNationality(nationality);
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFirstNameFather() {
        return this.firstNameFather;
    }

    public Members firstNameFather(String firstNameFather) {
        this.setFirstNameFather(firstNameFather);
        return this;
    }

    public void setFirstNameFather(String firstNameFather) {
        this.firstNameFather = firstNameFather;
    }

    public String getFirstNameMather() {
        return this.firstNameMather;
    }

    public Members firstNameMather(String firstNameMather) {
        this.setFirstNameMather(firstNameMather);
        return this;
    }

    public void setFirstNameMather(String firstNameMather) {
        this.firstNameMather = firstNameMather;
    }

    public String getLastNameFather() {
        return this.lastNameFather;
    }

    public Members lastNameFather(String lastNameFather) {
        this.setLastNameFather(lastNameFather);
        return this;
    }

    public void setLastNameFather(String lastNameFather) {
        this.lastNameFather = lastNameFather;
    }

    public String getLastNameMather() {
        return this.lastNameMather;
    }

    public Members lastNameMather(String lastNameMather) {
        this.setLastNameMather(lastNameMather);
        return this;
    }

    public void setLastNameMather(String lastNameMather) {
        this.lastNameMather = lastNameMather;
    }

    public String getSite() {
        return this.site;
    }

    public Members site(String site) {
        this.setSite(site);
        return this;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getPositionHeld() {
        return this.positionHeld;
    }

    public Members positionHeld(String positionHeld) {
        this.setPositionHeld(positionHeld);
        return this;
    }

    public void setPositionHeld(String positionHeld) {
        this.positionHeld = positionHeld;
    }

    public String getMatriculeOfHierarchicalChief() {
        return this.matriculeOfHierarchicalChief;
    }

    public Members matriculeOfHierarchicalChief(String matriculeOfHierarchicalChief) {
        this.setMatriculeOfHierarchicalChief(matriculeOfHierarchicalChief);
        return this;
    }

    public void setMatriculeOfHierarchicalChief(String matriculeOfHierarchicalChief) {
        this.matriculeOfHierarchicalChief = matriculeOfHierarchicalChief;
    }

    public String getFirstNameHierarchicalChief() {
        return this.firstNameHierarchicalChief;
    }

    public Members firstNameHierarchicalChief(String firstNameHierarchicalChief) {
        this.setFirstNameHierarchicalChief(firstNameHierarchicalChief);
        return this;
    }

    public void setFirstNameHierarchicalChief(String firstNameHierarchicalChief) {
        this.firstNameHierarchicalChief = firstNameHierarchicalChief;
    }

    public String getLastNameHierarchicalChief() {
        return this.lastNameHierarchicalChief;
    }

    public Members lastNameHierarchicalChief(String lastNameHierarchicalChief) {
        this.setLastNameHierarchicalChief(lastNameHierarchicalChief);
        return this;
    }

    public void setLastNameHierarchicalChief(String lastNameHierarchicalChief) {
        this.lastNameHierarchicalChief = lastNameHierarchicalChief;
    }

    public String getIdentityDocumentType() {
        return this.identityDocumentType;
    }

    public Members identityDocumentType(String identityDocumentType) {
        this.setIdentityDocumentType(identityDocumentType);
        return this;
    }

    public void setIdentityDocumentType(String identityDocumentType) {
        this.identityDocumentType = identityDocumentType;
    }

    public String getNumberIdentityDocuement() {
        return this.numberIdentityDocuement;
    }

    public Members numberIdentityDocuement(String numberIdentityDocuement) {
        this.setNumberIdentityDocuement(numberIdentityDocuement);
        return this;
    }

    public void setNumberIdentityDocuement(String numberIdentityDocuement) {
        this.numberIdentityDocuement = numberIdentityDocuement;
    }

    public String getNumberSocialSecurity() {
        return this.numberSocialSecurity;
    }

    public Members numberSocialSecurity(String numberSocialSecurity) {
        this.setNumberSocialSecurity(numberSocialSecurity);
        return this;
    }

    public void setNumberSocialSecurity(String numberSocialSecurity) {
        this.numberSocialSecurity = numberSocialSecurity;
    }

    public String getPersonelAdress() {
        return this.personelAdress;
    }

    public Members personelAdress(String personelAdress) {
        this.setPersonelAdress(personelAdress);
        return this;
    }

    public void setPersonelAdress(String personelAdress) {
        this.personelAdress = personelAdress;
    }

    public String getAdressOfNextOfKin() {
        return this.adressOfNextOfKin;
    }

    public Members adressOfNextOfKin(String adressOfNextOfKin) {
        this.setAdressOfNextOfKin(adressOfNextOfKin);
        return this;
    }

    public void setAdressOfNextOfKin(String adressOfNextOfKin) {
        this.adressOfNextOfKin = adressOfNextOfKin;
    }

    public String getMailAdress() {
        return this.mailAdress;
    }

    public Members mailAdress(String mailAdress) {
        this.setMailAdress(mailAdress);
        return this;
    }

    public void setMailAdress(String mailAdress) {
        this.mailAdress = mailAdress;
    }

    public Long getHomePhone() {
        return this.homePhone;
    }

    public Members homePhone(Long homePhone) {
        this.setHomePhone(homePhone);
        return this;
    }

    public void setHomePhone(Long homePhone) {
        this.homePhone = homePhone;
    }

    public Long getPersonalPhone() {
        return this.personalPhone;
    }

    public Members personalPhone(Long personalPhone) {
        this.setPersonalPhone(personalPhone);
        return this;
    }

    public void setPersonalPhone(Long personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getStatus() {
        return this.status;
    }

    public Members status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getHouseHolder() {
        return this.houseHolder;
    }

    public Members houseHolder(Boolean houseHolder) {
        this.setHouseHolder(houseHolder);
        return this;
    }

    public void setHouseHolder(Boolean houseHolder) {
        this.houseHolder = houseHolder;
    }

    public Integer getDependents() {
        return this.dependents;
    }

    public Members dependents(Integer dependents) {
        this.setDependents(dependents);
        return this;
    }

    public void setDependents(Integer dependents) {
        this.dependents = dependents;
    }

    public Boolean getDrivingPermit() {
        return this.drivingPermit;
    }

    public Members drivingPermit(Boolean drivingPermit) {
        this.setDrivingPermit(drivingPermit);
        return this;
    }

    public void setDrivingPermit(Boolean drivingPermit) {
        this.drivingPermit = drivingPermit;
    }

    public String getFirstNameSpousse() {
        return this.firstNameSpousse;
    }

    public Members firstNameSpousse(String firstNameSpousse) {
        this.setFirstNameSpousse(firstNameSpousse);
        return this;
    }

    public void setFirstNameSpousse(String firstNameSpousse) {
        this.firstNameSpousse = firstNameSpousse;
    }

    public String getLastNameSpousse() {
        return this.lastNameSpousse;
    }

    public Members lastNameSpousse(String lastNameSpousse) {
        this.setLastNameSpousse(lastNameSpousse);
        return this;
    }

    public void setLastNameSpousse(String lastNameSpousse) {
        this.lastNameSpousse = lastNameSpousse;
    }

    public Long getPhoneSpousse() {
        return this.phoneSpousse;
    }

    public Members phoneSpousse(Long phoneSpousse) {
        this.setPhoneSpousse(phoneSpousse);
        return this;
    }

    public void setPhoneSpousse(Long phoneSpousse) {
        this.phoneSpousse = phoneSpousse;
    }

    public String getNameEmergencyContact() {
        return this.nameEmergencyContact;
    }

    public Members nameEmergencyContact(String nameEmergencyContact) {
        this.setNameEmergencyContact(nameEmergencyContact);
        return this;
    }

    public void setNameEmergencyContact(String nameEmergencyContact) {
        this.nameEmergencyContact = nameEmergencyContact;
    }

    public Long getPhoneEmergencyContact() {
        return this.phoneEmergencyContact;
    }

    public Members phoneEmergencyContact(Long phoneEmergencyContact) {
        this.setPhoneEmergencyContact(phoneEmergencyContact);
        return this;
    }

    public void setPhoneEmergencyContact(Long phoneEmergencyContact) {
        this.phoneEmergencyContact = phoneEmergencyContact;
    }

    public Boolean getChronicDisease() {
        return this.chronicDisease;
    }

    public Members chronicDisease(Boolean chronicDisease) {
        this.setChronicDisease(chronicDisease);
        return this;
    }

    public void setChronicDisease(Boolean chronicDisease) {
        this.chronicDisease = chronicDisease;
    }

    public String getLevelOfStudy() {
        return this.levelOfStudy;
    }

    public Members levelOfStudy(String levelOfStudy) {
        this.setLevelOfStudy(levelOfStudy);
        return this;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Members user(User user) {
        this.setUser(user);
        return this;
    }

    public Set<Diploma> getDiplomas() {
        return this.diplomas;
    }

    public void setDiplomas(Set<Diploma> diplomas) {
        if (this.diplomas != null) {
            this.diplomas.forEach(i -> i.setMembers(null));
        }
        if (diplomas != null) {
            diplomas.forEach(i -> i.setMembers(this));
        }
        this.diplomas = diplomas;
    }

    public Members diplomas(Set<Diploma> diplomas) {
        this.setDiplomas(diplomas);
        return this;
    }

    public Members addDiploma(Diploma diploma) {
        this.diplomas.add(diploma);
        diploma.setMembers(this);
        return this;
    }

    public Members removeDiploma(Diploma diploma) {
        this.diplomas.remove(diploma);
        diploma.setMembers(null);
        return this;
    }

    public Set<ProfessionalExpeience> getProfessionalExpeiences() {
        return this.professionalExpeiences;
    }

    public void setProfessionalExpeiences(Set<ProfessionalExpeience> professionalExpeiences) {
        if (this.professionalExpeiences != null) {
            this.professionalExpeiences.forEach(i -> i.setMembers(null));
        }
        if (professionalExpeiences != null) {
            professionalExpeiences.forEach(i -> i.setMembers(this));
        }
        this.professionalExpeiences = professionalExpeiences;
    }

    public Members professionalExpeiences(Set<ProfessionalExpeience> professionalExpeiences) {
        this.setProfessionalExpeiences(professionalExpeiences);
        return this;
    }

    public Members addProfessionalExpeience(ProfessionalExpeience professionalExpeience) {
        this.professionalExpeiences.add(professionalExpeience);
        professionalExpeience.setMembers(this);
        return this;
    }

    public Members removeProfessionalExpeience(ProfessionalExpeience professionalExpeience) {
        this.professionalExpeiences.remove(professionalExpeience);
        professionalExpeience.setMembers(null);
        return this;
    }

    public Set<ChildrentCoordinate> getChildrentCoordinates() {
        return this.childrentCoordinates;
    }

    public void setChildrentCoordinates(Set<ChildrentCoordinate> childrentCoordinates) {
        if (this.childrentCoordinates != null) {
            this.childrentCoordinates.forEach(i -> i.setMembers(null));
        }
        if (childrentCoordinates != null) {
            childrentCoordinates.forEach(i -> i.setMembers(this));
        }
        this.childrentCoordinates = childrentCoordinates;
    }

    public Members childrentCoordinates(Set<ChildrentCoordinate> childrentCoordinates) {
        this.setChildrentCoordinates(childrentCoordinates);
        return this;
    }

    public Members addChildrentCoordinate(ChildrentCoordinate childrentCoordinate) {
        this.childrentCoordinates.add(childrentCoordinate);
        childrentCoordinate.setMembers(this);
        return this;
    }

    public Members removeChildrentCoordinate(ChildrentCoordinate childrentCoordinate) {
        this.childrentCoordinates.remove(childrentCoordinate);
        childrentCoordinate.setMembers(null);
        return this;
    }

    public Set<Advance> getAdvances() {
        return this.advances;
    }

    public void setAdvances(Set<Advance> advances) {
        if (this.advances != null) {
            this.advances.forEach(i -> i.setMembers(null));
        }
        if (advances != null) {
            advances.forEach(i -> i.setMembers(this));
        }
        this.advances = advances;
    }

    public Members advances(Set<Advance> advances) {
        this.setAdvances(advances);
        return this;
    }

    public Members addAdvance(Advance advance) {
        this.advances.add(advance);
        advance.setMembers(this);
        return this;
    }

    public Members removeAdvance(Advance advance) {
        this.advances.remove(advance);
        advance.setMembers(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Members)) {
            return false;
        }
        return id != null && id.equals(((Members) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Members{" +
            "id=" + getId() +
            ", matricule='" + getMatricule() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", dateOfBrith='" + getDateOfBrith() + "'" +
            ", civility='" + getCivility() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", firstNameFather='" + getFirstNameFather() + "'" +
            ", firstNameMather='" + getFirstNameMather() + "'" +
            ", lastNameFather='" + getLastNameFather() + "'" +
            ", lastNameMather='" + getLastNameMather() + "'" +
            ", site='" + getSite() + "'" +
            ", positionHeld='" + getPositionHeld() + "'" +
            ", matriculeOfHierarchicalChief='" + getMatriculeOfHierarchicalChief() + "'" +
            ", firstNameHierarchicalChief='" + getFirstNameHierarchicalChief() + "'" +
            ", lastNameHierarchicalChief='" + getLastNameHierarchicalChief() + "'" +
            ", identityDocumentType='" + getIdentityDocumentType() + "'" +
            ", numberIdentityDocuement='" + getNumberIdentityDocuement() + "'" +
            ", numberSocialSecurity='" + getNumberSocialSecurity() + "'" +
            ", personelAdress='" + getPersonelAdress() + "'" +
            ", adressOfNextOfKin='" + getAdressOfNextOfKin() + "'" +
            ", mailAdress='" + getMailAdress() + "'" +
            ", homePhone=" + getHomePhone() +
            ", personalPhone=" + getPersonalPhone() +
            ", status='" + getStatus() + "'" +
            ", houseHolder='" + getHouseHolder() + "'" +
            ", dependents=" + getDependents() +
            ", drivingPermit='" + getDrivingPermit() + "'" +
            ", firstNameSpousse='" + getFirstNameSpousse() + "'" +
            ", lastNameSpousse='" + getLastNameSpousse() + "'" +
            ", phoneSpousse=" + getPhoneSpousse() +
            ", nameEmergencyContact='" + getNameEmergencyContact() + "'" +
            ", phoneEmergencyContact=" + getPhoneEmergencyContact() +
            ", chronicDisease='" + getChronicDisease() + "'" +
            ", levelOfStudy='" + getLevelOfStudy() + "'" +
            "}";
    }
}
