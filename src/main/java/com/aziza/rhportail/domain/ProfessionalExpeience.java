package com.aziza.rhportail.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ProfessionalExpeience.
 */
@Entity
@Table(name = "professional_expeience")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProfessionalExpeience implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "company")
    private String company;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "poste")
    private String poste;

    @Column(name = "departement")
    private String departement;

    @Column(name = "store")
    private String store;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "diplomas", "professionalExpeiences", "childrentCoordinates", "advances" }, allowSetters = true)
    private Members members;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProfessionalExpeience id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return this.company;
    }

    public ProfessionalExpeience company(String company) {
        this.setCompany(company);
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public ProfessionalExpeience startDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return this.endDate;
    }

    public ProfessionalExpeience endDate(LocalDate endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getPoste() {
        return this.poste;
    }

    public ProfessionalExpeience poste(String poste) {
        this.setPoste(poste);
        return this;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getDepartement() {
        return this.departement;
    }

    public ProfessionalExpeience departement(String departement) {
        this.setDepartement(departement);
        return this;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getStore() {
        return this.store;
    }

    public ProfessionalExpeience store(String store) {
        this.setStore(store);
        return this;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public Members getMembers() {
        return this.members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public ProfessionalExpeience members(Members members) {
        this.setMembers(members);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfessionalExpeience)) {
            return false;
        }
        return id != null && id.equals(((ProfessionalExpeience) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfessionalExpeience{" +
            "id=" + getId() +
            ", company='" + getCompany() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", poste='" + getPoste() + "'" +
            ", departement='" + getDepartement() + "'" +
            ", store='" + getStore() + "'" +
            "}";
    }
}
