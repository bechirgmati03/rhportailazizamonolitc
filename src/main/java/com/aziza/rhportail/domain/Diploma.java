package com.aziza.rhportail.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Diploma.
 */
@Entity
@Table(name = "diploma")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Diploma implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "institution")
    private String institution;

    @Column(name = "year_of_graduation")
    private Integer yearOfGraduation;

    @Column(name = "diploma_title")
    private String diplomaTitle;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "diplomas", "professionalExpeiences", "childrentCoordinates", "advances" }, allowSetters = true)
    private Members members;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Diploma id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstitution() {
        return this.institution;
    }

    public Diploma institution(String institution) {
        this.setInstitution(institution);
        return this;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Integer getYearOfGraduation() {
        return this.yearOfGraduation;
    }

    public Diploma yearOfGraduation(Integer yearOfGraduation) {
        this.setYearOfGraduation(yearOfGraduation);
        return this;
    }

    public void setYearOfGraduation(Integer yearOfGraduation) {
        this.yearOfGraduation = yearOfGraduation;
    }

    public String getDiplomaTitle() {
        return this.diplomaTitle;
    }

    public Diploma diplomaTitle(String diplomaTitle) {
        this.setDiplomaTitle(diplomaTitle);
        return this;
    }

    public void setDiplomaTitle(String diplomaTitle) {
        this.diplomaTitle = diplomaTitle;
    }

    public Members getMembers() {
        return this.members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public Diploma members(Members members) {
        this.setMembers(members);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diploma)) {
            return false;
        }
        return id != null && id.equals(((Diploma) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Diploma{" +
            "id=" + getId() +
            ", institution='" + getInstitution() + "'" +
            ", yearOfGraduation=" + getYearOfGraduation() +
            ", diplomaTitle='" + getDiplomaTitle() + "'" +
            "}";
    }
}
