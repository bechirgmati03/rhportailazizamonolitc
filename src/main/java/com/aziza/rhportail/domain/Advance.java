package com.aziza.rhportail.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Advance.
 */
@Entity
@Table(name = "advance")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Advance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "reference")
    private String reference;

    @Column(name = "salary_advance_reason")
    private String salaryAdvanceReason;

    @Column(name = "salary_advance_reason_details")
    private String salaryAdvanceReasonDetails;

    @Column(name = "salary_advance_types")
    private String salaryAdvanceTypes;

    @Column(name = "ceiling_salary_advance_types")
    private Double ceilingSalaryAdvanceTypes;

    @Column(name = "degree_of_urgency")
    private String degreeOfUrgency;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "maximum_salary_advance")
    private Double maximumSalaryAdvance;

    @Column(name = "salary_advance_amount")
    private Double salaryAdvanceAmount;

    @Column(name = "repayment_period")
    private Double repaymentPeriod;

    @Column(name = "salary_deduction_method")
    private String salaryDeductionMethod;

    @Column(name = "total_amount_monthly_payment_deducte")
    private Double totalAmountMonthlyPaymentDeducte;

    @Column(name = "salary_monthly_payment_deducte")
    private Double salaryMonthlyPaymentDeducte;

    @Column(name = "primes_monthly_payment_deducte")
    private Double primesMonthlyPaymentDeducte;

    @Column(name = "status")
    private String status;

    @Column(name = "reason_rejection")
    private String reasonRejection;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "diplomas", "professionalExpeiences", "childrentCoordinates", "advances" }, allowSetters = true)
    private Members members;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Advance id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return this.reference;
    }

    public Advance reference(String reference) {
        this.setReference(reference);
        return this;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSalaryAdvanceReason() {
        return this.salaryAdvanceReason;
    }

    public Advance salaryAdvanceReason(String salaryAdvanceReason) {
        this.setSalaryAdvanceReason(salaryAdvanceReason);
        return this;
    }

    public void setSalaryAdvanceReason(String salaryAdvanceReason) {
        this.salaryAdvanceReason = salaryAdvanceReason;
    }

    public String getSalaryAdvanceReasonDetails() {
        return this.salaryAdvanceReasonDetails;
    }

    public Advance salaryAdvanceReasonDetails(String salaryAdvanceReasonDetails) {
        this.setSalaryAdvanceReasonDetails(salaryAdvanceReasonDetails);
        return this;
    }

    public void setSalaryAdvanceReasonDetails(String salaryAdvanceReasonDetails) {
        this.salaryAdvanceReasonDetails = salaryAdvanceReasonDetails;
    }

    public String getSalaryAdvanceTypes() {
        return this.salaryAdvanceTypes;
    }

    public Advance salaryAdvanceTypes(String salaryAdvanceTypes) {
        this.setSalaryAdvanceTypes(salaryAdvanceTypes);
        return this;
    }

    public void setSalaryAdvanceTypes(String salaryAdvanceTypes) {
        this.salaryAdvanceTypes = salaryAdvanceTypes;
    }

    public Double getCeilingSalaryAdvanceTypes() {
        return this.ceilingSalaryAdvanceTypes;
    }

    public Advance ceilingSalaryAdvanceTypes(Double ceilingSalaryAdvanceTypes) {
        this.setCeilingSalaryAdvanceTypes(ceilingSalaryAdvanceTypes);
        return this;
    }

    public void setCeilingSalaryAdvanceTypes(Double ceilingSalaryAdvanceTypes) {
        this.ceilingSalaryAdvanceTypes = ceilingSalaryAdvanceTypes;
    }

    public String getDegreeOfUrgency() {
        return this.degreeOfUrgency;
    }

    public Advance degreeOfUrgency(String degreeOfUrgency) {
        this.setDegreeOfUrgency(degreeOfUrgency);
        return this;
    }

    public void setDegreeOfUrgency(String degreeOfUrgency) {
        this.degreeOfUrgency = degreeOfUrgency;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Advance date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getMaximumSalaryAdvance() {
        return this.maximumSalaryAdvance;
    }

    public Advance maximumSalaryAdvance(Double maximumSalaryAdvance) {
        this.setMaximumSalaryAdvance(maximumSalaryAdvance);
        return this;
    }

    public void setMaximumSalaryAdvance(Double maximumSalaryAdvance) {
        this.maximumSalaryAdvance = maximumSalaryAdvance;
    }

    public Double getSalaryAdvanceAmount() {
        return this.salaryAdvanceAmount;
    }

    public Advance salaryAdvanceAmount(Double salaryAdvanceAmount) {
        this.setSalaryAdvanceAmount(salaryAdvanceAmount);
        return this;
    }

    public void setSalaryAdvanceAmount(Double salaryAdvanceAmount) {
        this.salaryAdvanceAmount = salaryAdvanceAmount;
    }

    public Double getRepaymentPeriod() {
        return this.repaymentPeriod;
    }

    public Advance repaymentPeriod(Double repaymentPeriod) {
        this.setRepaymentPeriod(repaymentPeriod);
        return this;
    }

    public void setRepaymentPeriod(Double repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public String getSalaryDeductionMethod() {
        return this.salaryDeductionMethod;
    }

    public Advance salaryDeductionMethod(String salaryDeductionMethod) {
        this.setSalaryDeductionMethod(salaryDeductionMethod);
        return this;
    }

    public void setSalaryDeductionMethod(String salaryDeductionMethod) {
        this.salaryDeductionMethod = salaryDeductionMethod;
    }

    public Double getTotalAmountMonthlyPaymentDeducte() {
        return this.totalAmountMonthlyPaymentDeducte;
    }

    public Advance totalAmountMonthlyPaymentDeducte(Double totalAmountMonthlyPaymentDeducte) {
        this.setTotalAmountMonthlyPaymentDeducte(totalAmountMonthlyPaymentDeducte);
        return this;
    }

    public void setTotalAmountMonthlyPaymentDeducte(Double totalAmountMonthlyPaymentDeducte) {
        this.totalAmountMonthlyPaymentDeducte = totalAmountMonthlyPaymentDeducte;
    }

    public Double getSalaryMonthlyPaymentDeducte() {
        return this.salaryMonthlyPaymentDeducte;
    }

    public Advance salaryMonthlyPaymentDeducte(Double salaryMonthlyPaymentDeducte) {
        this.setSalaryMonthlyPaymentDeducte(salaryMonthlyPaymentDeducte);
        return this;
    }

    public void setSalaryMonthlyPaymentDeducte(Double salaryMonthlyPaymentDeducte) {
        this.salaryMonthlyPaymentDeducte = salaryMonthlyPaymentDeducte;
    }

    public Double getPrimesMonthlyPaymentDeducte() {
        return this.primesMonthlyPaymentDeducte;
    }

    public Advance primesMonthlyPaymentDeducte(Double primesMonthlyPaymentDeducte) {
        this.setPrimesMonthlyPaymentDeducte(primesMonthlyPaymentDeducte);
        return this;
    }

    public void setPrimesMonthlyPaymentDeducte(Double primesMonthlyPaymentDeducte) {
        this.primesMonthlyPaymentDeducte = primesMonthlyPaymentDeducte;
    }

    public String getStatus() {
        return this.status;
    }

    public Advance status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReasonRejection() {
        return this.reasonRejection;
    }

    public Advance reasonRejection(String reasonRejection) {
        this.setReasonRejection(reasonRejection);
        return this;
    }

    public void setReasonRejection(String reasonRejection) {
        this.reasonRejection = reasonRejection;
    }

    public Members getMembers() {
        return this.members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public Advance members(Members members) {
        this.setMembers(members);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Advance)) {
            return false;
        }
        return id != null && id.equals(((Advance) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Advance{" +
            "id=" + getId() +
            ", reference='" + getReference() + "'" +
            ", salaryAdvanceReason='" + getSalaryAdvanceReason() + "'" +
            ", salaryAdvanceReasonDetails='" + getSalaryAdvanceReasonDetails() + "'" +
            ", salaryAdvanceTypes='" + getSalaryAdvanceTypes() + "'" +
            ", ceilingSalaryAdvanceTypes=" + getCeilingSalaryAdvanceTypes() +
            ", degreeOfUrgency='" + getDegreeOfUrgency() + "'" +
            ", date='" + getDate() + "'" +
            ", maximumSalaryAdvance=" + getMaximumSalaryAdvance() +
            ", salaryAdvanceAmount=" + getSalaryAdvanceAmount() +
            ", repaymentPeriod=" + getRepaymentPeriod() +
            ", salaryDeductionMethod='" + getSalaryDeductionMethod() + "'" +
            ", totalAmountMonthlyPaymentDeducte=" + getTotalAmountMonthlyPaymentDeducte() +
            ", salaryMonthlyPaymentDeducte=" + getSalaryMonthlyPaymentDeducte() +
            ", primesMonthlyPaymentDeducte=" + getPrimesMonthlyPaymentDeducte() +
            ", status='" + getStatus() + "'" +
            ", reasonRejection='" + getReasonRejection() + "'" +
            "}";
    }
}
