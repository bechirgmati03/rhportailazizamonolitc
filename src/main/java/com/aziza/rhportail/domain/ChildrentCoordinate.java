package com.aziza.rhportail.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ChildrentCoordinate.
 */
@Entity
@Table(name = "childrent_coordinate")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ChildrentCoordinate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name_child")
    private String firstNameChild;

    @Column(name = "last_name_child")
    private String lastNameChild;

    @Column(name = "date_of_birth_child")
    private String dateOfBirthChild;

    @Column(name = "school_level")
    private String schoolLevel;

    @Column(name = "handicapped")
    private Boolean handicapped;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "diplomas", "professionalExpeiences", "childrentCoordinates", "advances" }, allowSetters = true)
    private Members members;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ChildrentCoordinate id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstNameChild() {
        return this.firstNameChild;
    }

    public ChildrentCoordinate firstNameChild(String firstNameChild) {
        this.setFirstNameChild(firstNameChild);
        return this;
    }

    public void setFirstNameChild(String firstNameChild) {
        this.firstNameChild = firstNameChild;
    }

    public String getLastNameChild() {
        return this.lastNameChild;
    }

    public ChildrentCoordinate lastNameChild(String lastNameChild) {
        this.setLastNameChild(lastNameChild);
        return this;
    }

    public void setLastNameChild(String lastNameChild) {
        this.lastNameChild = lastNameChild;
    }

    public String getDateOfBirthChild() {
        return this.dateOfBirthChild;
    }

    public ChildrentCoordinate dateOfBirthChild(String dateOfBirthChild) {
        this.setDateOfBirthChild(dateOfBirthChild);
        return this;
    }

    public void setDateOfBirthChild(String dateOfBirthChild) {
        this.dateOfBirthChild = dateOfBirthChild;
    }

    public String getSchoolLevel() {
        return this.schoolLevel;
    }

    public ChildrentCoordinate schoolLevel(String schoolLevel) {
        this.setSchoolLevel(schoolLevel);
        return this;
    }

    public void setSchoolLevel(String schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public Boolean getHandicapped() {
        return this.handicapped;
    }

    public ChildrentCoordinate handicapped(Boolean handicapped) {
        this.setHandicapped(handicapped);
        return this;
    }

    public void setHandicapped(Boolean handicapped) {
        this.handicapped = handicapped;
    }

    public Members getMembers() {
        return this.members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public ChildrentCoordinate members(Members members) {
        this.setMembers(members);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChildrentCoordinate)) {
            return false;
        }
        return id != null && id.equals(((ChildrentCoordinate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChildrentCoordinate{" +
            "id=" + getId() +
            ", firstNameChild='" + getFirstNameChild() + "'" +
            ", lastNameChild='" + getLastNameChild() + "'" +
            ", dateOfBirthChild='" + getDateOfBirthChild() + "'" +
            ", schoolLevel='" + getSchoolLevel() + "'" +
            ", handicapped='" + getHandicapped() + "'" +
            "}";
    }
}
