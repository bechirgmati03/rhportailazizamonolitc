package com.aziza.rhportail.service;

import com.aziza.rhportail.service.dto.MembersDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.aziza.rhportail.domain.Members}.
 */
public interface MembersService {
    /**
     * Save a members.
     *
     * @param membersDTO the entity to save.
     * @return the persisted entity.
     */
    MembersDTO save(MembersDTO membersDTO);

    /**
     * Partially updates a members.
     *
     * @param membersDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MembersDTO> partialUpdate(MembersDTO membersDTO);

    /**
     * Get all the members.
     *
     * @return the list of entities.
     */
    List<MembersDTO> findAll();

    /**
     * Get the "id" members.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MembersDTO> findOne(Long id);

    /**
     * Delete the "id" members.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
