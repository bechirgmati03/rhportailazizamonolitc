package com.aziza.rhportail.service;

import com.aziza.rhportail.service.dto.ProfessionalExpeienceDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.aziza.rhportail.domain.ProfessionalExpeience}.
 */
public interface ProfessionalExpeienceService {
    /**
     * Save a professionalExpeience.
     *
     * @param professionalExpeienceDTO the entity to save.
     * @return the persisted entity.
     */
    ProfessionalExpeienceDTO save(ProfessionalExpeienceDTO professionalExpeienceDTO);

    /**
     * Partially updates a professionalExpeience.
     *
     * @param professionalExpeienceDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProfessionalExpeienceDTO> partialUpdate(ProfessionalExpeienceDTO professionalExpeienceDTO);

    /**
     * Get all the professionalExpeiences.
     *
     * @return the list of entities.
     */
    List<ProfessionalExpeienceDTO> findAll();

    /**
     * Get the "id" professionalExpeience.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProfessionalExpeienceDTO> findOne(Long id);

    /**
     * Delete the "id" professionalExpeience.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
