package com.aziza.rhportail.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.aziza.rhportail.domain.Members} entity.
 */
public class MembersDTO implements Serializable {

    private Long id;

    private String matricule;

    private String firstName;

    private String lastName;

    private LocalDate dateOfBrith;

    private String civility;

    private String placeOfBirth;

    private String nationality;

    private String firstNameFather;

    private String firstNameMather;

    private String lastNameFather;

    private String lastNameMather;

    private String site;

    private String positionHeld;

    private String matriculeOfHierarchicalChief;

    private String firstNameHierarchicalChief;

    private String lastNameHierarchicalChief;

    private String identityDocumentType;

    private String numberIdentityDocuement;

    private String numberSocialSecurity;

    private String personelAdress;

    private String adressOfNextOfKin;

    private String mailAdress;

    private Long homePhone;

    private Long personalPhone;

    private String status;

    private Boolean houseHolder;

    private Integer dependents;

    private Boolean drivingPermit;

    private String firstNameSpousse;

    private String lastNameSpousse;

    private Long phoneSpousse;

    private String nameEmergencyContact;

    private Long phoneEmergencyContact;

    private Boolean chronicDisease;

    private String levelOfStudy;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBrith() {
        return dateOfBrith;
    }

    public void setDateOfBrith(LocalDate dateOfBrith) {
        this.dateOfBrith = dateOfBrith;
    }

    public String getCivility() {
        return civility;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFirstNameFather() {
        return firstNameFather;
    }

    public void setFirstNameFather(String firstNameFather) {
        this.firstNameFather = firstNameFather;
    }

    public String getFirstNameMather() {
        return firstNameMather;
    }

    public void setFirstNameMather(String firstNameMather) {
        this.firstNameMather = firstNameMather;
    }

    public String getLastNameFather() {
        return lastNameFather;
    }

    public void setLastNameFather(String lastNameFather) {
        this.lastNameFather = lastNameFather;
    }

    public String getLastNameMather() {
        return lastNameMather;
    }

    public void setLastNameMather(String lastNameMather) {
        this.lastNameMather = lastNameMather;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getPositionHeld() {
        return positionHeld;
    }

    public void setPositionHeld(String positionHeld) {
        this.positionHeld = positionHeld;
    }

    public String getMatriculeOfHierarchicalChief() {
        return matriculeOfHierarchicalChief;
    }

    public void setMatriculeOfHierarchicalChief(String matriculeOfHierarchicalChief) {
        this.matriculeOfHierarchicalChief = matriculeOfHierarchicalChief;
    }

    public String getFirstNameHierarchicalChief() {
        return firstNameHierarchicalChief;
    }

    public void setFirstNameHierarchicalChief(String firstNameHierarchicalChief) {
        this.firstNameHierarchicalChief = firstNameHierarchicalChief;
    }

    public String getLastNameHierarchicalChief() {
        return lastNameHierarchicalChief;
    }

    public void setLastNameHierarchicalChief(String lastNameHierarchicalChief) {
        this.lastNameHierarchicalChief = lastNameHierarchicalChief;
    }

    public String getIdentityDocumentType() {
        return identityDocumentType;
    }

    public void setIdentityDocumentType(String identityDocumentType) {
        this.identityDocumentType = identityDocumentType;
    }

    public String getNumberIdentityDocuement() {
        return numberIdentityDocuement;
    }

    public void setNumberIdentityDocuement(String numberIdentityDocuement) {
        this.numberIdentityDocuement = numberIdentityDocuement;
    }

    public String getNumberSocialSecurity() {
        return numberSocialSecurity;
    }

    public void setNumberSocialSecurity(String numberSocialSecurity) {
        this.numberSocialSecurity = numberSocialSecurity;
    }

    public String getPersonelAdress() {
        return personelAdress;
    }

    public void setPersonelAdress(String personelAdress) {
        this.personelAdress = personelAdress;
    }

    public String getAdressOfNextOfKin() {
        return adressOfNextOfKin;
    }

    public void setAdressOfNextOfKin(String adressOfNextOfKin) {
        this.adressOfNextOfKin = adressOfNextOfKin;
    }

    public String getMailAdress() {
        return mailAdress;
    }

    public void setMailAdress(String mailAdress) {
        this.mailAdress = mailAdress;
    }

    public Long getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(Long homePhone) {
        this.homePhone = homePhone;
    }

    public Long getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(Long personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getHouseHolder() {
        return houseHolder;
    }

    public void setHouseHolder(Boolean houseHolder) {
        this.houseHolder = houseHolder;
    }

    public Integer getDependents() {
        return dependents;
    }

    public void setDependents(Integer dependents) {
        this.dependents = dependents;
    }

    public Boolean getDrivingPermit() {
        return drivingPermit;
    }

    public void setDrivingPermit(Boolean drivingPermit) {
        this.drivingPermit = drivingPermit;
    }

    public String getFirstNameSpousse() {
        return firstNameSpousse;
    }

    public void setFirstNameSpousse(String firstNameSpousse) {
        this.firstNameSpousse = firstNameSpousse;
    }

    public String getLastNameSpousse() {
        return lastNameSpousse;
    }

    public void setLastNameSpousse(String lastNameSpousse) {
        this.lastNameSpousse = lastNameSpousse;
    }

    public Long getPhoneSpousse() {
        return phoneSpousse;
    }

    public void setPhoneSpousse(Long phoneSpousse) {
        this.phoneSpousse = phoneSpousse;
    }

    public String getNameEmergencyContact() {
        return nameEmergencyContact;
    }

    public void setNameEmergencyContact(String nameEmergencyContact) {
        this.nameEmergencyContact = nameEmergencyContact;
    }

    public Long getPhoneEmergencyContact() {
        return phoneEmergencyContact;
    }

    public void setPhoneEmergencyContact(Long phoneEmergencyContact) {
        this.phoneEmergencyContact = phoneEmergencyContact;
    }

    public Boolean getChronicDisease() {
        return chronicDisease;
    }

    public void setChronicDisease(Boolean chronicDisease) {
        this.chronicDisease = chronicDisease;
    }

    public String getLevelOfStudy() {
        return levelOfStudy;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembersDTO)) {
            return false;
        }

        MembersDTO membersDTO = (MembersDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, membersDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MembersDTO{" +
            "id=" + getId() +
            ", matricule='" + getMatricule() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", dateOfBrith='" + getDateOfBrith() + "'" +
            ", civility='" + getCivility() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", firstNameFather='" + getFirstNameFather() + "'" +
            ", firstNameMather='" + getFirstNameMather() + "'" +
            ", lastNameFather='" + getLastNameFather() + "'" +
            ", lastNameMather='" + getLastNameMather() + "'" +
            ", site='" + getSite() + "'" +
            ", positionHeld='" + getPositionHeld() + "'" +
            ", matriculeOfHierarchicalChief='" + getMatriculeOfHierarchicalChief() + "'" +
            ", firstNameHierarchicalChief='" + getFirstNameHierarchicalChief() + "'" +
            ", lastNameHierarchicalChief='" + getLastNameHierarchicalChief() + "'" +
            ", identityDocumentType='" + getIdentityDocumentType() + "'" +
            ", numberIdentityDocuement='" + getNumberIdentityDocuement() + "'" +
            ", numberSocialSecurity='" + getNumberSocialSecurity() + "'" +
            ", personelAdress='" + getPersonelAdress() + "'" +
            ", adressOfNextOfKin='" + getAdressOfNextOfKin() + "'" +
            ", mailAdress='" + getMailAdress() + "'" +
            ", homePhone=" + getHomePhone() +
            ", personalPhone=" + getPersonalPhone() +
            ", status='" + getStatus() + "'" +
            ", houseHolder='" + getHouseHolder() + "'" +
            ", dependents=" + getDependents() +
            ", drivingPermit='" + getDrivingPermit() + "'" +
            ", firstNameSpousse='" + getFirstNameSpousse() + "'" +
            ", lastNameSpousse='" + getLastNameSpousse() + "'" +
            ", phoneSpousse=" + getPhoneSpousse() +
            ", nameEmergencyContact='" + getNameEmergencyContact() + "'" +
            ", phoneEmergencyContact=" + getPhoneEmergencyContact() +
            ", chronicDisease='" + getChronicDisease() + "'" +
            ", levelOfStudy='" + getLevelOfStudy() + "'" +
            ", user=" + getUser() +
            "}";
    }
}
