package com.aziza.rhportail.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.aziza.rhportail.domain.Advance} entity.
 */
public class AdvanceDTO implements Serializable {

    private Long id;

    private String reference;

    private String salaryAdvanceReason;

    private String salaryAdvanceReasonDetails;

    private String salaryAdvanceTypes;

    private Double ceilingSalaryAdvanceTypes;

    private String degreeOfUrgency;

    private LocalDate date;

    private Double maximumSalaryAdvance;

    private Double salaryAdvanceAmount;

    private Double repaymentPeriod;

    private String salaryDeductionMethod;

    private Double totalAmountMonthlyPaymentDeducte;

    private Double salaryMonthlyPaymentDeducte;

    private Double primesMonthlyPaymentDeducte;

    private String status;

    private String reasonRejection;

    private MembersDTO members;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSalaryAdvanceReason() {
        return salaryAdvanceReason;
    }

    public void setSalaryAdvanceReason(String salaryAdvanceReason) {
        this.salaryAdvanceReason = salaryAdvanceReason;
    }

    public String getSalaryAdvanceReasonDetails() {
        return salaryAdvanceReasonDetails;
    }

    public void setSalaryAdvanceReasonDetails(String salaryAdvanceReasonDetails) {
        this.salaryAdvanceReasonDetails = salaryAdvanceReasonDetails;
    }

    public String getSalaryAdvanceTypes() {
        return salaryAdvanceTypes;
    }

    public void setSalaryAdvanceTypes(String salaryAdvanceTypes) {
        this.salaryAdvanceTypes = salaryAdvanceTypes;
    }

    public Double getCeilingSalaryAdvanceTypes() {
        return ceilingSalaryAdvanceTypes;
    }

    public void setCeilingSalaryAdvanceTypes(Double ceilingSalaryAdvanceTypes) {
        this.ceilingSalaryAdvanceTypes = ceilingSalaryAdvanceTypes;
    }

    public String getDegreeOfUrgency() {
        return degreeOfUrgency;
    }

    public void setDegreeOfUrgency(String degreeOfUrgency) {
        this.degreeOfUrgency = degreeOfUrgency;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getMaximumSalaryAdvance() {
        return maximumSalaryAdvance;
    }

    public void setMaximumSalaryAdvance(Double maximumSalaryAdvance) {
        this.maximumSalaryAdvance = maximumSalaryAdvance;
    }

    public Double getSalaryAdvanceAmount() {
        return salaryAdvanceAmount;
    }

    public void setSalaryAdvanceAmount(Double salaryAdvanceAmount) {
        this.salaryAdvanceAmount = salaryAdvanceAmount;
    }

    public Double getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(Double repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public String getSalaryDeductionMethod() {
        return salaryDeductionMethod;
    }

    public void setSalaryDeductionMethod(String salaryDeductionMethod) {
        this.salaryDeductionMethod = salaryDeductionMethod;
    }

    public Double getTotalAmountMonthlyPaymentDeducte() {
        return totalAmountMonthlyPaymentDeducte;
    }

    public void setTotalAmountMonthlyPaymentDeducte(Double totalAmountMonthlyPaymentDeducte) {
        this.totalAmountMonthlyPaymentDeducte = totalAmountMonthlyPaymentDeducte;
    }

    public Double getSalaryMonthlyPaymentDeducte() {
        return salaryMonthlyPaymentDeducte;
    }

    public void setSalaryMonthlyPaymentDeducte(Double salaryMonthlyPaymentDeducte) {
        this.salaryMonthlyPaymentDeducte = salaryMonthlyPaymentDeducte;
    }

    public Double getPrimesMonthlyPaymentDeducte() {
        return primesMonthlyPaymentDeducte;
    }

    public void setPrimesMonthlyPaymentDeducte(Double primesMonthlyPaymentDeducte) {
        this.primesMonthlyPaymentDeducte = primesMonthlyPaymentDeducte;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReasonRejection() {
        return reasonRejection;
    }

    public void setReasonRejection(String reasonRejection) {
        this.reasonRejection = reasonRejection;
    }

    public MembersDTO getMembers() {
        return members;
    }

    public void setMembers(MembersDTO members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdvanceDTO)) {
            return false;
        }

        AdvanceDTO advanceDTO = (AdvanceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, advanceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdvanceDTO{" +
            "id=" + getId() +
            ", reference='" + getReference() + "'" +
            ", salaryAdvanceReason='" + getSalaryAdvanceReason() + "'" +
            ", salaryAdvanceReasonDetails='" + getSalaryAdvanceReasonDetails() + "'" +
            ", salaryAdvanceTypes='" + getSalaryAdvanceTypes() + "'" +
            ", ceilingSalaryAdvanceTypes=" + getCeilingSalaryAdvanceTypes() +
            ", degreeOfUrgency='" + getDegreeOfUrgency() + "'" +
            ", date='" + getDate() + "'" +
            ", maximumSalaryAdvance=" + getMaximumSalaryAdvance() +
            ", salaryAdvanceAmount=" + getSalaryAdvanceAmount() +
            ", repaymentPeriod=" + getRepaymentPeriod() +
            ", salaryDeductionMethod='" + getSalaryDeductionMethod() + "'" +
            ", totalAmountMonthlyPaymentDeducte=" + getTotalAmountMonthlyPaymentDeducte() +
            ", salaryMonthlyPaymentDeducte=" + getSalaryMonthlyPaymentDeducte() +
            ", primesMonthlyPaymentDeducte=" + getPrimesMonthlyPaymentDeducte() +
            ", status='" + getStatus() + "'" +
            ", reasonRejection='" + getReasonRejection() + "'" +
            ", members=" + getMembers() +
            "}";
    }
}
