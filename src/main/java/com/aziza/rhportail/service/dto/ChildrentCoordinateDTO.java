package com.aziza.rhportail.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.aziza.rhportail.domain.ChildrentCoordinate} entity.
 */
public class ChildrentCoordinateDTO implements Serializable {

    private Long id;

    private String firstNameChild;

    private String lastNameChild;

    private String dateOfBirthChild;

    private String schoolLevel;

    private Boolean handicapped;

    private MembersDTO members;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstNameChild() {
        return firstNameChild;
    }

    public void setFirstNameChild(String firstNameChild) {
        this.firstNameChild = firstNameChild;
    }

    public String getLastNameChild() {
        return lastNameChild;
    }

    public void setLastNameChild(String lastNameChild) {
        this.lastNameChild = lastNameChild;
    }

    public String getDateOfBirthChild() {
        return dateOfBirthChild;
    }

    public void setDateOfBirthChild(String dateOfBirthChild) {
        this.dateOfBirthChild = dateOfBirthChild;
    }

    public String getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(String schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public Boolean getHandicapped() {
        return handicapped;
    }

    public void setHandicapped(Boolean handicapped) {
        this.handicapped = handicapped;
    }

    public MembersDTO getMembers() {
        return members;
    }

    public void setMembers(MembersDTO members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChildrentCoordinateDTO)) {
            return false;
        }

        ChildrentCoordinateDTO childrentCoordinateDTO = (ChildrentCoordinateDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, childrentCoordinateDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChildrentCoordinateDTO{" +
            "id=" + getId() +
            ", firstNameChild='" + getFirstNameChild() + "'" +
            ", lastNameChild='" + getLastNameChild() + "'" +
            ", dateOfBirthChild='" + getDateOfBirthChild() + "'" +
            ", schoolLevel='" + getSchoolLevel() + "'" +
            ", handicapped='" + getHandicapped() + "'" +
            ", members=" + getMembers() +
            "}";
    }
}
