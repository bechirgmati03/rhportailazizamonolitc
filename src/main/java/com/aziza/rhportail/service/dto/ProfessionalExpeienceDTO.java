package com.aziza.rhportail.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.aziza.rhportail.domain.ProfessionalExpeience} entity.
 */
public class ProfessionalExpeienceDTO implements Serializable {

    private Long id;

    private String company;

    private LocalDate startDate;

    private LocalDate endDate;

    private String poste;

    private String departement;

    private String store;

    private MembersDTO members;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public MembersDTO getMembers() {
        return members;
    }

    public void setMembers(MembersDTO members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfessionalExpeienceDTO)) {
            return false;
        }

        ProfessionalExpeienceDTO professionalExpeienceDTO = (ProfessionalExpeienceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, professionalExpeienceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfessionalExpeienceDTO{" +
            "id=" + getId() +
            ", company='" + getCompany() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", poste='" + getPoste() + "'" +
            ", departement='" + getDepartement() + "'" +
            ", store='" + getStore() + "'" +
            ", members=" + getMembers() +
            "}";
    }
}
