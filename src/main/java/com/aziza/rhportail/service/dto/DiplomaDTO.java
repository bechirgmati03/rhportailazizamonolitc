package com.aziza.rhportail.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.aziza.rhportail.domain.Diploma} entity.
 */
public class DiplomaDTO implements Serializable {

    private Long id;

    private String institution;

    private Integer yearOfGraduation;

    private String diplomaTitle;

    private MembersDTO members;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Integer getYearOfGraduation() {
        return yearOfGraduation;
    }

    public void setYearOfGraduation(Integer yearOfGraduation) {
        this.yearOfGraduation = yearOfGraduation;
    }

    public String getDiplomaTitle() {
        return diplomaTitle;
    }

    public void setDiplomaTitle(String diplomaTitle) {
        this.diplomaTitle = diplomaTitle;
    }

    public MembersDTO getMembers() {
        return members;
    }

    public void setMembers(MembersDTO members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DiplomaDTO)) {
            return false;
        }

        DiplomaDTO diplomaDTO = (DiplomaDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, diplomaDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DiplomaDTO{" +
            "id=" + getId() +
            ", institution='" + getInstitution() + "'" +
            ", yearOfGraduation=" + getYearOfGraduation() +
            ", diplomaTitle='" + getDiplomaTitle() + "'" +
            ", members=" + getMembers() +
            "}";
    }
}
