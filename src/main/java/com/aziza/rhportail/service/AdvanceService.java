package com.aziza.rhportail.service;

import com.aziza.rhportail.service.dto.AdvanceDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.aziza.rhportail.domain.Advance}.
 */
public interface AdvanceService {
    /**
     * Save a advance.
     *
     * @param advanceDTO the entity to save.
     * @return the persisted entity.
     */
    AdvanceDTO save(AdvanceDTO advanceDTO);

    /**
     * Partially updates a advance.
     *
     * @param advanceDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AdvanceDTO> partialUpdate(AdvanceDTO advanceDTO);

    /**
     * Get all the advances.
     *
     * @return the list of entities.
     */
    List<AdvanceDTO> findAll();

    /**
     * Get the "id" advance.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AdvanceDTO> findOne(Long id);

    /**
     * Delete the "id" advance.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
