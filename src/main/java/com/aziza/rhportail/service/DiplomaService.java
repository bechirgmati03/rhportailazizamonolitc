package com.aziza.rhportail.service;

import com.aziza.rhportail.service.dto.DiplomaDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.aziza.rhportail.domain.Diploma}.
 */
public interface DiplomaService {
    /**
     * Save a diploma.
     *
     * @param diplomaDTO the entity to save.
     * @return the persisted entity.
     */
    DiplomaDTO save(DiplomaDTO diplomaDTO);

    /**
     * Partially updates a diploma.
     *
     * @param diplomaDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DiplomaDTO> partialUpdate(DiplomaDTO diplomaDTO);

    /**
     * Get all the diplomas.
     *
     * @return the list of entities.
     */
    List<DiplomaDTO> findAll();

    /**
     * Get the "id" diploma.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DiplomaDTO> findOne(Long id);

    /**
     * Delete the "id" diploma.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
