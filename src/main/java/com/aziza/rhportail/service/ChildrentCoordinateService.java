package com.aziza.rhportail.service;

import com.aziza.rhportail.service.dto.ChildrentCoordinateDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.aziza.rhportail.domain.ChildrentCoordinate}.
 */
public interface ChildrentCoordinateService {
    /**
     * Save a childrentCoordinate.
     *
     * @param childrentCoordinateDTO the entity to save.
     * @return the persisted entity.
     */
    ChildrentCoordinateDTO save(ChildrentCoordinateDTO childrentCoordinateDTO);

    /**
     * Partially updates a childrentCoordinate.
     *
     * @param childrentCoordinateDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ChildrentCoordinateDTO> partialUpdate(ChildrentCoordinateDTO childrentCoordinateDTO);

    /**
     * Get all the childrentCoordinates.
     *
     * @return the list of entities.
     */
    List<ChildrentCoordinateDTO> findAll();

    /**
     * Get the "id" childrentCoordinate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ChildrentCoordinateDTO> findOne(Long id);

    /**
     * Delete the "id" childrentCoordinate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
