package com.aziza.rhportail.service.mapper;

import com.aziza.rhportail.domain.Advance;
import com.aziza.rhportail.service.dto.AdvanceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Advance} and its DTO {@link AdvanceDTO}.
 */
@Mapper(componentModel = "spring", uses = { MembersMapper.class })
public interface AdvanceMapper extends EntityMapper<AdvanceDTO, Advance> {
    @Mapping(target = "members", source = "members", qualifiedByName = "id")
    AdvanceDTO toDto(Advance s);
}
