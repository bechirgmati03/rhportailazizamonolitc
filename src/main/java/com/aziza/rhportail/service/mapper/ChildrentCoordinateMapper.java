package com.aziza.rhportail.service.mapper;

import com.aziza.rhportail.domain.ChildrentCoordinate;
import com.aziza.rhportail.service.dto.ChildrentCoordinateDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChildrentCoordinate} and its DTO {@link ChildrentCoordinateDTO}.
 */
@Mapper(componentModel = "spring", uses = { MembersMapper.class })
public interface ChildrentCoordinateMapper extends EntityMapper<ChildrentCoordinateDTO, ChildrentCoordinate> {
    @Mapping(target = "members", source = "members", qualifiedByName = "id")
    ChildrentCoordinateDTO toDto(ChildrentCoordinate s);
}
