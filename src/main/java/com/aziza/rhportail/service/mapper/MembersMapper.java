package com.aziza.rhportail.service.mapper;

import com.aziza.rhportail.domain.Members;
import com.aziza.rhportail.service.dto.MembersDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Members} and its DTO {@link MembersDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface MembersMapper extends EntityMapper<MembersDTO, Members> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    MembersDTO toDto(Members s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MembersDTO toDtoId(Members members);
}
