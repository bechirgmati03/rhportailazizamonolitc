package com.aziza.rhportail.service.mapper;

import com.aziza.rhportail.domain.ProfessionalExpeience;
import com.aziza.rhportail.service.dto.ProfessionalExpeienceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProfessionalExpeience} and its DTO {@link ProfessionalExpeienceDTO}.
 */
@Mapper(componentModel = "spring", uses = { MembersMapper.class })
public interface ProfessionalExpeienceMapper extends EntityMapper<ProfessionalExpeienceDTO, ProfessionalExpeience> {
    @Mapping(target = "members", source = "members", qualifiedByName = "id")
    ProfessionalExpeienceDTO toDto(ProfessionalExpeience s);
}
