package com.aziza.rhportail.service.mapper;

import com.aziza.rhportail.domain.Diploma;
import com.aziza.rhportail.service.dto.DiplomaDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Diploma} and its DTO {@link DiplomaDTO}.
 */
@Mapper(componentModel = "spring", uses = { MembersMapper.class })
public interface DiplomaMapper extends EntityMapper<DiplomaDTO, Diploma> {
    @Mapping(target = "members", source = "members", qualifiedByName = "id")
    DiplomaDTO toDto(Diploma s);
}
