package com.aziza.rhportail.service.impl;

import com.aziza.rhportail.domain.ChildrentCoordinate;
import com.aziza.rhportail.repository.ChildrentCoordinateRepository;
import com.aziza.rhportail.service.ChildrentCoordinateService;
import com.aziza.rhportail.service.dto.ChildrentCoordinateDTO;
import com.aziza.rhportail.service.mapper.ChildrentCoordinateMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ChildrentCoordinate}.
 */
@Service
@Transactional
public class ChildrentCoordinateServiceImpl implements ChildrentCoordinateService {

    private final Logger log = LoggerFactory.getLogger(ChildrentCoordinateServiceImpl.class);

    private final ChildrentCoordinateRepository childrentCoordinateRepository;

    private final ChildrentCoordinateMapper childrentCoordinateMapper;

    public ChildrentCoordinateServiceImpl(
        ChildrentCoordinateRepository childrentCoordinateRepository,
        ChildrentCoordinateMapper childrentCoordinateMapper
    ) {
        this.childrentCoordinateRepository = childrentCoordinateRepository;
        this.childrentCoordinateMapper = childrentCoordinateMapper;
    }

    @Override
    public ChildrentCoordinateDTO save(ChildrentCoordinateDTO childrentCoordinateDTO) {
        log.debug("Request to save ChildrentCoordinate : {}", childrentCoordinateDTO);
        ChildrentCoordinate childrentCoordinate = childrentCoordinateMapper.toEntity(childrentCoordinateDTO);
        childrentCoordinate = childrentCoordinateRepository.save(childrentCoordinate);
        return childrentCoordinateMapper.toDto(childrentCoordinate);
    }

    @Override
    public Optional<ChildrentCoordinateDTO> partialUpdate(ChildrentCoordinateDTO childrentCoordinateDTO) {
        log.debug("Request to partially update ChildrentCoordinate : {}", childrentCoordinateDTO);

        return childrentCoordinateRepository
            .findById(childrentCoordinateDTO.getId())
            .map(existingChildrentCoordinate -> {
                childrentCoordinateMapper.partialUpdate(existingChildrentCoordinate, childrentCoordinateDTO);

                return existingChildrentCoordinate;
            })
            .map(childrentCoordinateRepository::save)
            .map(childrentCoordinateMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChildrentCoordinateDTO> findAll() {
        log.debug("Request to get all ChildrentCoordinates");
        return childrentCoordinateRepository
            .findAll()
            .stream()
            .map(childrentCoordinateMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ChildrentCoordinateDTO> findOne(Long id) {
        log.debug("Request to get ChildrentCoordinate : {}", id);
        return childrentCoordinateRepository.findById(id).map(childrentCoordinateMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ChildrentCoordinate : {}", id);
        childrentCoordinateRepository.deleteById(id);
    }
}
