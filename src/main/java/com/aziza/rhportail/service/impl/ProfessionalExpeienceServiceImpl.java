package com.aziza.rhportail.service.impl;

import com.aziza.rhportail.domain.ProfessionalExpeience;
import com.aziza.rhportail.repository.ProfessionalExpeienceRepository;
import com.aziza.rhportail.service.ProfessionalExpeienceService;
import com.aziza.rhportail.service.dto.ProfessionalExpeienceDTO;
import com.aziza.rhportail.service.mapper.ProfessionalExpeienceMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ProfessionalExpeience}.
 */
@Service
@Transactional
public class ProfessionalExpeienceServiceImpl implements ProfessionalExpeienceService {

    private final Logger log = LoggerFactory.getLogger(ProfessionalExpeienceServiceImpl.class);

    private final ProfessionalExpeienceRepository professionalExpeienceRepository;

    private final ProfessionalExpeienceMapper professionalExpeienceMapper;

    public ProfessionalExpeienceServiceImpl(
        ProfessionalExpeienceRepository professionalExpeienceRepository,
        ProfessionalExpeienceMapper professionalExpeienceMapper
    ) {
        this.professionalExpeienceRepository = professionalExpeienceRepository;
        this.professionalExpeienceMapper = professionalExpeienceMapper;
    }

    @Override
    public ProfessionalExpeienceDTO save(ProfessionalExpeienceDTO professionalExpeienceDTO) {
        log.debug("Request to save ProfessionalExpeience : {}", professionalExpeienceDTO);
        ProfessionalExpeience professionalExpeience = professionalExpeienceMapper.toEntity(professionalExpeienceDTO);
        professionalExpeience = professionalExpeienceRepository.save(professionalExpeience);
        return professionalExpeienceMapper.toDto(professionalExpeience);
    }

    @Override
    public Optional<ProfessionalExpeienceDTO> partialUpdate(ProfessionalExpeienceDTO professionalExpeienceDTO) {
        log.debug("Request to partially update ProfessionalExpeience : {}", professionalExpeienceDTO);

        return professionalExpeienceRepository
            .findById(professionalExpeienceDTO.getId())
            .map(existingProfessionalExpeience -> {
                professionalExpeienceMapper.partialUpdate(existingProfessionalExpeience, professionalExpeienceDTO);

                return existingProfessionalExpeience;
            })
            .map(professionalExpeienceRepository::save)
            .map(professionalExpeienceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessionalExpeienceDTO> findAll() {
        log.debug("Request to get all ProfessionalExpeiences");
        return professionalExpeienceRepository
            .findAll()
            .stream()
            .map(professionalExpeienceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ProfessionalExpeienceDTO> findOne(Long id) {
        log.debug("Request to get ProfessionalExpeience : {}", id);
        return professionalExpeienceRepository.findById(id).map(professionalExpeienceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProfessionalExpeience : {}", id);
        professionalExpeienceRepository.deleteById(id);
    }
}
