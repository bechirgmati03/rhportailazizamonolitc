package com.aziza.rhportail.service.impl;

import com.aziza.rhportail.domain.Advance;
import com.aziza.rhportail.repository.AdvanceRepository;
import com.aziza.rhportail.service.AdvanceService;
import com.aziza.rhportail.service.dto.AdvanceDTO;
import com.aziza.rhportail.service.mapper.AdvanceMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Advance}.
 */
@Service
@Transactional
public class AdvanceServiceImpl implements AdvanceService {

    private final Logger log = LoggerFactory.getLogger(AdvanceServiceImpl.class);

    private final AdvanceRepository advanceRepository;

    private final AdvanceMapper advanceMapper;

    public AdvanceServiceImpl(AdvanceRepository advanceRepository, AdvanceMapper advanceMapper) {
        this.advanceRepository = advanceRepository;
        this.advanceMapper = advanceMapper;
    }

    @Override
    public AdvanceDTO save(AdvanceDTO advanceDTO) {
        log.debug("Request to save Advance : {}", advanceDTO);
        Advance advance = advanceMapper.toEntity(advanceDTO);
        advance = advanceRepository.save(advance);
        return advanceMapper.toDto(advance);
    }

    @Override
    public Optional<AdvanceDTO> partialUpdate(AdvanceDTO advanceDTO) {
        log.debug("Request to partially update Advance : {}", advanceDTO);

        return advanceRepository
            .findById(advanceDTO.getId())
            .map(existingAdvance -> {
                advanceMapper.partialUpdate(existingAdvance, advanceDTO);

                return existingAdvance;
            })
            .map(advanceRepository::save)
            .map(advanceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdvanceDTO> findAll() {
        log.debug("Request to get all Advances");
        return advanceRepository.findAll().stream().map(advanceMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AdvanceDTO> findOne(Long id) {
        log.debug("Request to get Advance : {}", id);
        return advanceRepository.findById(id).map(advanceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Advance : {}", id);
        advanceRepository.deleteById(id);
    }
}
