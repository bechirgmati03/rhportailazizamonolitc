package com.aziza.rhportail.service.impl;

import com.aziza.rhportail.domain.Members;
import com.aziza.rhportail.repository.MembersRepository;
import com.aziza.rhportail.service.MembersService;
import com.aziza.rhportail.service.dto.MembersDTO;
import com.aziza.rhportail.service.mapper.MembersMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Members}.
 */
@Service
@Transactional
public class MembersServiceImpl implements MembersService {

    private final Logger log = LoggerFactory.getLogger(MembersServiceImpl.class);

    private final MembersRepository membersRepository;

    private final MembersMapper membersMapper;

    public MembersServiceImpl(MembersRepository membersRepository, MembersMapper membersMapper) {
        this.membersRepository = membersRepository;
        this.membersMapper = membersMapper;
    }

    @Override
    public MembersDTO save(MembersDTO membersDTO) {
        log.debug("Request to save Members : {}", membersDTO);
        Members members = membersMapper.toEntity(membersDTO);
        members = membersRepository.save(members);
        return membersMapper.toDto(members);
    }

    @Override
    public Optional<MembersDTO> partialUpdate(MembersDTO membersDTO) {
        log.debug("Request to partially update Members : {}", membersDTO);

        return membersRepository
            .findById(membersDTO.getId())
            .map(existingMembers -> {
                membersMapper.partialUpdate(existingMembers, membersDTO);

                return existingMembers;
            })
            .map(membersRepository::save)
            .map(membersMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MembersDTO> findAll() {
        log.debug("Request to get all Members");
        return membersRepository.findAll().stream().map(membersMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MembersDTO> findOne(Long id) {
        log.debug("Request to get Members : {}", id);
        return membersRepository.findById(id).map(membersMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Members : {}", id);
        membersRepository.deleteById(id);
    }
}
