package com.aziza.rhportail.service.impl;

import com.aziza.rhportail.domain.Diploma;
import com.aziza.rhportail.repository.DiplomaRepository;
import com.aziza.rhportail.service.DiplomaService;
import com.aziza.rhportail.service.dto.DiplomaDTO;
import com.aziza.rhportail.service.mapper.DiplomaMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Diploma}.
 */
@Service
@Transactional
public class DiplomaServiceImpl implements DiplomaService {

    private final Logger log = LoggerFactory.getLogger(DiplomaServiceImpl.class);

    private final DiplomaRepository diplomaRepository;

    private final DiplomaMapper diplomaMapper;

    public DiplomaServiceImpl(DiplomaRepository diplomaRepository, DiplomaMapper diplomaMapper) {
        this.diplomaRepository = diplomaRepository;
        this.diplomaMapper = diplomaMapper;
    }

    @Override
    public DiplomaDTO save(DiplomaDTO diplomaDTO) {
        log.debug("Request to save Diploma : {}", diplomaDTO);
        Diploma diploma = diplomaMapper.toEntity(diplomaDTO);
        diploma = diplomaRepository.save(diploma);
        return diplomaMapper.toDto(diploma);
    }

    @Override
    public Optional<DiplomaDTO> partialUpdate(DiplomaDTO diplomaDTO) {
        log.debug("Request to partially update Diploma : {}", diplomaDTO);

        return diplomaRepository
            .findById(diplomaDTO.getId())
            .map(existingDiploma -> {
                diplomaMapper.partialUpdate(existingDiploma, diplomaDTO);

                return existingDiploma;
            })
            .map(diplomaRepository::save)
            .map(diplomaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DiplomaDTO> findAll() {
        log.debug("Request to get all Diplomas");
        return diplomaRepository.findAll().stream().map(diplomaMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DiplomaDTO> findOne(Long id) {
        log.debug("Request to get Diploma : {}", id);
        return diplomaRepository.findById(id).map(diplomaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Diploma : {}", id);
        diplomaRepository.deleteById(id);
    }
}
