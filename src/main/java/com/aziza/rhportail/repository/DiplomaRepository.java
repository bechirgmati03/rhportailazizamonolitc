package com.aziza.rhportail.repository;

import com.aziza.rhportail.domain.Diploma;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Diploma entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiplomaRepository extends JpaRepository<Diploma, Long> {}
