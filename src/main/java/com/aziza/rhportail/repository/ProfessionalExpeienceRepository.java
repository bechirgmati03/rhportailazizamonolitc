package com.aziza.rhportail.repository;

import com.aziza.rhportail.domain.ProfessionalExpeience;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ProfessionalExpeience entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfessionalExpeienceRepository extends JpaRepository<ProfessionalExpeience, Long> {}
