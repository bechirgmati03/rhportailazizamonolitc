package com.aziza.rhportail.repository;

import com.aziza.rhportail.domain.ChildrentCoordinate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChildrentCoordinate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChildrentCoordinateRepository extends JpaRepository<ChildrentCoordinate, Long> {}
