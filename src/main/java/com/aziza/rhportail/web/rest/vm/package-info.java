/**
 * View Models used by Spring MVC REST controllers.
 */
package com.aziza.rhportail.web.rest.vm;
