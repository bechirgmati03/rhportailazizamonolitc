package com.aziza.rhportail.web.rest;

import com.aziza.rhportail.repository.MembersRepository;
import com.aziza.rhportail.service.MembersService;
import com.aziza.rhportail.service.dto.MembersDTO;
import com.aziza.rhportail.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.aziza.rhportail.domain.Members}.
 */
@RestController
@RequestMapping("/api")
public class MembersResource {

    private final Logger log = LoggerFactory.getLogger(MembersResource.class);

    private static final String ENTITY_NAME = "members";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MembersService membersService;

    private final MembersRepository membersRepository;

    public MembersResource(MembersService membersService, MembersRepository membersRepository) {
        this.membersService = membersService;
        this.membersRepository = membersRepository;
    }

    /**
     * {@code POST  /members} : Create a new members.
     *
     * @param membersDTO the membersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new membersDTO, or with status {@code 400 (Bad Request)} if the members has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/members")
    public ResponseEntity<MembersDTO> createMembers(@RequestBody MembersDTO membersDTO) throws URISyntaxException {
        log.debug("REST request to save Members : {}", membersDTO);
        if (membersDTO.getId() != null) {
            throw new BadRequestAlertException("A new members cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MembersDTO result = membersService.save(membersDTO);
        return ResponseEntity
            .created(new URI("/api/members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /members/:id} : Updates an existing members.
     *
     * @param id the id of the membersDTO to save.
     * @param membersDTO the membersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membersDTO,
     * or with status {@code 400 (Bad Request)} if the membersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the membersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/members/{id}")
    public ResponseEntity<MembersDTO> updateMembers(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MembersDTO membersDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Members : {}, {}", id, membersDTO);
        if (membersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membersDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membersRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MembersDTO result = membersService.save(membersDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /members/:id} : Partial updates given fields of an existing members, field will ignore if it is null
     *
     * @param id the id of the membersDTO to save.
     * @param membersDTO the membersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membersDTO,
     * or with status {@code 400 (Bad Request)} if the membersDTO is not valid,
     * or with status {@code 404 (Not Found)} if the membersDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the membersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/members/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MembersDTO> partialUpdateMembers(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MembersDTO membersDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Members partially : {}, {}", id, membersDTO);
        if (membersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membersDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membersRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MembersDTO> result = membersService.partialUpdate(membersDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membersDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /members} : get all the members.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of members in body.
     */
    @GetMapping("/members")
    public List<MembersDTO> getAllMembers() {
        log.debug("REST request to get all Members");
        return membersService.findAll();
    }

    /**
     * {@code GET  /members/:id} : get the "id" members.
     *
     * @param id the id of the membersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the membersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/members/{id}")
    public ResponseEntity<MembersDTO> getMembers(@PathVariable Long id) {
        log.debug("REST request to get Members : {}", id);
        Optional<MembersDTO> membersDTO = membersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(membersDTO);
    }

    /**
     * {@code DELETE  /members/:id} : delete the "id" members.
     *
     * @param id the id of the membersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/members/{id}")
    public ResponseEntity<Void> deleteMembers(@PathVariable Long id) {
        log.debug("REST request to delete Members : {}", id);
        membersService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
