package com.aziza.rhportail.web.rest;

import com.aziza.rhportail.repository.DiplomaRepository;
import com.aziza.rhportail.service.DiplomaService;
import com.aziza.rhportail.service.dto.DiplomaDTO;
import com.aziza.rhportail.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.aziza.rhportail.domain.Diploma}.
 */
@RestController
@RequestMapping("/api")
public class DiplomaResource {

    private final Logger log = LoggerFactory.getLogger(DiplomaResource.class);

    private static final String ENTITY_NAME = "diploma";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DiplomaService diplomaService;

    private final DiplomaRepository diplomaRepository;

    public DiplomaResource(DiplomaService diplomaService, DiplomaRepository diplomaRepository) {
        this.diplomaService = diplomaService;
        this.diplomaRepository = diplomaRepository;
    }

    /**
     * {@code POST  /diplomas} : Create a new diploma.
     *
     * @param diplomaDTO the diplomaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new diplomaDTO, or with status {@code 400 (Bad Request)} if the diploma has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diplomas")
    public ResponseEntity<DiplomaDTO> createDiploma(@RequestBody DiplomaDTO diplomaDTO) throws URISyntaxException {
        log.debug("REST request to save Diploma : {}", diplomaDTO);
        if (diplomaDTO.getId() != null) {
            throw new BadRequestAlertException("A new diploma cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DiplomaDTO result = diplomaService.save(diplomaDTO);
        return ResponseEntity
            .created(new URI("/api/diplomas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diplomas/:id} : Updates an existing diploma.
     *
     * @param id the id of the diplomaDTO to save.
     * @param diplomaDTO the diplomaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diplomaDTO,
     * or with status {@code 400 (Bad Request)} if the diplomaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the diplomaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diplomas/{id}")
    public ResponseEntity<DiplomaDTO> updateDiploma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DiplomaDTO diplomaDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Diploma : {}, {}", id, diplomaDTO);
        if (diplomaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, diplomaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!diplomaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DiplomaDTO result = diplomaService.save(diplomaDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diplomaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /diplomas/:id} : Partial updates given fields of an existing diploma, field will ignore if it is null
     *
     * @param id the id of the diplomaDTO to save.
     * @param diplomaDTO the diplomaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diplomaDTO,
     * or with status {@code 400 (Bad Request)} if the diplomaDTO is not valid,
     * or with status {@code 404 (Not Found)} if the diplomaDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the diplomaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/diplomas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DiplomaDTO> partialUpdateDiploma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DiplomaDTO diplomaDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Diploma partially : {}, {}", id, diplomaDTO);
        if (diplomaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, diplomaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!diplomaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DiplomaDTO> result = diplomaService.partialUpdate(diplomaDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diplomaDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /diplomas} : get all the diplomas.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of diplomas in body.
     */
    @GetMapping("/diplomas")
    public List<DiplomaDTO> getAllDiplomas() {
        log.debug("REST request to get all Diplomas");
        return diplomaService.findAll();
    }

    /**
     * {@code GET  /diplomas/:id} : get the "id" diploma.
     *
     * @param id the id of the diplomaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the diplomaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diplomas/{id}")
    public ResponseEntity<DiplomaDTO> getDiploma(@PathVariable Long id) {
        log.debug("REST request to get Diploma : {}", id);
        Optional<DiplomaDTO> diplomaDTO = diplomaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(diplomaDTO);
    }

    /**
     * {@code DELETE  /diplomas/:id} : delete the "id" diploma.
     *
     * @param id the id of the diplomaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diplomas/{id}")
    public ResponseEntity<Void> deleteDiploma(@PathVariable Long id) {
        log.debug("REST request to delete Diploma : {}", id);
        diplomaService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
