package com.aziza.rhportail.web.rest;

import com.aziza.rhportail.repository.ProfessionalExpeienceRepository;
import com.aziza.rhportail.service.ProfessionalExpeienceService;
import com.aziza.rhportail.service.dto.ProfessionalExpeienceDTO;
import com.aziza.rhportail.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.aziza.rhportail.domain.ProfessionalExpeience}.
 */
@RestController
@RequestMapping("/api")
public class ProfessionalExpeienceResource {

    private final Logger log = LoggerFactory.getLogger(ProfessionalExpeienceResource.class);

    private static final String ENTITY_NAME = "professionalExpeience";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfessionalExpeienceService professionalExpeienceService;

    private final ProfessionalExpeienceRepository professionalExpeienceRepository;

    public ProfessionalExpeienceResource(
        ProfessionalExpeienceService professionalExpeienceService,
        ProfessionalExpeienceRepository professionalExpeienceRepository
    ) {
        this.professionalExpeienceService = professionalExpeienceService;
        this.professionalExpeienceRepository = professionalExpeienceRepository;
    }

    /**
     * {@code POST  /professional-expeiences} : Create a new professionalExpeience.
     *
     * @param professionalExpeienceDTO the professionalExpeienceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new professionalExpeienceDTO, or with status {@code 400 (Bad Request)} if the professionalExpeience has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/professional-expeiences")
    public ResponseEntity<ProfessionalExpeienceDTO> createProfessionalExpeience(
        @RequestBody ProfessionalExpeienceDTO professionalExpeienceDTO
    ) throws URISyntaxException {
        log.debug("REST request to save ProfessionalExpeience : {}", professionalExpeienceDTO);
        if (professionalExpeienceDTO.getId() != null) {
            throw new BadRequestAlertException("A new professionalExpeience cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProfessionalExpeienceDTO result = professionalExpeienceService.save(professionalExpeienceDTO);
        return ResponseEntity
            .created(new URI("/api/professional-expeiences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /professional-expeiences/:id} : Updates an existing professionalExpeience.
     *
     * @param id the id of the professionalExpeienceDTO to save.
     * @param professionalExpeienceDTO the professionalExpeienceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated professionalExpeienceDTO,
     * or with status {@code 400 (Bad Request)} if the professionalExpeienceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the professionalExpeienceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/professional-expeiences/{id}")
    public ResponseEntity<ProfessionalExpeienceDTO> updateProfessionalExpeience(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ProfessionalExpeienceDTO professionalExpeienceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ProfessionalExpeience : {}, {}", id, professionalExpeienceDTO);
        if (professionalExpeienceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, professionalExpeienceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!professionalExpeienceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ProfessionalExpeienceDTO result = professionalExpeienceService.save(professionalExpeienceDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, professionalExpeienceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /professional-expeiences/:id} : Partial updates given fields of an existing professionalExpeience, field will ignore if it is null
     *
     * @param id the id of the professionalExpeienceDTO to save.
     * @param professionalExpeienceDTO the professionalExpeienceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated professionalExpeienceDTO,
     * or with status {@code 400 (Bad Request)} if the professionalExpeienceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the professionalExpeienceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the professionalExpeienceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/professional-expeiences/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ProfessionalExpeienceDTO> partialUpdateProfessionalExpeience(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ProfessionalExpeienceDTO professionalExpeienceDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ProfessionalExpeience partially : {}, {}", id, professionalExpeienceDTO);
        if (professionalExpeienceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, professionalExpeienceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!professionalExpeienceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ProfessionalExpeienceDTO> result = professionalExpeienceService.partialUpdate(professionalExpeienceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, professionalExpeienceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /professional-expeiences} : get all the professionalExpeiences.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of professionalExpeiences in body.
     */
    @GetMapping("/professional-expeiences")
    public List<ProfessionalExpeienceDTO> getAllProfessionalExpeiences() {
        log.debug("REST request to get all ProfessionalExpeiences");
        return professionalExpeienceService.findAll();
    }

    /**
     * {@code GET  /professional-expeiences/:id} : get the "id" professionalExpeience.
     *
     * @param id the id of the professionalExpeienceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the professionalExpeienceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/professional-expeiences/{id}")
    public ResponseEntity<ProfessionalExpeienceDTO> getProfessionalExpeience(@PathVariable Long id) {
        log.debug("REST request to get ProfessionalExpeience : {}", id);
        Optional<ProfessionalExpeienceDTO> professionalExpeienceDTO = professionalExpeienceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(professionalExpeienceDTO);
    }

    /**
     * {@code DELETE  /professional-expeiences/:id} : delete the "id" professionalExpeience.
     *
     * @param id the id of the professionalExpeienceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/professional-expeiences/{id}")
    public ResponseEntity<Void> deleteProfessionalExpeience(@PathVariable Long id) {
        log.debug("REST request to delete ProfessionalExpeience : {}", id);
        professionalExpeienceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
