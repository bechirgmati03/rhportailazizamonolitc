package com.aziza.rhportail.web.rest;

import com.aziza.rhportail.repository.AdvanceRepository;
import com.aziza.rhportail.service.AdvanceService;
import com.aziza.rhportail.service.dto.AdvanceDTO;
import com.aziza.rhportail.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.aziza.rhportail.domain.Advance}.
 */
@RestController
@RequestMapping("/api")
public class AdvanceResource {

    private final Logger log = LoggerFactory.getLogger(AdvanceResource.class);

    private static final String ENTITY_NAME = "advance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdvanceService advanceService;

    private final AdvanceRepository advanceRepository;

    public AdvanceResource(AdvanceService advanceService, AdvanceRepository advanceRepository) {
        this.advanceService = advanceService;
        this.advanceRepository = advanceRepository;
    }

    /**
     * {@code POST  /advances} : Create a new advance.
     *
     * @param advanceDTO the advanceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new advanceDTO, or with status {@code 400 (Bad Request)} if the advance has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/advances")
    public ResponseEntity<AdvanceDTO> createAdvance(@RequestBody AdvanceDTO advanceDTO) throws URISyntaxException {
        log.debug("REST request to save Advance : {}", advanceDTO);
        if (advanceDTO.getId() != null) {
            throw new BadRequestAlertException("A new advance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvanceDTO result = advanceService.save(advanceDTO);
        return ResponseEntity
            .created(new URI("/api/advances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /advances/:id} : Updates an existing advance.
     *
     * @param id the id of the advanceDTO to save.
     * @param advanceDTO the advanceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated advanceDTO,
     * or with status {@code 400 (Bad Request)} if the advanceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the advanceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/advances/{id}")
    public ResponseEntity<AdvanceDTO> updateAdvance(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AdvanceDTO advanceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Advance : {}, {}", id, advanceDTO);
        if (advanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, advanceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!advanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AdvanceDTO result = advanceService.save(advanceDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, advanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /advances/:id} : Partial updates given fields of an existing advance, field will ignore if it is null
     *
     * @param id the id of the advanceDTO to save.
     * @param advanceDTO the advanceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated advanceDTO,
     * or with status {@code 400 (Bad Request)} if the advanceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the advanceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the advanceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/advances/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AdvanceDTO> partialUpdateAdvance(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AdvanceDTO advanceDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Advance partially : {}, {}", id, advanceDTO);
        if (advanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, advanceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!advanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AdvanceDTO> result = advanceService.partialUpdate(advanceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, advanceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /advances} : get all the advances.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of advances in body.
     */
    @GetMapping("/advances")
    public List<AdvanceDTO> getAllAdvances() {
        log.debug("REST request to get all Advances");
        return advanceService.findAll();
    }

    /**
     * {@code GET  /advances/:id} : get the "id" advance.
     *
     * @param id the id of the advanceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advanceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/advances/{id}")
    public ResponseEntity<AdvanceDTO> getAdvance(@PathVariable Long id) {
        log.debug("REST request to get Advance : {}", id);
        Optional<AdvanceDTO> advanceDTO = advanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advanceDTO);
    }

    /**
     * {@code DELETE  /advances/:id} : delete the "id" advance.
     *
     * @param id the id of the advanceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/advances/{id}")
    public ResponseEntity<Void> deleteAdvance(@PathVariable Long id) {
        log.debug("REST request to delete Advance : {}", id);
        advanceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
