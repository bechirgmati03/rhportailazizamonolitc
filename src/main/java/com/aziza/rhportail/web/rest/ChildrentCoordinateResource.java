package com.aziza.rhportail.web.rest;

import com.aziza.rhportail.repository.ChildrentCoordinateRepository;
import com.aziza.rhportail.service.ChildrentCoordinateService;
import com.aziza.rhportail.service.dto.ChildrentCoordinateDTO;
import com.aziza.rhportail.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.aziza.rhportail.domain.ChildrentCoordinate}.
 */
@RestController
@RequestMapping("/api")
public class ChildrentCoordinateResource {

    private final Logger log = LoggerFactory.getLogger(ChildrentCoordinateResource.class);

    private static final String ENTITY_NAME = "childrentCoordinate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChildrentCoordinateService childrentCoordinateService;

    private final ChildrentCoordinateRepository childrentCoordinateRepository;

    public ChildrentCoordinateResource(
        ChildrentCoordinateService childrentCoordinateService,
        ChildrentCoordinateRepository childrentCoordinateRepository
    ) {
        this.childrentCoordinateService = childrentCoordinateService;
        this.childrentCoordinateRepository = childrentCoordinateRepository;
    }

    /**
     * {@code POST  /childrent-coordinates} : Create a new childrentCoordinate.
     *
     * @param childrentCoordinateDTO the childrentCoordinateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new childrentCoordinateDTO, or with status {@code 400 (Bad Request)} if the childrentCoordinate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/childrent-coordinates")
    public ResponseEntity<ChildrentCoordinateDTO> createChildrentCoordinate(@RequestBody ChildrentCoordinateDTO childrentCoordinateDTO)
        throws URISyntaxException {
        log.debug("REST request to save ChildrentCoordinate : {}", childrentCoordinateDTO);
        if (childrentCoordinateDTO.getId() != null) {
            throw new BadRequestAlertException("A new childrentCoordinate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChildrentCoordinateDTO result = childrentCoordinateService.save(childrentCoordinateDTO);
        return ResponseEntity
            .created(new URI("/api/childrent-coordinates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /childrent-coordinates/:id} : Updates an existing childrentCoordinate.
     *
     * @param id the id of the childrentCoordinateDTO to save.
     * @param childrentCoordinateDTO the childrentCoordinateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated childrentCoordinateDTO,
     * or with status {@code 400 (Bad Request)} if the childrentCoordinateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the childrentCoordinateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/childrent-coordinates/{id}")
    public ResponseEntity<ChildrentCoordinateDTO> updateChildrentCoordinate(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChildrentCoordinateDTO childrentCoordinateDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ChildrentCoordinate : {}, {}", id, childrentCoordinateDTO);
        if (childrentCoordinateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, childrentCoordinateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!childrentCoordinateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChildrentCoordinateDTO result = childrentCoordinateService.save(childrentCoordinateDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, childrentCoordinateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /childrent-coordinates/:id} : Partial updates given fields of an existing childrentCoordinate, field will ignore if it is null
     *
     * @param id the id of the childrentCoordinateDTO to save.
     * @param childrentCoordinateDTO the childrentCoordinateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated childrentCoordinateDTO,
     * or with status {@code 400 (Bad Request)} if the childrentCoordinateDTO is not valid,
     * or with status {@code 404 (Not Found)} if the childrentCoordinateDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the childrentCoordinateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/childrent-coordinates/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ChildrentCoordinateDTO> partialUpdateChildrentCoordinate(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChildrentCoordinateDTO childrentCoordinateDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChildrentCoordinate partially : {}, {}", id, childrentCoordinateDTO);
        if (childrentCoordinateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, childrentCoordinateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!childrentCoordinateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChildrentCoordinateDTO> result = childrentCoordinateService.partialUpdate(childrentCoordinateDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, childrentCoordinateDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /childrent-coordinates} : get all the childrentCoordinates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of childrentCoordinates in body.
     */
    @GetMapping("/childrent-coordinates")
    public List<ChildrentCoordinateDTO> getAllChildrentCoordinates() {
        log.debug("REST request to get all ChildrentCoordinates");
        return childrentCoordinateService.findAll();
    }

    /**
     * {@code GET  /childrent-coordinates/:id} : get the "id" childrentCoordinate.
     *
     * @param id the id of the childrentCoordinateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the childrentCoordinateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/childrent-coordinates/{id}")
    public ResponseEntity<ChildrentCoordinateDTO> getChildrentCoordinate(@PathVariable Long id) {
        log.debug("REST request to get ChildrentCoordinate : {}", id);
        Optional<ChildrentCoordinateDTO> childrentCoordinateDTO = childrentCoordinateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(childrentCoordinateDTO);
    }

    /**
     * {@code DELETE  /childrent-coordinates/:id} : delete the "id" childrentCoordinate.
     *
     * @param id the id of the childrentCoordinateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/childrent-coordinates/{id}")
    public ResponseEntity<Void> deleteChildrentCoordinate(@PathVariable Long id) {
        log.debug("REST request to delete ChildrentCoordinate : {}", id);
        childrentCoordinateService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
