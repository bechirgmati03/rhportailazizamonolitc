import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AdvanceService } from '../service/advance.service';

import { AdvanceComponent } from './advance.component';

describe('Advance Management Component', () => {
  let comp: AdvanceComponent;
  let fixture: ComponentFixture<AdvanceComponent>;
  let service: AdvanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AdvanceComponent],
    })
      .overrideTemplate(AdvanceComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AdvanceComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AdvanceService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.advances?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
