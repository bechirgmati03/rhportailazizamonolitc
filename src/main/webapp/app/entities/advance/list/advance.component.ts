import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAdvance } from '../advance.model';
import { AdvanceService } from '../service/advance.service';
import { AdvanceDeleteDialogComponent } from '../delete/advance-delete-dialog.component';

@Component({
  selector: 'jhi-advance',
  templateUrl: './advance.component.html',
})
export class AdvanceComponent implements OnInit {
  advances?: IAdvance[];
  isLoading = false;

  constructor(protected advanceService: AdvanceService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.advanceService.query().subscribe({
      next: (res: HttpResponse<IAdvance[]>) => {
        this.isLoading = false;
        this.advances = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAdvance): number {
    return item.id!;
  }

  delete(advance: IAdvance): void {
    const modalRef = this.modalService.open(AdvanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.advance = advance;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
