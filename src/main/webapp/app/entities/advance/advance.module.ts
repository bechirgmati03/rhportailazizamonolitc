import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AdvanceComponent } from './list/advance.component';
import { AdvanceDetailComponent } from './detail/advance-detail.component';
import { AdvanceUpdateComponent } from './update/advance-update.component';
import { AdvanceDeleteDialogComponent } from './delete/advance-delete-dialog.component';
import { AdvanceRoutingModule } from './route/advance-routing.module';

@NgModule({
  imports: [SharedModule, AdvanceRoutingModule],
  declarations: [AdvanceComponent, AdvanceDetailComponent, AdvanceUpdateComponent, AdvanceDeleteDialogComponent],
  entryComponents: [AdvanceDeleteDialogComponent],
})
export class AdvanceModule {}
