import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AdvanceService } from '../service/advance.service';
import { IAdvance, Advance } from '../advance.model';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

import { AdvanceUpdateComponent } from './advance-update.component';

describe('Advance Management Update Component', () => {
  let comp: AdvanceUpdateComponent;
  let fixture: ComponentFixture<AdvanceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let advanceService: AdvanceService;
  let membersService: MembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AdvanceUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AdvanceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AdvanceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    advanceService = TestBed.inject(AdvanceService);
    membersService = TestBed.inject(MembersService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Members query and add missing value', () => {
      const advance: IAdvance = { id: 456 };
      const members: IMembers = { id: 70935 };
      advance.members = members;

      const membersCollection: IMembers[] = [{ id: 60893 }];
      jest.spyOn(membersService, 'query').mockReturnValue(of(new HttpResponse({ body: membersCollection })));
      const additionalMembers = [members];
      const expectedCollection: IMembers[] = [...additionalMembers, ...membersCollection];
      jest.spyOn(membersService, 'addMembersToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ advance });
      comp.ngOnInit();

      expect(membersService.query).toHaveBeenCalled();
      expect(membersService.addMembersToCollectionIfMissing).toHaveBeenCalledWith(membersCollection, ...additionalMembers);
      expect(comp.membersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const advance: IAdvance = { id: 456 };
      const members: IMembers = { id: 24964 };
      advance.members = members;

      activatedRoute.data = of({ advance });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(advance));
      expect(comp.membersSharedCollection).toContain(members);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Advance>>();
      const advance = { id: 123 };
      jest.spyOn(advanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ advance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: advance }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(advanceService.update).toHaveBeenCalledWith(advance);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Advance>>();
      const advance = new Advance();
      jest.spyOn(advanceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ advance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: advance }));
      saveSubject.complete();

      // THEN
      expect(advanceService.create).toHaveBeenCalledWith(advance);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Advance>>();
      const advance = { id: 123 };
      jest.spyOn(advanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ advance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(advanceService.update).toHaveBeenCalledWith(advance);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembersById', () => {
      it('Should return tracked Members primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembersById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
