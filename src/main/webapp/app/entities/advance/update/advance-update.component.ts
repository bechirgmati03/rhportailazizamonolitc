import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAdvance, Advance } from '../advance.model';
import { AdvanceService } from '../service/advance.service';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

@Component({
  selector: 'jhi-advance-update',
  templateUrl: './advance-update.component.html',
})
export class AdvanceUpdateComponent implements OnInit {
  isSaving = false;

  membersSharedCollection: IMembers[] = [];

  editForm = this.fb.group({
    id: [],
    reference: [],
    salaryAdvanceReason: [],
    salaryAdvanceReasonDetails: [],
    salaryAdvanceTypes: [],
    ceilingSalaryAdvanceTypes: [],
    degreeOfUrgency: [],
    date: [],
    maximumSalaryAdvance: [],
    salaryAdvanceAmount: [],
    repaymentPeriod: [],
    salaryDeductionMethod: [],
    totalAmountMonthlyPaymentDeducte: [],
    salaryMonthlyPaymentDeducte: [],
    primesMonthlyPaymentDeducte: [],
    status: [],
    reasonRejection: [],
    members: [],
  });

  constructor(
    protected advanceService: AdvanceService,
    protected membersService: MembersService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ advance }) => {
      this.updateForm(advance);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const advance = this.createFromForm();
    if (advance.id !== undefined) {
      this.subscribeToSaveResponse(this.advanceService.update(advance));
    } else {
      this.subscribeToSaveResponse(this.advanceService.create(advance));
    }
  }

  trackMembersById(index: number, item: IMembers): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAdvance>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(advance: IAdvance): void {
    this.editForm.patchValue({
      id: advance.id,
      reference: advance.reference,
      salaryAdvanceReason: advance.salaryAdvanceReason,
      salaryAdvanceReasonDetails: advance.salaryAdvanceReasonDetails,
      salaryAdvanceTypes: advance.salaryAdvanceTypes,
      ceilingSalaryAdvanceTypes: advance.ceilingSalaryAdvanceTypes,
      degreeOfUrgency: advance.degreeOfUrgency,
      date: advance.date,
      maximumSalaryAdvance: advance.maximumSalaryAdvance,
      salaryAdvanceAmount: advance.salaryAdvanceAmount,
      repaymentPeriod: advance.repaymentPeriod,
      salaryDeductionMethod: advance.salaryDeductionMethod,
      totalAmountMonthlyPaymentDeducte: advance.totalAmountMonthlyPaymentDeducte,
      salaryMonthlyPaymentDeducte: advance.salaryMonthlyPaymentDeducte,
      primesMonthlyPaymentDeducte: advance.primesMonthlyPaymentDeducte,
      status: advance.status,
      reasonRejection: advance.reasonRejection,
      members: advance.members,
    });

    this.membersSharedCollection = this.membersService.addMembersToCollectionIfMissing(this.membersSharedCollection, advance.members);
  }

  protected loadRelationshipsOptions(): void {
    this.membersService
      .query()
      .pipe(map((res: HttpResponse<IMembers[]>) => res.body ?? []))
      .pipe(map((members: IMembers[]) => this.membersService.addMembersToCollectionIfMissing(members, this.editForm.get('members')!.value)))
      .subscribe((members: IMembers[]) => (this.membersSharedCollection = members));
  }

  protected createFromForm(): IAdvance {
    return {
      ...new Advance(),
      id: this.editForm.get(['id'])!.value,
      reference: this.editForm.get(['reference'])!.value,
      salaryAdvanceReason: this.editForm.get(['salaryAdvanceReason'])!.value,
      salaryAdvanceReasonDetails: this.editForm.get(['salaryAdvanceReasonDetails'])!.value,
      salaryAdvanceTypes: this.editForm.get(['salaryAdvanceTypes'])!.value,
      ceilingSalaryAdvanceTypes: this.editForm.get(['ceilingSalaryAdvanceTypes'])!.value,
      degreeOfUrgency: this.editForm.get(['degreeOfUrgency'])!.value,
      date: this.editForm.get(['date'])!.value,
      maximumSalaryAdvance: this.editForm.get(['maximumSalaryAdvance'])!.value,
      salaryAdvanceAmount: this.editForm.get(['salaryAdvanceAmount'])!.value,
      repaymentPeriod: this.editForm.get(['repaymentPeriod'])!.value,
      salaryDeductionMethod: this.editForm.get(['salaryDeductionMethod'])!.value,
      totalAmountMonthlyPaymentDeducte: this.editForm.get(['totalAmountMonthlyPaymentDeducte'])!.value,
      salaryMonthlyPaymentDeducte: this.editForm.get(['salaryMonthlyPaymentDeducte'])!.value,
      primesMonthlyPaymentDeducte: this.editForm.get(['primesMonthlyPaymentDeducte'])!.value,
      status: this.editForm.get(['status'])!.value,
      reasonRejection: this.editForm.get(['reasonRejection'])!.value,
      members: this.editForm.get(['members'])!.value,
    };
  }
}
