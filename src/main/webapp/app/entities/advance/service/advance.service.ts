import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAdvance, getAdvanceIdentifier } from '../advance.model';

export type EntityResponseType = HttpResponse<IAdvance>;
export type EntityArrayResponseType = HttpResponse<IAdvance[]>;

@Injectable({ providedIn: 'root' })
export class AdvanceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/advances');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(advance: IAdvance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(advance);
    return this.http
      .post<IAdvance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(advance: IAdvance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(advance);
    return this.http
      .put<IAdvance>(`${this.resourceUrl}/${getAdvanceIdentifier(advance) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(advance: IAdvance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(advance);
    return this.http
      .patch<IAdvance>(`${this.resourceUrl}/${getAdvanceIdentifier(advance) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAdvance>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAdvance[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAdvanceToCollectionIfMissing(advanceCollection: IAdvance[], ...advancesToCheck: (IAdvance | null | undefined)[]): IAdvance[] {
    const advances: IAdvance[] = advancesToCheck.filter(isPresent);
    if (advances.length > 0) {
      const advanceCollectionIdentifiers = advanceCollection.map(advanceItem => getAdvanceIdentifier(advanceItem)!);
      const advancesToAdd = advances.filter(advanceItem => {
        const advanceIdentifier = getAdvanceIdentifier(advanceItem);
        if (advanceIdentifier == null || advanceCollectionIdentifiers.includes(advanceIdentifier)) {
          return false;
        }
        advanceCollectionIdentifiers.push(advanceIdentifier);
        return true;
      });
      return [...advancesToAdd, ...advanceCollection];
    }
    return advanceCollection;
  }

  protected convertDateFromClient(advance: IAdvance): IAdvance {
    return Object.assign({}, advance, {
      date: advance.date?.isValid() ? advance.date.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((advance: IAdvance) => {
        advance.date = advance.date ? dayjs(advance.date) : undefined;
      });
    }
    return res;
  }
}
