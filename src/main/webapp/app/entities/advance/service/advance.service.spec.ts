import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAdvance, Advance } from '../advance.model';

import { AdvanceService } from './advance.service';

describe('Advance Service', () => {
  let service: AdvanceService;
  let httpMock: HttpTestingController;
  let elemDefault: IAdvance;
  let expectedResult: IAdvance | IAdvance[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AdvanceService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      reference: 'AAAAAAA',
      salaryAdvanceReason: 'AAAAAAA',
      salaryAdvanceReasonDetails: 'AAAAAAA',
      salaryAdvanceTypes: 'AAAAAAA',
      ceilingSalaryAdvanceTypes: 0,
      degreeOfUrgency: 'AAAAAAA',
      date: currentDate,
      maximumSalaryAdvance: 0,
      salaryAdvanceAmount: 0,
      repaymentPeriod: 0,
      salaryDeductionMethod: 'AAAAAAA',
      totalAmountMonthlyPaymentDeducte: 0,
      salaryMonthlyPaymentDeducte: 0,
      primesMonthlyPaymentDeducte: 0,
      status: 'AAAAAAA',
      reasonRejection: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Advance', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.create(new Advance()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Advance', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          reference: 'BBBBBB',
          salaryAdvanceReason: 'BBBBBB',
          salaryAdvanceReasonDetails: 'BBBBBB',
          salaryAdvanceTypes: 'BBBBBB',
          ceilingSalaryAdvanceTypes: 1,
          degreeOfUrgency: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          maximumSalaryAdvance: 1,
          salaryAdvanceAmount: 1,
          repaymentPeriod: 1,
          salaryDeductionMethod: 'BBBBBB',
          totalAmountMonthlyPaymentDeducte: 1,
          salaryMonthlyPaymentDeducte: 1,
          primesMonthlyPaymentDeducte: 1,
          status: 'BBBBBB',
          reasonRejection: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Advance', () => {
      const patchObject = Object.assign(
        {
          salaryAdvanceReasonDetails: 'BBBBBB',
          ceilingSalaryAdvanceTypes: 1,
          degreeOfUrgency: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          maximumSalaryAdvance: 1,
          salaryAdvanceAmount: 1,
          salaryDeductionMethod: 'BBBBBB',
          totalAmountMonthlyPaymentDeducte: 1,
          reasonRejection: 'BBBBBB',
        },
        new Advance()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Advance', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          reference: 'BBBBBB',
          salaryAdvanceReason: 'BBBBBB',
          salaryAdvanceReasonDetails: 'BBBBBB',
          salaryAdvanceTypes: 'BBBBBB',
          ceilingSalaryAdvanceTypes: 1,
          degreeOfUrgency: 'BBBBBB',
          date: currentDate.format(DATE_FORMAT),
          maximumSalaryAdvance: 1,
          salaryAdvanceAmount: 1,
          repaymentPeriod: 1,
          salaryDeductionMethod: 'BBBBBB',
          totalAmountMonthlyPaymentDeducte: 1,
          salaryMonthlyPaymentDeducte: 1,
          primesMonthlyPaymentDeducte: 1,
          status: 'BBBBBB',
          reasonRejection: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Advance', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addAdvanceToCollectionIfMissing', () => {
      it('should add a Advance to an empty array', () => {
        const advance: IAdvance = { id: 123 };
        expectedResult = service.addAdvanceToCollectionIfMissing([], advance);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(advance);
      });

      it('should not add a Advance to an array that contains it', () => {
        const advance: IAdvance = { id: 123 };
        const advanceCollection: IAdvance[] = [
          {
            ...advance,
          },
          { id: 456 },
        ];
        expectedResult = service.addAdvanceToCollectionIfMissing(advanceCollection, advance);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Advance to an array that doesn't contain it", () => {
        const advance: IAdvance = { id: 123 };
        const advanceCollection: IAdvance[] = [{ id: 456 }];
        expectedResult = service.addAdvanceToCollectionIfMissing(advanceCollection, advance);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(advance);
      });

      it('should add only unique Advance to an array', () => {
        const advanceArray: IAdvance[] = [{ id: 123 }, { id: 456 }, { id: 57908 }];
        const advanceCollection: IAdvance[] = [{ id: 123 }];
        expectedResult = service.addAdvanceToCollectionIfMissing(advanceCollection, ...advanceArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const advance: IAdvance = { id: 123 };
        const advance2: IAdvance = { id: 456 };
        expectedResult = service.addAdvanceToCollectionIfMissing([], advance, advance2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(advance);
        expect(expectedResult).toContain(advance2);
      });

      it('should accept null and undefined values', () => {
        const advance: IAdvance = { id: 123 };
        expectedResult = service.addAdvanceToCollectionIfMissing([], null, advance, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(advance);
      });

      it('should return initial array if no Advance is added', () => {
        const advanceCollection: IAdvance[] = [{ id: 123 }];
        expectedResult = service.addAdvanceToCollectionIfMissing(advanceCollection, undefined, null);
        expect(expectedResult).toEqual(advanceCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
