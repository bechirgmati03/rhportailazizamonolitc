import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAdvance, Advance } from '../advance.model';
import { AdvanceService } from '../service/advance.service';

@Injectable({ providedIn: 'root' })
export class AdvanceRoutingResolveService implements Resolve<IAdvance> {
  constructor(protected service: AdvanceService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAdvance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((advance: HttpResponse<Advance>) => {
          if (advance.body) {
            return of(advance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Advance());
  }
}
