import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AdvanceComponent } from '../list/advance.component';
import { AdvanceDetailComponent } from '../detail/advance-detail.component';
import { AdvanceUpdateComponent } from '../update/advance-update.component';
import { AdvanceRoutingResolveService } from './advance-routing-resolve.service';

const advanceRoute: Routes = [
  {
    path: '',
    component: AdvanceComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AdvanceDetailComponent,
    resolve: {
      advance: AdvanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AdvanceUpdateComponent,
    resolve: {
      advance: AdvanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AdvanceUpdateComponent,
    resolve: {
      advance: AdvanceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(advanceRoute)],
  exports: [RouterModule],
})
export class AdvanceRoutingModule {}
