import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAdvance } from '../advance.model';
import { AdvanceService } from '../service/advance.service';

@Component({
  templateUrl: './advance-delete-dialog.component.html',
})
export class AdvanceDeleteDialogComponent {
  advance?: IAdvance;

  constructor(protected advanceService: AdvanceService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.advanceService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
