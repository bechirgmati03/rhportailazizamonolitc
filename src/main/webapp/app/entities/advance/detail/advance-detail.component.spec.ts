import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AdvanceDetailComponent } from './advance-detail.component';

describe('Advance Management Detail Component', () => {
  let comp: AdvanceDetailComponent;
  let fixture: ComponentFixture<AdvanceDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdvanceDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ advance: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AdvanceDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AdvanceDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load advance on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.advance).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
