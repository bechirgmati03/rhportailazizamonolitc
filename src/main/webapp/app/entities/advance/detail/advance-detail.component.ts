import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAdvance } from '../advance.model';

@Component({
  selector: 'jhi-advance-detail',
  templateUrl: './advance-detail.component.html',
})
export class AdvanceDetailComponent implements OnInit {
  advance: IAdvance | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ advance }) => {
      this.advance = advance;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
