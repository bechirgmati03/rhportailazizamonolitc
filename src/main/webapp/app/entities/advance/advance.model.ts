import dayjs from 'dayjs/esm';
import { IMembers } from 'app/entities/members/members.model';

export interface IAdvance {
  id?: number;
  reference?: string | null;
  salaryAdvanceReason?: string | null;
  salaryAdvanceReasonDetails?: string | null;
  salaryAdvanceTypes?: string | null;
  ceilingSalaryAdvanceTypes?: number | null;
  degreeOfUrgency?: string | null;
  date?: dayjs.Dayjs | null;
  maximumSalaryAdvance?: number | null;
  salaryAdvanceAmount?: number | null;
  repaymentPeriod?: number | null;
  salaryDeductionMethod?: string | null;
  totalAmountMonthlyPaymentDeducte?: number | null;
  salaryMonthlyPaymentDeducte?: number | null;
  primesMonthlyPaymentDeducte?: number | null;
  status?: string | null;
  reasonRejection?: string | null;
  members?: IMembers | null;
}

export class Advance implements IAdvance {
  constructor(
    public id?: number,
    public reference?: string | null,
    public salaryAdvanceReason?: string | null,
    public salaryAdvanceReasonDetails?: string | null,
    public salaryAdvanceTypes?: string | null,
    public ceilingSalaryAdvanceTypes?: number | null,
    public degreeOfUrgency?: string | null,
    public date?: dayjs.Dayjs | null,
    public maximumSalaryAdvance?: number | null,
    public salaryAdvanceAmount?: number | null,
    public repaymentPeriod?: number | null,
    public salaryDeductionMethod?: string | null,
    public totalAmountMonthlyPaymentDeducte?: number | null,
    public salaryMonthlyPaymentDeducte?: number | null,
    public primesMonthlyPaymentDeducte?: number | null,
    public status?: string | null,
    public reasonRejection?: string | null,
    public members?: IMembers | null
  ) {}
}

export function getAdvanceIdentifier(advance: IAdvance): number | undefined {
  return advance.id;
}
