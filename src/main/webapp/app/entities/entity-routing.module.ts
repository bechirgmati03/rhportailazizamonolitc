import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'members',
        data: { pageTitle: 'rhportailazizamonolitcApp.members.home.title' },
        loadChildren: () => import('./members/members.module').then(m => m.MembersModule),
      },
      {
        path: 'childrent-coordinate',
        data: { pageTitle: 'rhportailazizamonolitcApp.childrentCoordinate.home.title' },
        loadChildren: () => import('./childrent-coordinate/childrent-coordinate.module').then(m => m.ChildrentCoordinateModule),
      },
      {
        path: 'diploma',
        data: { pageTitle: 'rhportailazizamonolitcApp.diploma.home.title' },
        loadChildren: () => import('./diploma/diploma.module').then(m => m.DiplomaModule),
      },
      {
        path: 'professional-expeience',
        data: { pageTitle: 'rhportailazizamonolitcApp.professionalExpeience.home.title' },
        loadChildren: () => import('./professional-expeience/professional-expeience.module').then(m => m.ProfessionalExpeienceModule),
      },
      {
        path: 'advance',
        data: { pageTitle: 'rhportailazizamonolitcApp.advance.home.title' },
        loadChildren: () => import('./advance/advance.module').then(m => m.AdvanceModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
