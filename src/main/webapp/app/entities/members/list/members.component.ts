import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMembers } from '../members.model';
import { MembersService } from '../service/members.service';
import { MembersDeleteDialogComponent } from '../delete/members-delete-dialog.component';

@Component({
  selector: 'jhi-members',
  templateUrl: './members.component.html',
})
export class MembersComponent implements OnInit {
  members?: IMembers[];
  isLoading = false;

  constructor(protected membersService: MembersService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.membersService.query().subscribe({
      next: (res: HttpResponse<IMembers[]>) => {
        this.isLoading = false;
        this.members = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMembers): number {
    return item.id!;
  }

  delete(members: IMembers): void {
    const modalRef = this.modalService.open(MembersDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.members = members;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
