import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MembersService } from '../service/members.service';

import { MembersComponent } from './members.component';

describe('Members Management Component', () => {
  let comp: MembersComponent;
  let fixture: ComponentFixture<MembersComponent>;
  let service: MembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MembersComponent],
    })
      .overrideTemplate(MembersComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MembersComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MembersService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.members?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
