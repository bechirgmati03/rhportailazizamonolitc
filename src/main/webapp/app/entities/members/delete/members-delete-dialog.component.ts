import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMembers } from '../members.model';
import { MembersService } from '../service/members.service';

@Component({
  templateUrl: './members-delete-dialog.component.html',
})
export class MembersDeleteDialogComponent {
  members?: IMembers;

  constructor(protected membersService: MembersService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.membersService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
