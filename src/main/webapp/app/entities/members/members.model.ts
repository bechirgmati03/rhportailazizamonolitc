import dayjs from 'dayjs/esm';
import { IUser } from 'app/entities/user/user.model';
import { IDiploma } from 'app/entities/diploma/diploma.model';
import { IProfessionalExpeience } from 'app/entities/professional-expeience/professional-expeience.model';
import { IChildrentCoordinate } from 'app/entities/childrent-coordinate/childrent-coordinate.model';
import { IAdvance } from 'app/entities/advance/advance.model';

export interface IMembers {
  id?: number;
  matricule?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  dateOfBrith?: dayjs.Dayjs | null;
  civility?: string | null;
  placeOfBirth?: string | null;
  nationality?: string | null;
  firstNameFather?: string | null;
  firstNameMather?: string | null;
  lastNameFather?: string | null;
  lastNameMather?: string | null;
  site?: string | null;
  positionHeld?: string | null;
  matriculeOfHierarchicalChief?: string | null;
  firstNameHierarchicalChief?: string | null;
  lastNameHierarchicalChief?: string | null;
  identityDocumentType?: string | null;
  numberIdentityDocuement?: string | null;
  numberSocialSecurity?: string | null;
  personelAdress?: string | null;
  adressOfNextOfKin?: string | null;
  mailAdress?: string | null;
  homePhone?: number | null;
  personalPhone?: number | null;
  status?: string | null;
  houseHolder?: boolean | null;
  dependents?: number | null;
  drivingPermit?: boolean | null;
  firstNameSpousse?: string | null;
  lastNameSpousse?: string | null;
  phoneSpousse?: number | null;
  nameEmergencyContact?: string | null;
  phoneEmergencyContact?: number | null;
  chronicDisease?: boolean | null;
  levelOfStudy?: string | null;
  user?: IUser | null;
  diplomas?: IDiploma[] | null;
  professionalExpeiences?: IProfessionalExpeience[] | null;
  childrentCoordinates?: IChildrentCoordinate[] | null;
  advances?: IAdvance[] | null;
}

export class Members implements IMembers {
  constructor(
    public id?: number,
    public matricule?: string | null,
    public firstName?: string | null,
    public lastName?: string | null,
    public dateOfBrith?: dayjs.Dayjs | null,
    public civility?: string | null,
    public placeOfBirth?: string | null,
    public nationality?: string | null,
    public firstNameFather?: string | null,
    public firstNameMather?: string | null,
    public lastNameFather?: string | null,
    public lastNameMather?: string | null,
    public site?: string | null,
    public positionHeld?: string | null,
    public matriculeOfHierarchicalChief?: string | null,
    public firstNameHierarchicalChief?: string | null,
    public lastNameHierarchicalChief?: string | null,
    public identityDocumentType?: string | null,
    public numberIdentityDocuement?: string | null,
    public numberSocialSecurity?: string | null,
    public personelAdress?: string | null,
    public adressOfNextOfKin?: string | null,
    public mailAdress?: string | null,
    public homePhone?: number | null,
    public personalPhone?: number | null,
    public status?: string | null,
    public houseHolder?: boolean | null,
    public dependents?: number | null,
    public drivingPermit?: boolean | null,
    public firstNameSpousse?: string | null,
    public lastNameSpousse?: string | null,
    public phoneSpousse?: number | null,
    public nameEmergencyContact?: string | null,
    public phoneEmergencyContact?: number | null,
    public chronicDisease?: boolean | null,
    public levelOfStudy?: string | null,
    public user?: IUser | null,
    public diplomas?: IDiploma[] | null,
    public professionalExpeiences?: IProfessionalExpeience[] | null,
    public childrentCoordinates?: IChildrentCoordinate[] | null,
    public advances?: IAdvance[] | null
  ) {
    this.houseHolder = this.houseHolder ?? false;
    this.drivingPermit = this.drivingPermit ?? false;
    this.chronicDisease = this.chronicDisease ?? false;
  }
}

export function getMembersIdentifier(members: IMembers): number | undefined {
  return members.id;
}
