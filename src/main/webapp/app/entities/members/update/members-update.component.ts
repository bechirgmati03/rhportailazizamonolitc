import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMembers, Members } from '../members.model';
import { MembersService } from '../service/members.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-members-update',
  templateUrl: './members-update.component.html',
})
export class MembersUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    matricule: [],
    firstName: [],
    lastName: [],
    dateOfBrith: [],
    civility: [],
    placeOfBirth: [],
    nationality: [],
    firstNameFather: [],
    firstNameMather: [],
    lastNameFather: [],
    lastNameMather: [],
    site: [],
    positionHeld: [],
    matriculeOfHierarchicalChief: [],
    firstNameHierarchicalChief: [],
    lastNameHierarchicalChief: [],
    identityDocumentType: [],
    numberIdentityDocuement: [],
    numberSocialSecurity: [],
    personelAdress: [],
    adressOfNextOfKin: [],
    mailAdress: [],
    homePhone: [],
    personalPhone: [],
    status: [],
    houseHolder: [],
    dependents: [],
    drivingPermit: [],
    firstNameSpousse: [],
    lastNameSpousse: [],
    phoneSpousse: [],
    nameEmergencyContact: [],
    phoneEmergencyContact: [],
    chronicDisease: [],
    levelOfStudy: [],
    user: [],
  });

  constructor(
    protected membersService: MembersService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ members }) => {
      this.updateForm(members);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const members = this.createFromForm();
    if (members.id !== undefined) {
      this.subscribeToSaveResponse(this.membersService.update(members));
    } else {
      this.subscribeToSaveResponse(this.membersService.create(members));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMembers>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(members: IMembers): void {
    this.editForm.patchValue({
      id: members.id,
      matricule: members.matricule,
      firstName: members.firstName,
      lastName: members.lastName,
      dateOfBrith: members.dateOfBrith,
      civility: members.civility,
      placeOfBirth: members.placeOfBirth,
      nationality: members.nationality,
      firstNameFather: members.firstNameFather,
      firstNameMather: members.firstNameMather,
      lastNameFather: members.lastNameFather,
      lastNameMather: members.lastNameMather,
      site: members.site,
      positionHeld: members.positionHeld,
      matriculeOfHierarchicalChief: members.matriculeOfHierarchicalChief,
      firstNameHierarchicalChief: members.firstNameHierarchicalChief,
      lastNameHierarchicalChief: members.lastNameHierarchicalChief,
      identityDocumentType: members.identityDocumentType,
      numberIdentityDocuement: members.numberIdentityDocuement,
      numberSocialSecurity: members.numberSocialSecurity,
      personelAdress: members.personelAdress,
      adressOfNextOfKin: members.adressOfNextOfKin,
      mailAdress: members.mailAdress,
      homePhone: members.homePhone,
      personalPhone: members.personalPhone,
      status: members.status,
      houseHolder: members.houseHolder,
      dependents: members.dependents,
      drivingPermit: members.drivingPermit,
      firstNameSpousse: members.firstNameSpousse,
      lastNameSpousse: members.lastNameSpousse,
      phoneSpousse: members.phoneSpousse,
      nameEmergencyContact: members.nameEmergencyContact,
      phoneEmergencyContact: members.phoneEmergencyContact,
      chronicDisease: members.chronicDisease,
      levelOfStudy: members.levelOfStudy,
      user: members.user,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, members.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): IMembers {
    return {
      ...new Members(),
      id: this.editForm.get(['id'])!.value,
      matricule: this.editForm.get(['matricule'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      dateOfBrith: this.editForm.get(['dateOfBrith'])!.value,
      civility: this.editForm.get(['civility'])!.value,
      placeOfBirth: this.editForm.get(['placeOfBirth'])!.value,
      nationality: this.editForm.get(['nationality'])!.value,
      firstNameFather: this.editForm.get(['firstNameFather'])!.value,
      firstNameMather: this.editForm.get(['firstNameMather'])!.value,
      lastNameFather: this.editForm.get(['lastNameFather'])!.value,
      lastNameMather: this.editForm.get(['lastNameMather'])!.value,
      site: this.editForm.get(['site'])!.value,
      positionHeld: this.editForm.get(['positionHeld'])!.value,
      matriculeOfHierarchicalChief: this.editForm.get(['matriculeOfHierarchicalChief'])!.value,
      firstNameHierarchicalChief: this.editForm.get(['firstNameHierarchicalChief'])!.value,
      lastNameHierarchicalChief: this.editForm.get(['lastNameHierarchicalChief'])!.value,
      identityDocumentType: this.editForm.get(['identityDocumentType'])!.value,
      numberIdentityDocuement: this.editForm.get(['numberIdentityDocuement'])!.value,
      numberSocialSecurity: this.editForm.get(['numberSocialSecurity'])!.value,
      personelAdress: this.editForm.get(['personelAdress'])!.value,
      adressOfNextOfKin: this.editForm.get(['adressOfNextOfKin'])!.value,
      mailAdress: this.editForm.get(['mailAdress'])!.value,
      homePhone: this.editForm.get(['homePhone'])!.value,
      personalPhone: this.editForm.get(['personalPhone'])!.value,
      status: this.editForm.get(['status'])!.value,
      houseHolder: this.editForm.get(['houseHolder'])!.value,
      dependents: this.editForm.get(['dependents'])!.value,
      drivingPermit: this.editForm.get(['drivingPermit'])!.value,
      firstNameSpousse: this.editForm.get(['firstNameSpousse'])!.value,
      lastNameSpousse: this.editForm.get(['lastNameSpousse'])!.value,
      phoneSpousse: this.editForm.get(['phoneSpousse'])!.value,
      nameEmergencyContact: this.editForm.get(['nameEmergencyContact'])!.value,
      phoneEmergencyContact: this.editForm.get(['phoneEmergencyContact'])!.value,
      chronicDisease: this.editForm.get(['chronicDisease'])!.value,
      levelOfStudy: this.editForm.get(['levelOfStudy'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
