import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMembers, Members } from '../members.model';

import { MembersService } from './members.service';

describe('Members Service', () => {
  let service: MembersService;
  let httpMock: HttpTestingController;
  let elemDefault: IMembers;
  let expectedResult: IMembers | IMembers[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MembersService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      matricule: 'AAAAAAA',
      firstName: 'AAAAAAA',
      lastName: 'AAAAAAA',
      dateOfBrith: currentDate,
      civility: 'AAAAAAA',
      placeOfBirth: 'AAAAAAA',
      nationality: 'AAAAAAA',
      firstNameFather: 'AAAAAAA',
      firstNameMather: 'AAAAAAA',
      lastNameFather: 'AAAAAAA',
      lastNameMather: 'AAAAAAA',
      site: 'AAAAAAA',
      positionHeld: 'AAAAAAA',
      matriculeOfHierarchicalChief: 'AAAAAAA',
      firstNameHierarchicalChief: 'AAAAAAA',
      lastNameHierarchicalChief: 'AAAAAAA',
      identityDocumentType: 'AAAAAAA',
      numberIdentityDocuement: 'AAAAAAA',
      numberSocialSecurity: 'AAAAAAA',
      personelAdress: 'AAAAAAA',
      adressOfNextOfKin: 'AAAAAAA',
      mailAdress: 'AAAAAAA',
      homePhone: 0,
      personalPhone: 0,
      status: 'AAAAAAA',
      houseHolder: false,
      dependents: 0,
      drivingPermit: false,
      firstNameSpousse: 'AAAAAAA',
      lastNameSpousse: 'AAAAAAA',
      phoneSpousse: 0,
      nameEmergencyContact: 'AAAAAAA',
      phoneEmergencyContact: 0,
      chronicDisease: false,
      levelOfStudy: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateOfBrith: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Members', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateOfBrith: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateOfBrith: currentDate,
        },
        returnedFromService
      );

      service.create(new Members()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Members', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          matricule: 'BBBBBB',
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          dateOfBrith: currentDate.format(DATE_FORMAT),
          civility: 'BBBBBB',
          placeOfBirth: 'BBBBBB',
          nationality: 'BBBBBB',
          firstNameFather: 'BBBBBB',
          firstNameMather: 'BBBBBB',
          lastNameFather: 'BBBBBB',
          lastNameMather: 'BBBBBB',
          site: 'BBBBBB',
          positionHeld: 'BBBBBB',
          matriculeOfHierarchicalChief: 'BBBBBB',
          firstNameHierarchicalChief: 'BBBBBB',
          lastNameHierarchicalChief: 'BBBBBB',
          identityDocumentType: 'BBBBBB',
          numberIdentityDocuement: 'BBBBBB',
          numberSocialSecurity: 'BBBBBB',
          personelAdress: 'BBBBBB',
          adressOfNextOfKin: 'BBBBBB',
          mailAdress: 'BBBBBB',
          homePhone: 1,
          personalPhone: 1,
          status: 'BBBBBB',
          houseHolder: true,
          dependents: 1,
          drivingPermit: true,
          firstNameSpousse: 'BBBBBB',
          lastNameSpousse: 'BBBBBB',
          phoneSpousse: 1,
          nameEmergencyContact: 'BBBBBB',
          phoneEmergencyContact: 1,
          chronicDisease: true,
          levelOfStudy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateOfBrith: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Members', () => {
      const patchObject = Object.assign(
        {
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          civility: 'BBBBBB',
          firstNameMather: 'BBBBBB',
          lastNameFather: 'BBBBBB',
          lastNameMather: 'BBBBBB',
          positionHeld: 'BBBBBB',
          matriculeOfHierarchicalChief: 'BBBBBB',
          firstNameHierarchicalChief: 'BBBBBB',
          lastNameHierarchicalChief: 'BBBBBB',
          adressOfNextOfKin: 'BBBBBB',
          mailAdress: 'BBBBBB',
          dependents: 1,
          firstNameSpousse: 'BBBBBB',
          lastNameSpousse: 'BBBBBB',
          phoneSpousse: 1,
          nameEmergencyContact: 'BBBBBB',
          chronicDisease: true,
          levelOfStudy: 'BBBBBB',
        },
        new Members()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateOfBrith: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Members', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          matricule: 'BBBBBB',
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          dateOfBrith: currentDate.format(DATE_FORMAT),
          civility: 'BBBBBB',
          placeOfBirth: 'BBBBBB',
          nationality: 'BBBBBB',
          firstNameFather: 'BBBBBB',
          firstNameMather: 'BBBBBB',
          lastNameFather: 'BBBBBB',
          lastNameMather: 'BBBBBB',
          site: 'BBBBBB',
          positionHeld: 'BBBBBB',
          matriculeOfHierarchicalChief: 'BBBBBB',
          firstNameHierarchicalChief: 'BBBBBB',
          lastNameHierarchicalChief: 'BBBBBB',
          identityDocumentType: 'BBBBBB',
          numberIdentityDocuement: 'BBBBBB',
          numberSocialSecurity: 'BBBBBB',
          personelAdress: 'BBBBBB',
          adressOfNextOfKin: 'BBBBBB',
          mailAdress: 'BBBBBB',
          homePhone: 1,
          personalPhone: 1,
          status: 'BBBBBB',
          houseHolder: true,
          dependents: 1,
          drivingPermit: true,
          firstNameSpousse: 'BBBBBB',
          lastNameSpousse: 'BBBBBB',
          phoneSpousse: 1,
          nameEmergencyContact: 'BBBBBB',
          phoneEmergencyContact: 1,
          chronicDisease: true,
          levelOfStudy: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateOfBrith: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Members', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMembersToCollectionIfMissing', () => {
      it('should add a Members to an empty array', () => {
        const members: IMembers = { id: 123 };
        expectedResult = service.addMembersToCollectionIfMissing([], members);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(members);
      });

      it('should not add a Members to an array that contains it', () => {
        const members: IMembers = { id: 123 };
        const membersCollection: IMembers[] = [
          {
            ...members,
          },
          { id: 456 },
        ];
        expectedResult = service.addMembersToCollectionIfMissing(membersCollection, members);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Members to an array that doesn't contain it", () => {
        const members: IMembers = { id: 123 };
        const membersCollection: IMembers[] = [{ id: 456 }];
        expectedResult = service.addMembersToCollectionIfMissing(membersCollection, members);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(members);
      });

      it('should add only unique Members to an array', () => {
        const membersArray: IMembers[] = [{ id: 123 }, { id: 456 }, { id: 62908 }];
        const membersCollection: IMembers[] = [{ id: 123 }];
        expectedResult = service.addMembersToCollectionIfMissing(membersCollection, ...membersArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const members: IMembers = { id: 123 };
        const members2: IMembers = { id: 456 };
        expectedResult = service.addMembersToCollectionIfMissing([], members, members2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(members);
        expect(expectedResult).toContain(members2);
      });

      it('should accept null and undefined values', () => {
        const members: IMembers = { id: 123 };
        expectedResult = service.addMembersToCollectionIfMissing([], null, members, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(members);
      });

      it('should return initial array if no Members is added', () => {
        const membersCollection: IMembers[] = [{ id: 123 }];
        expectedResult = service.addMembersToCollectionIfMissing(membersCollection, undefined, null);
        expect(expectedResult).toEqual(membersCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
