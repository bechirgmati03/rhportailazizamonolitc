import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMembers, getMembersIdentifier } from '../members.model';

export type EntityResponseType = HttpResponse<IMembers>;
export type EntityArrayResponseType = HttpResponse<IMembers[]>;

@Injectable({ providedIn: 'root' })
export class MembersService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/members');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(members: IMembers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(members);
    return this.http
      .post<IMembers>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(members: IMembers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(members);
    return this.http
      .put<IMembers>(`${this.resourceUrl}/${getMembersIdentifier(members) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(members: IMembers): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(members);
    return this.http
      .patch<IMembers>(`${this.resourceUrl}/${getMembersIdentifier(members) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMembers>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMembers[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMembersToCollectionIfMissing(membersCollection: IMembers[], ...membersToCheck: (IMembers | null | undefined)[]): IMembers[] {
    const members: IMembers[] = membersToCheck.filter(isPresent);
    if (members.length > 0) {
      const membersCollectionIdentifiers = membersCollection.map(membersItem => getMembersIdentifier(membersItem)!);
      const membersToAdd = members.filter(membersItem => {
        const membersIdentifier = getMembersIdentifier(membersItem);
        if (membersIdentifier == null || membersCollectionIdentifiers.includes(membersIdentifier)) {
          return false;
        }
        membersCollectionIdentifiers.push(membersIdentifier);
        return true;
      });
      return [...membersToAdd, ...membersCollection];
    }
    return membersCollection;
  }

  protected convertDateFromClient(members: IMembers): IMembers {
    return Object.assign({}, members, {
      dateOfBrith: members.dateOfBrith?.isValid() ? members.dateOfBrith.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateOfBrith = res.body.dateOfBrith ? dayjs(res.body.dateOfBrith) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((members: IMembers) => {
        members.dateOfBrith = members.dateOfBrith ? dayjs(members.dateOfBrith) : undefined;
      });
    }
    return res;
  }
}
