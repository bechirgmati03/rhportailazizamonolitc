import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDiploma } from '../diploma.model';
import { DiplomaService } from '../service/diploma.service';

@Component({
  templateUrl: './diploma-delete-dialog.component.html',
})
export class DiplomaDeleteDialogComponent {
  diploma?: IDiploma;

  constructor(protected diplomaService: DiplomaService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.diplomaService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
