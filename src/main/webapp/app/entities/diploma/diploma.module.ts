import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DiplomaComponent } from './list/diploma.component';
import { DiplomaDetailComponent } from './detail/diploma-detail.component';
import { DiplomaUpdateComponent } from './update/diploma-update.component';
import { DiplomaDeleteDialogComponent } from './delete/diploma-delete-dialog.component';
import { DiplomaRoutingModule } from './route/diploma-routing.module';

@NgModule({
  imports: [SharedModule, DiplomaRoutingModule],
  declarations: [DiplomaComponent, DiplomaDetailComponent, DiplomaUpdateComponent, DiplomaDeleteDialogComponent],
  entryComponents: [DiplomaDeleteDialogComponent],
})
export class DiplomaModule {}
