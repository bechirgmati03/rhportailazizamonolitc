import { IMembers } from 'app/entities/members/members.model';

export interface IDiploma {
  id?: number;
  institution?: string | null;
  yearOfGraduation?: number | null;
  diplomaTitle?: string | null;
  members?: IMembers | null;
}

export class Diploma implements IDiploma {
  constructor(
    public id?: number,
    public institution?: string | null,
    public yearOfGraduation?: number | null,
    public diplomaTitle?: string | null,
    public members?: IMembers | null
  ) {}
}

export function getDiplomaIdentifier(diploma: IDiploma): number | undefined {
  return diploma.id;
}
