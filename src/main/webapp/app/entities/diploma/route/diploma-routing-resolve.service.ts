import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDiploma, Diploma } from '../diploma.model';
import { DiplomaService } from '../service/diploma.service';

@Injectable({ providedIn: 'root' })
export class DiplomaRoutingResolveService implements Resolve<IDiploma> {
  constructor(protected service: DiplomaService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDiploma> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((diploma: HttpResponse<Diploma>) => {
          if (diploma.body) {
            return of(diploma.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Diploma());
  }
}
