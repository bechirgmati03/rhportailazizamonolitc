import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DiplomaComponent } from '../list/diploma.component';
import { DiplomaDetailComponent } from '../detail/diploma-detail.component';
import { DiplomaUpdateComponent } from '../update/diploma-update.component';
import { DiplomaRoutingResolveService } from './diploma-routing-resolve.service';

const diplomaRoute: Routes = [
  {
    path: '',
    component: DiplomaComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DiplomaDetailComponent,
    resolve: {
      diploma: DiplomaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DiplomaUpdateComponent,
    resolve: {
      diploma: DiplomaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DiplomaUpdateComponent,
    resolve: {
      diploma: DiplomaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(diplomaRoute)],
  exports: [RouterModule],
})
export class DiplomaRoutingModule {}
