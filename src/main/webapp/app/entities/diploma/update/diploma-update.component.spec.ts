import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DiplomaService } from '../service/diploma.service';
import { IDiploma, Diploma } from '../diploma.model';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

import { DiplomaUpdateComponent } from './diploma-update.component';

describe('Diploma Management Update Component', () => {
  let comp: DiplomaUpdateComponent;
  let fixture: ComponentFixture<DiplomaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let diplomaService: DiplomaService;
  let membersService: MembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DiplomaUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DiplomaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DiplomaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    diplomaService = TestBed.inject(DiplomaService);
    membersService = TestBed.inject(MembersService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Members query and add missing value', () => {
      const diploma: IDiploma = { id: 456 };
      const members: IMembers = { id: 16283 };
      diploma.members = members;

      const membersCollection: IMembers[] = [{ id: 63859 }];
      jest.spyOn(membersService, 'query').mockReturnValue(of(new HttpResponse({ body: membersCollection })));
      const additionalMembers = [members];
      const expectedCollection: IMembers[] = [...additionalMembers, ...membersCollection];
      jest.spyOn(membersService, 'addMembersToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ diploma });
      comp.ngOnInit();

      expect(membersService.query).toHaveBeenCalled();
      expect(membersService.addMembersToCollectionIfMissing).toHaveBeenCalledWith(membersCollection, ...additionalMembers);
      expect(comp.membersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const diploma: IDiploma = { id: 456 };
      const members: IMembers = { id: 65144 };
      diploma.members = members;

      activatedRoute.data = of({ diploma });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(diploma));
      expect(comp.membersSharedCollection).toContain(members);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Diploma>>();
      const diploma = { id: 123 };
      jest.spyOn(diplomaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diploma });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: diploma }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(diplomaService.update).toHaveBeenCalledWith(diploma);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Diploma>>();
      const diploma = new Diploma();
      jest.spyOn(diplomaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diploma });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: diploma }));
      saveSubject.complete();

      // THEN
      expect(diplomaService.create).toHaveBeenCalledWith(diploma);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Diploma>>();
      const diploma = { id: 123 };
      jest.spyOn(diplomaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ diploma });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(diplomaService.update).toHaveBeenCalledWith(diploma);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembersById', () => {
      it('Should return tracked Members primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembersById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
