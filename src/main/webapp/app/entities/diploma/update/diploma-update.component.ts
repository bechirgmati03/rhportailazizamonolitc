import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IDiploma, Diploma } from '../diploma.model';
import { DiplomaService } from '../service/diploma.service';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

@Component({
  selector: 'jhi-diploma-update',
  templateUrl: './diploma-update.component.html',
})
export class DiplomaUpdateComponent implements OnInit {
  isSaving = false;

  membersSharedCollection: IMembers[] = [];

  editForm = this.fb.group({
    id: [],
    institution: [],
    yearOfGraduation: [],
    diplomaTitle: [],
    members: [],
  });

  constructor(
    protected diplomaService: DiplomaService,
    protected membersService: MembersService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ diploma }) => {
      this.updateForm(diploma);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const diploma = this.createFromForm();
    if (diploma.id !== undefined) {
      this.subscribeToSaveResponse(this.diplomaService.update(diploma));
    } else {
      this.subscribeToSaveResponse(this.diplomaService.create(diploma));
    }
  }

  trackMembersById(index: number, item: IMembers): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiploma>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(diploma: IDiploma): void {
    this.editForm.patchValue({
      id: diploma.id,
      institution: diploma.institution,
      yearOfGraduation: diploma.yearOfGraduation,
      diplomaTitle: diploma.diplomaTitle,
      members: diploma.members,
    });

    this.membersSharedCollection = this.membersService.addMembersToCollectionIfMissing(this.membersSharedCollection, diploma.members);
  }

  protected loadRelationshipsOptions(): void {
    this.membersService
      .query()
      .pipe(map((res: HttpResponse<IMembers[]>) => res.body ?? []))
      .pipe(map((members: IMembers[]) => this.membersService.addMembersToCollectionIfMissing(members, this.editForm.get('members')!.value)))
      .subscribe((members: IMembers[]) => (this.membersSharedCollection = members));
  }

  protected createFromForm(): IDiploma {
    return {
      ...new Diploma(),
      id: this.editForm.get(['id'])!.value,
      institution: this.editForm.get(['institution'])!.value,
      yearOfGraduation: this.editForm.get(['yearOfGraduation'])!.value,
      diplomaTitle: this.editForm.get(['diplomaTitle'])!.value,
      members: this.editForm.get(['members'])!.value,
    };
  }
}
