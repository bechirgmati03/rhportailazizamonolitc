import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDiploma } from '../diploma.model';
import { DiplomaService } from '../service/diploma.service';
import { DiplomaDeleteDialogComponent } from '../delete/diploma-delete-dialog.component';

@Component({
  selector: 'jhi-diploma',
  templateUrl: './diploma.component.html',
})
export class DiplomaComponent implements OnInit {
  diplomas?: IDiploma[];
  isLoading = false;

  constructor(protected diplomaService: DiplomaService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.diplomaService.query().subscribe({
      next: (res: HttpResponse<IDiploma[]>) => {
        this.isLoading = false;
        this.diplomas = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDiploma): number {
    return item.id!;
  }

  delete(diploma: IDiploma): void {
    const modalRef = this.modalService.open(DiplomaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.diploma = diploma;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
