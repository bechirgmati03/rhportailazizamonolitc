import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DiplomaService } from '../service/diploma.service';

import { DiplomaComponent } from './diploma.component';

describe('Diploma Management Component', () => {
  let comp: DiplomaComponent;
  let fixture: ComponentFixture<DiplomaComponent>;
  let service: DiplomaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [DiplomaComponent],
    })
      .overrideTemplate(DiplomaComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DiplomaComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(DiplomaService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.diplomas?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
