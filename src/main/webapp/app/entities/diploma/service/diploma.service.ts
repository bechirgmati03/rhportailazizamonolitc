import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDiploma, getDiplomaIdentifier } from '../diploma.model';

export type EntityResponseType = HttpResponse<IDiploma>;
export type EntityArrayResponseType = HttpResponse<IDiploma[]>;

@Injectable({ providedIn: 'root' })
export class DiplomaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/diplomas');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(diploma: IDiploma): Observable<EntityResponseType> {
    return this.http.post<IDiploma>(this.resourceUrl, diploma, { observe: 'response' });
  }

  update(diploma: IDiploma): Observable<EntityResponseType> {
    return this.http.put<IDiploma>(`${this.resourceUrl}/${getDiplomaIdentifier(diploma) as number}`, diploma, { observe: 'response' });
  }

  partialUpdate(diploma: IDiploma): Observable<EntityResponseType> {
    return this.http.patch<IDiploma>(`${this.resourceUrl}/${getDiplomaIdentifier(diploma) as number}`, diploma, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDiploma>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDiploma[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDiplomaToCollectionIfMissing(diplomaCollection: IDiploma[], ...diplomasToCheck: (IDiploma | null | undefined)[]): IDiploma[] {
    const diplomas: IDiploma[] = diplomasToCheck.filter(isPresent);
    if (diplomas.length > 0) {
      const diplomaCollectionIdentifiers = diplomaCollection.map(diplomaItem => getDiplomaIdentifier(diplomaItem)!);
      const diplomasToAdd = diplomas.filter(diplomaItem => {
        const diplomaIdentifier = getDiplomaIdentifier(diplomaItem);
        if (diplomaIdentifier == null || diplomaCollectionIdentifiers.includes(diplomaIdentifier)) {
          return false;
        }
        diplomaCollectionIdentifiers.push(diplomaIdentifier);
        return true;
      });
      return [...diplomasToAdd, ...diplomaCollection];
    }
    return diplomaCollection;
  }
}
