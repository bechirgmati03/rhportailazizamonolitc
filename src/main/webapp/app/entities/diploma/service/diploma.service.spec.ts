import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDiploma, Diploma } from '../diploma.model';

import { DiplomaService } from './diploma.service';

describe('Diploma Service', () => {
  let service: DiplomaService;
  let httpMock: HttpTestingController;
  let elemDefault: IDiploma;
  let expectedResult: IDiploma | IDiploma[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DiplomaService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      institution: 'AAAAAAA',
      yearOfGraduation: 0,
      diplomaTitle: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Diploma', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Diploma()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Diploma', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          institution: 'BBBBBB',
          yearOfGraduation: 1,
          diplomaTitle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Diploma', () => {
      const patchObject = Object.assign(
        {
          diplomaTitle: 'BBBBBB',
        },
        new Diploma()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Diploma', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          institution: 'BBBBBB',
          yearOfGraduation: 1,
          diplomaTitle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Diploma', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addDiplomaToCollectionIfMissing', () => {
      it('should add a Diploma to an empty array', () => {
        const diploma: IDiploma = { id: 123 };
        expectedResult = service.addDiplomaToCollectionIfMissing([], diploma);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(diploma);
      });

      it('should not add a Diploma to an array that contains it', () => {
        const diploma: IDiploma = { id: 123 };
        const diplomaCollection: IDiploma[] = [
          {
            ...diploma,
          },
          { id: 456 },
        ];
        expectedResult = service.addDiplomaToCollectionIfMissing(diplomaCollection, diploma);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Diploma to an array that doesn't contain it", () => {
        const diploma: IDiploma = { id: 123 };
        const diplomaCollection: IDiploma[] = [{ id: 456 }];
        expectedResult = service.addDiplomaToCollectionIfMissing(diplomaCollection, diploma);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(diploma);
      });

      it('should add only unique Diploma to an array', () => {
        const diplomaArray: IDiploma[] = [{ id: 123 }, { id: 456 }, { id: 84908 }];
        const diplomaCollection: IDiploma[] = [{ id: 123 }];
        expectedResult = service.addDiplomaToCollectionIfMissing(diplomaCollection, ...diplomaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const diploma: IDiploma = { id: 123 };
        const diploma2: IDiploma = { id: 456 };
        expectedResult = service.addDiplomaToCollectionIfMissing([], diploma, diploma2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(diploma);
        expect(expectedResult).toContain(diploma2);
      });

      it('should accept null and undefined values', () => {
        const diploma: IDiploma = { id: 123 };
        expectedResult = service.addDiplomaToCollectionIfMissing([], null, diploma, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(diploma);
      });

      it('should return initial array if no Diploma is added', () => {
        const diplomaCollection: IDiploma[] = [{ id: 123 }];
        expectedResult = service.addDiplomaToCollectionIfMissing(diplomaCollection, undefined, null);
        expect(expectedResult).toEqual(diplomaCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
