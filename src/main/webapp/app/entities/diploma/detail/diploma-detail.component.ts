import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDiploma } from '../diploma.model';

@Component({
  selector: 'jhi-diploma-detail',
  templateUrl: './diploma-detail.component.html',
})
export class DiplomaDetailComponent implements OnInit {
  diploma: IDiploma | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ diploma }) => {
      this.diploma = diploma;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
