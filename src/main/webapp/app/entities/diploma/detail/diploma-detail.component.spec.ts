import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DiplomaDetailComponent } from './diploma-detail.component';

describe('Diploma Management Detail Component', () => {
  let comp: DiplomaDetailComponent;
  let fixture: ComponentFixture<DiplomaDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DiplomaDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ diploma: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DiplomaDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DiplomaDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load diploma on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.diploma).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
