import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IChildrentCoordinate, ChildrentCoordinate } from '../childrent-coordinate.model';
import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

@Component({
  selector: 'jhi-childrent-coordinate-update',
  templateUrl: './childrent-coordinate-update.component.html',
})
export class ChildrentCoordinateUpdateComponent implements OnInit {
  isSaving = false;

  membersSharedCollection: IMembers[] = [];

  editForm = this.fb.group({
    id: [],
    firstNameChild: [],
    lastNameChild: [],
    dateOfBirthChild: [],
    schoolLevel: [],
    handicapped: [],
    members: [],
  });

  constructor(
    protected childrentCoordinateService: ChildrentCoordinateService,
    protected membersService: MembersService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ childrentCoordinate }) => {
      this.updateForm(childrentCoordinate);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const childrentCoordinate = this.createFromForm();
    if (childrentCoordinate.id !== undefined) {
      this.subscribeToSaveResponse(this.childrentCoordinateService.update(childrentCoordinate));
    } else {
      this.subscribeToSaveResponse(this.childrentCoordinateService.create(childrentCoordinate));
    }
  }

  trackMembersById(index: number, item: IMembers): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChildrentCoordinate>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(childrentCoordinate: IChildrentCoordinate): void {
    this.editForm.patchValue({
      id: childrentCoordinate.id,
      firstNameChild: childrentCoordinate.firstNameChild,
      lastNameChild: childrentCoordinate.lastNameChild,
      dateOfBirthChild: childrentCoordinate.dateOfBirthChild,
      schoolLevel: childrentCoordinate.schoolLevel,
      handicapped: childrentCoordinate.handicapped,
      members: childrentCoordinate.members,
    });

    this.membersSharedCollection = this.membersService.addMembersToCollectionIfMissing(
      this.membersSharedCollection,
      childrentCoordinate.members
    );
  }

  protected loadRelationshipsOptions(): void {
    this.membersService
      .query()
      .pipe(map((res: HttpResponse<IMembers[]>) => res.body ?? []))
      .pipe(map((members: IMembers[]) => this.membersService.addMembersToCollectionIfMissing(members, this.editForm.get('members')!.value)))
      .subscribe((members: IMembers[]) => (this.membersSharedCollection = members));
  }

  protected createFromForm(): IChildrentCoordinate {
    return {
      ...new ChildrentCoordinate(),
      id: this.editForm.get(['id'])!.value,
      firstNameChild: this.editForm.get(['firstNameChild'])!.value,
      lastNameChild: this.editForm.get(['lastNameChild'])!.value,
      dateOfBirthChild: this.editForm.get(['dateOfBirthChild'])!.value,
      schoolLevel: this.editForm.get(['schoolLevel'])!.value,
      handicapped: this.editForm.get(['handicapped'])!.value,
      members: this.editForm.get(['members'])!.value,
    };
  }
}
