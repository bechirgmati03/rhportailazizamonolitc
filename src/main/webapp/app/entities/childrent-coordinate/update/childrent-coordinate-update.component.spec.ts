import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';
import { IChildrentCoordinate, ChildrentCoordinate } from '../childrent-coordinate.model';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

import { ChildrentCoordinateUpdateComponent } from './childrent-coordinate-update.component';

describe('ChildrentCoordinate Management Update Component', () => {
  let comp: ChildrentCoordinateUpdateComponent;
  let fixture: ComponentFixture<ChildrentCoordinateUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let childrentCoordinateService: ChildrentCoordinateService;
  let membersService: MembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ChildrentCoordinateUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ChildrentCoordinateUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChildrentCoordinateUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    childrentCoordinateService = TestBed.inject(ChildrentCoordinateService);
    membersService = TestBed.inject(MembersService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Members query and add missing value', () => {
      const childrentCoordinate: IChildrentCoordinate = { id: 456 };
      const members: IMembers = { id: 41657 };
      childrentCoordinate.members = members;

      const membersCollection: IMembers[] = [{ id: 77414 }];
      jest.spyOn(membersService, 'query').mockReturnValue(of(new HttpResponse({ body: membersCollection })));
      const additionalMembers = [members];
      const expectedCollection: IMembers[] = [...additionalMembers, ...membersCollection];
      jest.spyOn(membersService, 'addMembersToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ childrentCoordinate });
      comp.ngOnInit();

      expect(membersService.query).toHaveBeenCalled();
      expect(membersService.addMembersToCollectionIfMissing).toHaveBeenCalledWith(membersCollection, ...additionalMembers);
      expect(comp.membersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const childrentCoordinate: IChildrentCoordinate = { id: 456 };
      const members: IMembers = { id: 22493 };
      childrentCoordinate.members = members;

      activatedRoute.data = of({ childrentCoordinate });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(childrentCoordinate));
      expect(comp.membersSharedCollection).toContain(members);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChildrentCoordinate>>();
      const childrentCoordinate = { id: 123 };
      jest.spyOn(childrentCoordinateService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ childrentCoordinate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: childrentCoordinate }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(childrentCoordinateService.update).toHaveBeenCalledWith(childrentCoordinate);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChildrentCoordinate>>();
      const childrentCoordinate = new ChildrentCoordinate();
      jest.spyOn(childrentCoordinateService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ childrentCoordinate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: childrentCoordinate }));
      saveSubject.complete();

      // THEN
      expect(childrentCoordinateService.create).toHaveBeenCalledWith(childrentCoordinate);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ChildrentCoordinate>>();
      const childrentCoordinate = { id: 123 };
      jest.spyOn(childrentCoordinateService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ childrentCoordinate });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(childrentCoordinateService.update).toHaveBeenCalledWith(childrentCoordinate);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembersById', () => {
      it('Should return tracked Members primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembersById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
