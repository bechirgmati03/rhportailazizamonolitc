import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChildrentCoordinate, getChildrentCoordinateIdentifier } from '../childrent-coordinate.model';

export type EntityResponseType = HttpResponse<IChildrentCoordinate>;
export type EntityArrayResponseType = HttpResponse<IChildrentCoordinate[]>;

@Injectable({ providedIn: 'root' })
export class ChildrentCoordinateService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/childrent-coordinates');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(childrentCoordinate: IChildrentCoordinate): Observable<EntityResponseType> {
    return this.http.post<IChildrentCoordinate>(this.resourceUrl, childrentCoordinate, { observe: 'response' });
  }

  update(childrentCoordinate: IChildrentCoordinate): Observable<EntityResponseType> {
    return this.http.put<IChildrentCoordinate>(
      `${this.resourceUrl}/${getChildrentCoordinateIdentifier(childrentCoordinate) as number}`,
      childrentCoordinate,
      { observe: 'response' }
    );
  }

  partialUpdate(childrentCoordinate: IChildrentCoordinate): Observable<EntityResponseType> {
    return this.http.patch<IChildrentCoordinate>(
      `${this.resourceUrl}/${getChildrentCoordinateIdentifier(childrentCoordinate) as number}`,
      childrentCoordinate,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IChildrentCoordinate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChildrentCoordinate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChildrentCoordinateToCollectionIfMissing(
    childrentCoordinateCollection: IChildrentCoordinate[],
    ...childrentCoordinatesToCheck: (IChildrentCoordinate | null | undefined)[]
  ): IChildrentCoordinate[] {
    const childrentCoordinates: IChildrentCoordinate[] = childrentCoordinatesToCheck.filter(isPresent);
    if (childrentCoordinates.length > 0) {
      const childrentCoordinateCollectionIdentifiers = childrentCoordinateCollection.map(
        childrentCoordinateItem => getChildrentCoordinateIdentifier(childrentCoordinateItem)!
      );
      const childrentCoordinatesToAdd = childrentCoordinates.filter(childrentCoordinateItem => {
        const childrentCoordinateIdentifier = getChildrentCoordinateIdentifier(childrentCoordinateItem);
        if (childrentCoordinateIdentifier == null || childrentCoordinateCollectionIdentifiers.includes(childrentCoordinateIdentifier)) {
          return false;
        }
        childrentCoordinateCollectionIdentifiers.push(childrentCoordinateIdentifier);
        return true;
      });
      return [...childrentCoordinatesToAdd, ...childrentCoordinateCollection];
    }
    return childrentCoordinateCollection;
  }
}
