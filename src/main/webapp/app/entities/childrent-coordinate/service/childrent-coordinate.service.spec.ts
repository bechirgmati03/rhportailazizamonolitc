import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IChildrentCoordinate, ChildrentCoordinate } from '../childrent-coordinate.model';

import { ChildrentCoordinateService } from './childrent-coordinate.service';

describe('ChildrentCoordinate Service', () => {
  let service: ChildrentCoordinateService;
  let httpMock: HttpTestingController;
  let elemDefault: IChildrentCoordinate;
  let expectedResult: IChildrentCoordinate | IChildrentCoordinate[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ChildrentCoordinateService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      firstNameChild: 'AAAAAAA',
      lastNameChild: 'AAAAAAA',
      dateOfBirthChild: 'AAAAAAA',
      schoolLevel: 'AAAAAAA',
      handicapped: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ChildrentCoordinate', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ChildrentCoordinate()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ChildrentCoordinate', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          firstNameChild: 'BBBBBB',
          lastNameChild: 'BBBBBB',
          dateOfBirthChild: 'BBBBBB',
          schoolLevel: 'BBBBBB',
          handicapped: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ChildrentCoordinate', () => {
      const patchObject = Object.assign(
        {
          firstNameChild: 'BBBBBB',
          dateOfBirthChild: 'BBBBBB',
          schoolLevel: 'BBBBBB',
          handicapped: true,
        },
        new ChildrentCoordinate()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ChildrentCoordinate', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          firstNameChild: 'BBBBBB',
          lastNameChild: 'BBBBBB',
          dateOfBirthChild: 'BBBBBB',
          schoolLevel: 'BBBBBB',
          handicapped: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ChildrentCoordinate', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addChildrentCoordinateToCollectionIfMissing', () => {
      it('should add a ChildrentCoordinate to an empty array', () => {
        const childrentCoordinate: IChildrentCoordinate = { id: 123 };
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing([], childrentCoordinate);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(childrentCoordinate);
      });

      it('should not add a ChildrentCoordinate to an array that contains it', () => {
        const childrentCoordinate: IChildrentCoordinate = { id: 123 };
        const childrentCoordinateCollection: IChildrentCoordinate[] = [
          {
            ...childrentCoordinate,
          },
          { id: 456 },
        ];
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing(childrentCoordinateCollection, childrentCoordinate);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ChildrentCoordinate to an array that doesn't contain it", () => {
        const childrentCoordinate: IChildrentCoordinate = { id: 123 };
        const childrentCoordinateCollection: IChildrentCoordinate[] = [{ id: 456 }];
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing(childrentCoordinateCollection, childrentCoordinate);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(childrentCoordinate);
      });

      it('should add only unique ChildrentCoordinate to an array', () => {
        const childrentCoordinateArray: IChildrentCoordinate[] = [{ id: 123 }, { id: 456 }, { id: 24660 }];
        const childrentCoordinateCollection: IChildrentCoordinate[] = [{ id: 123 }];
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing(childrentCoordinateCollection, ...childrentCoordinateArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const childrentCoordinate: IChildrentCoordinate = { id: 123 };
        const childrentCoordinate2: IChildrentCoordinate = { id: 456 };
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing([], childrentCoordinate, childrentCoordinate2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(childrentCoordinate);
        expect(expectedResult).toContain(childrentCoordinate2);
      });

      it('should accept null and undefined values', () => {
        const childrentCoordinate: IChildrentCoordinate = { id: 123 };
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing([], null, childrentCoordinate, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(childrentCoordinate);
      });

      it('should return initial array if no ChildrentCoordinate is added', () => {
        const childrentCoordinateCollection: IChildrentCoordinate[] = [{ id: 123 }];
        expectedResult = service.addChildrentCoordinateToCollectionIfMissing(childrentCoordinateCollection, undefined, null);
        expect(expectedResult).toEqual(childrentCoordinateCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
