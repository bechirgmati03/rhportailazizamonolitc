import { IMembers } from 'app/entities/members/members.model';

export interface IChildrentCoordinate {
  id?: number;
  firstNameChild?: string | null;
  lastNameChild?: string | null;
  dateOfBirthChild?: string | null;
  schoolLevel?: string | null;
  handicapped?: boolean | null;
  members?: IMembers | null;
}

export class ChildrentCoordinate implements IChildrentCoordinate {
  constructor(
    public id?: number,
    public firstNameChild?: string | null,
    public lastNameChild?: string | null,
    public dateOfBirthChild?: string | null,
    public schoolLevel?: string | null,
    public handicapped?: boolean | null,
    public members?: IMembers | null
  ) {
    this.handicapped = this.handicapped ?? false;
  }
}

export function getChildrentCoordinateIdentifier(childrentCoordinate: IChildrentCoordinate): number | undefined {
  return childrentCoordinate.id;
}
