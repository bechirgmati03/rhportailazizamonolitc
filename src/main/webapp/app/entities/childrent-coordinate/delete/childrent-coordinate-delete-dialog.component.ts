import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChildrentCoordinate } from '../childrent-coordinate.model';
import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';

@Component({
  templateUrl: './childrent-coordinate-delete-dialog.component.html',
})
export class ChildrentCoordinateDeleteDialogComponent {
  childrentCoordinate?: IChildrentCoordinate;

  constructor(protected childrentCoordinateService: ChildrentCoordinateService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.childrentCoordinateService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
