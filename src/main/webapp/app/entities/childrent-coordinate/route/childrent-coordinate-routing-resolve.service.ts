import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChildrentCoordinate, ChildrentCoordinate } from '../childrent-coordinate.model';
import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';

@Injectable({ providedIn: 'root' })
export class ChildrentCoordinateRoutingResolveService implements Resolve<IChildrentCoordinate> {
  constructor(protected service: ChildrentCoordinateService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChildrentCoordinate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((childrentCoordinate: HttpResponse<ChildrentCoordinate>) => {
          if (childrentCoordinate.body) {
            return of(childrentCoordinate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChildrentCoordinate());
  }
}
