import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChildrentCoordinateComponent } from '../list/childrent-coordinate.component';
import { ChildrentCoordinateDetailComponent } from '../detail/childrent-coordinate-detail.component';
import { ChildrentCoordinateUpdateComponent } from '../update/childrent-coordinate-update.component';
import { ChildrentCoordinateRoutingResolveService } from './childrent-coordinate-routing-resolve.service';

const childrentCoordinateRoute: Routes = [
  {
    path: '',
    component: ChildrentCoordinateComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChildrentCoordinateDetailComponent,
    resolve: {
      childrentCoordinate: ChildrentCoordinateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChildrentCoordinateUpdateComponent,
    resolve: {
      childrentCoordinate: ChildrentCoordinateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChildrentCoordinateUpdateComponent,
    resolve: {
      childrentCoordinate: ChildrentCoordinateRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(childrentCoordinateRoute)],
  exports: [RouterModule],
})
export class ChildrentCoordinateRoutingModule {}
