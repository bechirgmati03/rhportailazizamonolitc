import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChildrentCoordinateDetailComponent } from './childrent-coordinate-detail.component';

describe('ChildrentCoordinate Management Detail Component', () => {
  let comp: ChildrentCoordinateDetailComponent;
  let fixture: ComponentFixture<ChildrentCoordinateDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChildrentCoordinateDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ childrentCoordinate: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ChildrentCoordinateDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ChildrentCoordinateDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load childrentCoordinate on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.childrentCoordinate).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
