import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChildrentCoordinate } from '../childrent-coordinate.model';

@Component({
  selector: 'jhi-childrent-coordinate-detail',
  templateUrl: './childrent-coordinate-detail.component.html',
})
export class ChildrentCoordinateDetailComponent implements OnInit {
  childrentCoordinate: IChildrentCoordinate | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ childrentCoordinate }) => {
      this.childrentCoordinate = childrentCoordinate;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
