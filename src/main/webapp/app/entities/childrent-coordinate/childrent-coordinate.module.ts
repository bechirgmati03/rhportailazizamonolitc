import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ChildrentCoordinateComponent } from './list/childrent-coordinate.component';
import { ChildrentCoordinateDetailComponent } from './detail/childrent-coordinate-detail.component';
import { ChildrentCoordinateUpdateComponent } from './update/childrent-coordinate-update.component';
import { ChildrentCoordinateDeleteDialogComponent } from './delete/childrent-coordinate-delete-dialog.component';
import { ChildrentCoordinateRoutingModule } from './route/childrent-coordinate-routing.module';

@NgModule({
  imports: [SharedModule, ChildrentCoordinateRoutingModule],
  declarations: [
    ChildrentCoordinateComponent,
    ChildrentCoordinateDetailComponent,
    ChildrentCoordinateUpdateComponent,
    ChildrentCoordinateDeleteDialogComponent,
  ],
  entryComponents: [ChildrentCoordinateDeleteDialogComponent],
})
export class ChildrentCoordinateModule {}
