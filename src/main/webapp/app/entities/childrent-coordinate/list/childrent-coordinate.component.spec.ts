import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';

import { ChildrentCoordinateComponent } from './childrent-coordinate.component';

describe('ChildrentCoordinate Management Component', () => {
  let comp: ChildrentCoordinateComponent;
  let fixture: ComponentFixture<ChildrentCoordinateComponent>;
  let service: ChildrentCoordinateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ChildrentCoordinateComponent],
    })
      .overrideTemplate(ChildrentCoordinateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChildrentCoordinateComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ChildrentCoordinateService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.childrentCoordinates?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
