import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IChildrentCoordinate } from '../childrent-coordinate.model';
import { ChildrentCoordinateService } from '../service/childrent-coordinate.service';
import { ChildrentCoordinateDeleteDialogComponent } from '../delete/childrent-coordinate-delete-dialog.component';

@Component({
  selector: 'jhi-childrent-coordinate',
  templateUrl: './childrent-coordinate.component.html',
})
export class ChildrentCoordinateComponent implements OnInit {
  childrentCoordinates?: IChildrentCoordinate[];
  isLoading = false;

  constructor(protected childrentCoordinateService: ChildrentCoordinateService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.childrentCoordinateService.query().subscribe({
      next: (res: HttpResponse<IChildrentCoordinate[]>) => {
        this.isLoading = false;
        this.childrentCoordinates = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IChildrentCoordinate): number {
    return item.id!;
  }

  delete(childrentCoordinate: IChildrentCoordinate): void {
    const modalRef = this.modalService.open(ChildrentCoordinateDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.childrentCoordinate = childrentCoordinate;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
