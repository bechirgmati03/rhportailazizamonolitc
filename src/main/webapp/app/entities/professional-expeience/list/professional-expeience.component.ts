import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProfessionalExpeience } from '../professional-expeience.model';
import { ProfessionalExpeienceService } from '../service/professional-expeience.service';
import { ProfessionalExpeienceDeleteDialogComponent } from '../delete/professional-expeience-delete-dialog.component';

@Component({
  selector: 'jhi-professional-expeience',
  templateUrl: './professional-expeience.component.html',
})
export class ProfessionalExpeienceComponent implements OnInit {
  professionalExpeiences?: IProfessionalExpeience[];
  isLoading = false;

  constructor(protected professionalExpeienceService: ProfessionalExpeienceService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.professionalExpeienceService.query().subscribe({
      next: (res: HttpResponse<IProfessionalExpeience[]>) => {
        this.isLoading = false;
        this.professionalExpeiences = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IProfessionalExpeience): number {
    return item.id!;
  }

  delete(professionalExpeience: IProfessionalExpeience): void {
    const modalRef = this.modalService.open(ProfessionalExpeienceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.professionalExpeience = professionalExpeience;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
