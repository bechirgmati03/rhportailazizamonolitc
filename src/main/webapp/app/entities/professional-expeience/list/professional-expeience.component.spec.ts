import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ProfessionalExpeienceService } from '../service/professional-expeience.service';

import { ProfessionalExpeienceComponent } from './professional-expeience.component';

describe('ProfessionalExpeience Management Component', () => {
  let comp: ProfessionalExpeienceComponent;
  let fixture: ComponentFixture<ProfessionalExpeienceComponent>;
  let service: ProfessionalExpeienceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ProfessionalExpeienceComponent],
    })
      .overrideTemplate(ProfessionalExpeienceComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProfessionalExpeienceComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ProfessionalExpeienceService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.professionalExpeiences?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
