import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IProfessionalExpeience, ProfessionalExpeience } from '../professional-expeience.model';
import { ProfessionalExpeienceService } from '../service/professional-expeience.service';

import { ProfessionalExpeienceRoutingResolveService } from './professional-expeience-routing-resolve.service';

describe('ProfessionalExpeience routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ProfessionalExpeienceRoutingResolveService;
  let service: ProfessionalExpeienceService;
  let resultProfessionalExpeience: IProfessionalExpeience | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(ProfessionalExpeienceRoutingResolveService);
    service = TestBed.inject(ProfessionalExpeienceService);
    resultProfessionalExpeience = undefined;
  });

  describe('resolve', () => {
    it('should return IProfessionalExpeience returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultProfessionalExpeience = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultProfessionalExpeience).toEqual({ id: 123 });
    });

    it('should return new IProfessionalExpeience if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultProfessionalExpeience = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultProfessionalExpeience).toEqual(new ProfessionalExpeience());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as ProfessionalExpeience })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultProfessionalExpeience = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultProfessionalExpeience).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
