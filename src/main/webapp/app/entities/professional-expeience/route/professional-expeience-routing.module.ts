import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ProfessionalExpeienceComponent } from '../list/professional-expeience.component';
import { ProfessionalExpeienceDetailComponent } from '../detail/professional-expeience-detail.component';
import { ProfessionalExpeienceUpdateComponent } from '../update/professional-expeience-update.component';
import { ProfessionalExpeienceRoutingResolveService } from './professional-expeience-routing-resolve.service';

const professionalExpeienceRoute: Routes = [
  {
    path: '',
    component: ProfessionalExpeienceComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProfessionalExpeienceDetailComponent,
    resolve: {
      professionalExpeience: ProfessionalExpeienceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProfessionalExpeienceUpdateComponent,
    resolve: {
      professionalExpeience: ProfessionalExpeienceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProfessionalExpeienceUpdateComponent,
    resolve: {
      professionalExpeience: ProfessionalExpeienceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(professionalExpeienceRoute)],
  exports: [RouterModule],
})
export class ProfessionalExpeienceRoutingModule {}
