import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IProfessionalExpeience, ProfessionalExpeience } from '../professional-expeience.model';
import { ProfessionalExpeienceService } from '../service/professional-expeience.service';

@Injectable({ providedIn: 'root' })
export class ProfessionalExpeienceRoutingResolveService implements Resolve<IProfessionalExpeience> {
  constructor(protected service: ProfessionalExpeienceService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProfessionalExpeience> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((professionalExpeience: HttpResponse<ProfessionalExpeience>) => {
          if (professionalExpeience.body) {
            return of(professionalExpeience.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProfessionalExpeience());
  }
}
