import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProfessionalExpeience } from '../professional-expeience.model';

@Component({
  selector: 'jhi-professional-expeience-detail',
  templateUrl: './professional-expeience-detail.component.html',
})
export class ProfessionalExpeienceDetailComponent implements OnInit {
  professionalExpeience: IProfessionalExpeience | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ professionalExpeience }) => {
      this.professionalExpeience = professionalExpeience;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
