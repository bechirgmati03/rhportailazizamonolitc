import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProfessionalExpeienceDetailComponent } from './professional-expeience-detail.component';

describe('ProfessionalExpeience Management Detail Component', () => {
  let comp: ProfessionalExpeienceDetailComponent;
  let fixture: ComponentFixture<ProfessionalExpeienceDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfessionalExpeienceDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ professionalExpeience: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ProfessionalExpeienceDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ProfessionalExpeienceDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load professionalExpeience on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.professionalExpeience).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
