import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IProfessionalExpeience } from '../professional-expeience.model';
import { ProfessionalExpeienceService } from '../service/professional-expeience.service';

@Component({
  templateUrl: './professional-expeience-delete-dialog.component.html',
})
export class ProfessionalExpeienceDeleteDialogComponent {
  professionalExpeience?: IProfessionalExpeience;

  constructor(protected professionalExpeienceService: ProfessionalExpeienceService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.professionalExpeienceService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
