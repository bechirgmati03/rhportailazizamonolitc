import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ProfessionalExpeienceComponent } from './list/professional-expeience.component';
import { ProfessionalExpeienceDetailComponent } from './detail/professional-expeience-detail.component';
import { ProfessionalExpeienceUpdateComponent } from './update/professional-expeience-update.component';
import { ProfessionalExpeienceDeleteDialogComponent } from './delete/professional-expeience-delete-dialog.component';
import { ProfessionalExpeienceRoutingModule } from './route/professional-expeience-routing.module';

@NgModule({
  imports: [SharedModule, ProfessionalExpeienceRoutingModule],
  declarations: [
    ProfessionalExpeienceComponent,
    ProfessionalExpeienceDetailComponent,
    ProfessionalExpeienceUpdateComponent,
    ProfessionalExpeienceDeleteDialogComponent,
  ],
  entryComponents: [ProfessionalExpeienceDeleteDialogComponent],
})
export class ProfessionalExpeienceModule {}
