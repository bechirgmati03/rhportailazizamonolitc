import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IProfessionalExpeience, ProfessionalExpeience } from '../professional-expeience.model';
import { ProfessionalExpeienceService } from '../service/professional-expeience.service';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

@Component({
  selector: 'jhi-professional-expeience-update',
  templateUrl: './professional-expeience-update.component.html',
})
export class ProfessionalExpeienceUpdateComponent implements OnInit {
  isSaving = false;

  membersSharedCollection: IMembers[] = [];

  editForm = this.fb.group({
    id: [],
    company: [],
    startDate: [],
    endDate: [],
    poste: [],
    departement: [],
    store: [],
    members: [],
  });

  constructor(
    protected professionalExpeienceService: ProfessionalExpeienceService,
    protected membersService: MembersService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ professionalExpeience }) => {
      this.updateForm(professionalExpeience);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const professionalExpeience = this.createFromForm();
    if (professionalExpeience.id !== undefined) {
      this.subscribeToSaveResponse(this.professionalExpeienceService.update(professionalExpeience));
    } else {
      this.subscribeToSaveResponse(this.professionalExpeienceService.create(professionalExpeience));
    }
  }

  trackMembersById(index: number, item: IMembers): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfessionalExpeience>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(professionalExpeience: IProfessionalExpeience): void {
    this.editForm.patchValue({
      id: professionalExpeience.id,
      company: professionalExpeience.company,
      startDate: professionalExpeience.startDate,
      endDate: professionalExpeience.endDate,
      poste: professionalExpeience.poste,
      departement: professionalExpeience.departement,
      store: professionalExpeience.store,
      members: professionalExpeience.members,
    });

    this.membersSharedCollection = this.membersService.addMembersToCollectionIfMissing(
      this.membersSharedCollection,
      professionalExpeience.members
    );
  }

  protected loadRelationshipsOptions(): void {
    this.membersService
      .query()
      .pipe(map((res: HttpResponse<IMembers[]>) => res.body ?? []))
      .pipe(map((members: IMembers[]) => this.membersService.addMembersToCollectionIfMissing(members, this.editForm.get('members')!.value)))
      .subscribe((members: IMembers[]) => (this.membersSharedCollection = members));
  }

  protected createFromForm(): IProfessionalExpeience {
    return {
      ...new ProfessionalExpeience(),
      id: this.editForm.get(['id'])!.value,
      company: this.editForm.get(['company'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      poste: this.editForm.get(['poste'])!.value,
      departement: this.editForm.get(['departement'])!.value,
      store: this.editForm.get(['store'])!.value,
      members: this.editForm.get(['members'])!.value,
    };
  }
}
