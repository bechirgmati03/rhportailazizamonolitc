import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ProfessionalExpeienceService } from '../service/professional-expeience.service';
import { IProfessionalExpeience, ProfessionalExpeience } from '../professional-expeience.model';
import { IMembers } from 'app/entities/members/members.model';
import { MembersService } from 'app/entities/members/service/members.service';

import { ProfessionalExpeienceUpdateComponent } from './professional-expeience-update.component';

describe('ProfessionalExpeience Management Update Component', () => {
  let comp: ProfessionalExpeienceUpdateComponent;
  let fixture: ComponentFixture<ProfessionalExpeienceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let professionalExpeienceService: ProfessionalExpeienceService;
  let membersService: MembersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ProfessionalExpeienceUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ProfessionalExpeienceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProfessionalExpeienceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    professionalExpeienceService = TestBed.inject(ProfessionalExpeienceService);
    membersService = TestBed.inject(MembersService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Members query and add missing value', () => {
      const professionalExpeience: IProfessionalExpeience = { id: 456 };
      const members: IMembers = { id: 83648 };
      professionalExpeience.members = members;

      const membersCollection: IMembers[] = [{ id: 69153 }];
      jest.spyOn(membersService, 'query').mockReturnValue(of(new HttpResponse({ body: membersCollection })));
      const additionalMembers = [members];
      const expectedCollection: IMembers[] = [...additionalMembers, ...membersCollection];
      jest.spyOn(membersService, 'addMembersToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ professionalExpeience });
      comp.ngOnInit();

      expect(membersService.query).toHaveBeenCalled();
      expect(membersService.addMembersToCollectionIfMissing).toHaveBeenCalledWith(membersCollection, ...additionalMembers);
      expect(comp.membersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const professionalExpeience: IProfessionalExpeience = { id: 456 };
      const members: IMembers = { id: 77690 };
      professionalExpeience.members = members;

      activatedRoute.data = of({ professionalExpeience });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(professionalExpeience));
      expect(comp.membersSharedCollection).toContain(members);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProfessionalExpeience>>();
      const professionalExpeience = { id: 123 };
      jest.spyOn(professionalExpeienceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ professionalExpeience });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: professionalExpeience }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(professionalExpeienceService.update).toHaveBeenCalledWith(professionalExpeience);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProfessionalExpeience>>();
      const professionalExpeience = new ProfessionalExpeience();
      jest.spyOn(professionalExpeienceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ professionalExpeience });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: professionalExpeience }));
      saveSubject.complete();

      // THEN
      expect(professionalExpeienceService.create).toHaveBeenCalledWith(professionalExpeience);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProfessionalExpeience>>();
      const professionalExpeience = { id: 123 };
      jest.spyOn(professionalExpeienceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ professionalExpeience });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(professionalExpeienceService.update).toHaveBeenCalledWith(professionalExpeience);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembersById', () => {
      it('Should return tracked Members primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembersById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
