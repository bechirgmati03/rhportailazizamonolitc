import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IProfessionalExpeience, ProfessionalExpeience } from '../professional-expeience.model';

import { ProfessionalExpeienceService } from './professional-expeience.service';

describe('ProfessionalExpeience Service', () => {
  let service: ProfessionalExpeienceService;
  let httpMock: HttpTestingController;
  let elemDefault: IProfessionalExpeience;
  let expectedResult: IProfessionalExpeience | IProfessionalExpeience[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ProfessionalExpeienceService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      company: 'AAAAAAA',
      startDate: currentDate,
      endDate: currentDate,
      poste: 'AAAAAAA',
      departement: 'AAAAAAA',
      store: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          startDate: currentDate.format(DATE_FORMAT),
          endDate: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ProfessionalExpeience', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          startDate: currentDate.format(DATE_FORMAT),
          endDate: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          startDate: currentDate,
          endDate: currentDate,
        },
        returnedFromService
      );

      service.create(new ProfessionalExpeience()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ProfessionalExpeience', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          company: 'BBBBBB',
          startDate: currentDate.format(DATE_FORMAT),
          endDate: currentDate.format(DATE_FORMAT),
          poste: 'BBBBBB',
          departement: 'BBBBBB',
          store: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          startDate: currentDate,
          endDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ProfessionalExpeience', () => {
      const patchObject = Object.assign(
        {
          company: 'BBBBBB',
          startDate: currentDate.format(DATE_FORMAT),
          store: 'BBBBBB',
        },
        new ProfessionalExpeience()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          startDate: currentDate,
          endDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ProfessionalExpeience', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          company: 'BBBBBB',
          startDate: currentDate.format(DATE_FORMAT),
          endDate: currentDate.format(DATE_FORMAT),
          poste: 'BBBBBB',
          departement: 'BBBBBB',
          store: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          startDate: currentDate,
          endDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ProfessionalExpeience', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addProfessionalExpeienceToCollectionIfMissing', () => {
      it('should add a ProfessionalExpeience to an empty array', () => {
        const professionalExpeience: IProfessionalExpeience = { id: 123 };
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing([], professionalExpeience);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(professionalExpeience);
      });

      it('should not add a ProfessionalExpeience to an array that contains it', () => {
        const professionalExpeience: IProfessionalExpeience = { id: 123 };
        const professionalExpeienceCollection: IProfessionalExpeience[] = [
          {
            ...professionalExpeience,
          },
          { id: 456 },
        ];
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing(professionalExpeienceCollection, professionalExpeience);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ProfessionalExpeience to an array that doesn't contain it", () => {
        const professionalExpeience: IProfessionalExpeience = { id: 123 };
        const professionalExpeienceCollection: IProfessionalExpeience[] = [{ id: 456 }];
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing(professionalExpeienceCollection, professionalExpeience);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(professionalExpeience);
      });

      it('should add only unique ProfessionalExpeience to an array', () => {
        const professionalExpeienceArray: IProfessionalExpeience[] = [{ id: 123 }, { id: 456 }, { id: 54005 }];
        const professionalExpeienceCollection: IProfessionalExpeience[] = [{ id: 123 }];
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing(
          professionalExpeienceCollection,
          ...professionalExpeienceArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const professionalExpeience: IProfessionalExpeience = { id: 123 };
        const professionalExpeience2: IProfessionalExpeience = { id: 456 };
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing([], professionalExpeience, professionalExpeience2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(professionalExpeience);
        expect(expectedResult).toContain(professionalExpeience2);
      });

      it('should accept null and undefined values', () => {
        const professionalExpeience: IProfessionalExpeience = { id: 123 };
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing([], null, professionalExpeience, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(professionalExpeience);
      });

      it('should return initial array if no ProfessionalExpeience is added', () => {
        const professionalExpeienceCollection: IProfessionalExpeience[] = [{ id: 123 }];
        expectedResult = service.addProfessionalExpeienceToCollectionIfMissing(professionalExpeienceCollection, undefined, null);
        expect(expectedResult).toEqual(professionalExpeienceCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
