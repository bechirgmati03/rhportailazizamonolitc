import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IProfessionalExpeience, getProfessionalExpeienceIdentifier } from '../professional-expeience.model';

export type EntityResponseType = HttpResponse<IProfessionalExpeience>;
export type EntityArrayResponseType = HttpResponse<IProfessionalExpeience[]>;

@Injectable({ providedIn: 'root' })
export class ProfessionalExpeienceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/professional-expeiences');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(professionalExpeience: IProfessionalExpeience): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(professionalExpeience);
    return this.http
      .post<IProfessionalExpeience>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(professionalExpeience: IProfessionalExpeience): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(professionalExpeience);
    return this.http
      .put<IProfessionalExpeience>(`${this.resourceUrl}/${getProfessionalExpeienceIdentifier(professionalExpeience) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(professionalExpeience: IProfessionalExpeience): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(professionalExpeience);
    return this.http
      .patch<IProfessionalExpeience>(`${this.resourceUrl}/${getProfessionalExpeienceIdentifier(professionalExpeience) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IProfessionalExpeience>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IProfessionalExpeience[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addProfessionalExpeienceToCollectionIfMissing(
    professionalExpeienceCollection: IProfessionalExpeience[],
    ...professionalExpeiencesToCheck: (IProfessionalExpeience | null | undefined)[]
  ): IProfessionalExpeience[] {
    const professionalExpeiences: IProfessionalExpeience[] = professionalExpeiencesToCheck.filter(isPresent);
    if (professionalExpeiences.length > 0) {
      const professionalExpeienceCollectionIdentifiers = professionalExpeienceCollection.map(
        professionalExpeienceItem => getProfessionalExpeienceIdentifier(professionalExpeienceItem)!
      );
      const professionalExpeiencesToAdd = professionalExpeiences.filter(professionalExpeienceItem => {
        const professionalExpeienceIdentifier = getProfessionalExpeienceIdentifier(professionalExpeienceItem);
        if (
          professionalExpeienceIdentifier == null ||
          professionalExpeienceCollectionIdentifiers.includes(professionalExpeienceIdentifier)
        ) {
          return false;
        }
        professionalExpeienceCollectionIdentifiers.push(professionalExpeienceIdentifier);
        return true;
      });
      return [...professionalExpeiencesToAdd, ...professionalExpeienceCollection];
    }
    return professionalExpeienceCollection;
  }

  protected convertDateFromClient(professionalExpeience: IProfessionalExpeience): IProfessionalExpeience {
    return Object.assign({}, professionalExpeience, {
      startDate: professionalExpeience.startDate?.isValid() ? professionalExpeience.startDate.format(DATE_FORMAT) : undefined,
      endDate: professionalExpeience.endDate?.isValid() ? professionalExpeience.endDate.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? dayjs(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? dayjs(res.body.endDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((professionalExpeience: IProfessionalExpeience) => {
        professionalExpeience.startDate = professionalExpeience.startDate ? dayjs(professionalExpeience.startDate) : undefined;
        professionalExpeience.endDate = professionalExpeience.endDate ? dayjs(professionalExpeience.endDate) : undefined;
      });
    }
    return res;
  }
}
