import dayjs from 'dayjs/esm';
import { IMembers } from 'app/entities/members/members.model';

export interface IProfessionalExpeience {
  id?: number;
  company?: string | null;
  startDate?: dayjs.Dayjs | null;
  endDate?: dayjs.Dayjs | null;
  poste?: string | null;
  departement?: string | null;
  store?: string | null;
  members?: IMembers | null;
}

export class ProfessionalExpeience implements IProfessionalExpeience {
  constructor(
    public id?: number,
    public company?: string | null,
    public startDate?: dayjs.Dayjs | null,
    public endDate?: dayjs.Dayjs | null,
    public poste?: string | null,
    public departement?: string | null,
    public store?: string | null,
    public members?: IMembers | null
  ) {}
}

export function getProfessionalExpeienceIdentifier(professionalExpeience: IProfessionalExpeience): number | undefined {
  return professionalExpeience.id;
}
